#import <Foundation/Foundation.h>

/**
 * Size of the icon
 */
@interface YMKSize : NSObject

/**
 * Width
 */
@property (nonatomic, readonly) double width;

/**
 * Height
 */
@property (nonatomic, readonly) double height;


+ (nonnull YMKSize *)sizeWithWidth:( double)width
                            height:( double)height;


@end

