#import <YandexMapKit/YMKAtomEntry.h>
#import <YandexMapKit/YMKReviewsSpecificEntry.h>

/// @cond EXCLUDE
/**
 * Full review data with atom metainfo.
 */
@interface YMKReviewsEntry : NSObject

/**
 * atom review part
 */
@property (nonatomic, readonly, nonnull) YMKAtomEntry *atomEntry;

/**
 * specific review part
 */
@property (nonatomic, readonly, nonnull) YMKReviewsSpecificEntry *content;


+ (nonnull YMKReviewsEntry *)entryWithAtomEntry:(nonnull YMKAtomEntry *)atomEntry
                                        content:(nonnull YMKReviewsSpecificEntry *)content;


@end
/// @endcond

