#import <YandexMapKit/YMKMasstransitRoute.h>

#import <yandex/maps/mapkit/masstransit/route.h>

#import <memory>

@interface YMKMasstransitRoute ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::masstransit::Route>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::masstransit::Route>)nativeRoute;
- (std::shared_ptr<::yandex::maps::mapkit::masstransit::Route>)native;

@end
