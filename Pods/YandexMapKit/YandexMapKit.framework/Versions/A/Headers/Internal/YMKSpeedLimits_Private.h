#import <YandexMapKit/YMKSpeedLimits.h>

#import <yandex/maps/mapkit/guidance/speeding_policy.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::guidance::SpeedLimits, YMKSpeedLimits, void> {
    static ::yandex::maps::mapkit::guidance::SpeedLimits from(
        YMKSpeedLimits* platformSpeedLimits);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::guidance::SpeedLimits, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSpeedLimits*>::value>::type> {
    static ::yandex::maps::mapkit::guidance::SpeedLimits from(
        PlatformType platformSpeedLimits)
    {
        return ToNative<::yandex::maps::mapkit::guidance::SpeedLimits, YMKSpeedLimits>::from(
            platformSpeedLimits);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::guidance::SpeedLimits> {
    static YMKSpeedLimits* from(
        const ::yandex::maps::mapkit::guidance::SpeedLimits& speedLimits);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
