#import <YandexMapKit/YMKPointCloudLayer.h>

#import <yandex/maps/mapkit/point_cloud_layer/point_cloud_layer.h>

#import <memory>

@interface YMKPointCloudLayer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::point_cloud_layer::PointCloudLayer>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::point_cloud_layer::PointCloudLayer>)nativePointCloudLayer;
- (std::shared_ptr<::yandex::maps::mapkit::point_cloud_layer::PointCloudLayer>)native;

@end
