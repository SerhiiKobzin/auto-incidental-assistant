#import <YandexMapKit/YMKDrivingRoute.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/driving/route.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKDrivingRoute ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::driving::Route>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::driving::Route>)nativeRoute;
- (std::shared_ptr<::yandex::maps::mapkit::driving::Route>)native;

@end
