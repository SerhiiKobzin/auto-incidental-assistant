#import <YandexMapKit/YMKMasstransitThreadSession.h>

#import <yandex/maps/mapkit/masstransit/session.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace masstransit {
namespace ios {

ThreadSession::OnThreadResponse onThreadResponse(
    YMKMasstransitThreadSessionThreadHandler handler);
ThreadSession::OnThreadError onThreadError(
    YMKMasstransitThreadSessionThreadHandler handler);

} // namespace ios
} // namespace masstransit
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKMasstransitThreadSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::ThreadSession>)native;

- (::yandex::maps::mapkit::masstransit::ThreadSession *)nativeThreadSession;

@end
