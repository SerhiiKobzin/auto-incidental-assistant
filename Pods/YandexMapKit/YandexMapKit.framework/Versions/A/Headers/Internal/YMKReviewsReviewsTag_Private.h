#import <YandexMapKit/YMKReviewsReviewsTag.h>

#import <yandex/maps/mapkit/reviews/reviews.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::reviews::Tag, YMKReviewsReviewsTag, void> {
    static ::yandex::maps::mapkit::reviews::Tag from(
        YMKReviewsReviewsTag* platformTag);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::reviews::Tag, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKReviewsReviewsTag*>::value>::type> {
    static ::yandex::maps::mapkit::reviews::Tag from(
        PlatformType platformTag)
    {
        return ToNative<::yandex::maps::mapkit::reviews::Tag, YMKReviewsReviewsTag>::from(
            platformTag);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::reviews::Tag> {
    static YMKReviewsReviewsTag* from(
        const ::yandex::maps::mapkit::reviews::Tag& tag);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
