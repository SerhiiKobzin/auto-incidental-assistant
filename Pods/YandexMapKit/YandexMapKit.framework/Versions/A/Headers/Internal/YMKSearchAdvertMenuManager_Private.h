#import <YandexMapKit/YMKSearchAdvertMenuManager.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/search/advert_manager.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKSearchAdvertMenuManager ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::search::AdvertMenuManager>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::search::AdvertMenuManager>)nativeAdvertMenuManager;
- (std::shared_ptr<::yandex::maps::mapkit::search::AdvertMenuManager>)native;

@end
