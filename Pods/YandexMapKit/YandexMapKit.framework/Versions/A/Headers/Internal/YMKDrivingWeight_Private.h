#import <YandexMapKit/YMKDrivingWeight.h>

#import <yandex/maps/mapkit/driving/weight.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::driving::Weight, YMKDrivingWeight, void> {
    static ::yandex::maps::mapkit::driving::Weight from(
        YMKDrivingWeight* platformWeight);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::driving::Weight, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingWeight*>::value>::type> {
    static ::yandex::maps::mapkit::driving::Weight from(
        PlatformType platformWeight)
    {
        return ToNative<::yandex::maps::mapkit::driving::Weight, YMKDrivingWeight>::from(
            platformWeight);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::driving::Weight> {
    static YMKDrivingWeight* from(
        const ::yandex::maps::mapkit::driving::Weight& weight);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
