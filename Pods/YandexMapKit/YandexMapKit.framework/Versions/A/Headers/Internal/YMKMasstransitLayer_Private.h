#import <YandexMapKit/YMKMasstransitLayer.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/masstransit/masstransit_layer.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKMasstransitLayer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::masstransit::MasstransitLayer>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::masstransit::MasstransitLayer>)nativeMasstransitLayer;
- (std::shared_ptr<::yandex::maps::mapkit::masstransit::MasstransitLayer>)native;

@end
