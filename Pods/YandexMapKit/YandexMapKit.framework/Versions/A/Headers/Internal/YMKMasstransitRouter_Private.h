#import <YandexMapKit/YMKMasstransitRouter.h>

#import <yandex/maps/mapkit/masstransit/masstransit_router.h>

#import <memory>

@interface YMKMasstransitRouter ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::masstransit::MasstransitRouter>)native;

- (::yandex::maps::mapkit::masstransit::MasstransitRouter *)nativeMasstransitRouter;

@end
