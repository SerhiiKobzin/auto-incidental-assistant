#import <YandexMapKit/YMKSpeedingPolicy.h>

#import <yandex/maps/mapkit/guidance/speeding_policy.h>

#import <memory>

@interface YMKSpeedingPolicy ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::guidance::SpeedingPolicy>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::guidance::SpeedingPolicy>)nativeSpeedingPolicy;
- (std::shared_ptr<::yandex::maps::mapkit::guidance::SpeedingPolicy>)native;

@end
