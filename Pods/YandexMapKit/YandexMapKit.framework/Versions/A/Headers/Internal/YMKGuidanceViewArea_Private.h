#import <YandexMapKit/YMKGuidanceViewArea.h>

#import <yandex/maps/mapkit/guidance/guide.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::guidance::ViewArea, YMKGuidanceViewArea, void> {
    static ::yandex::maps::mapkit::guidance::ViewArea from(
        YMKGuidanceViewArea* platformViewArea);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::guidance::ViewArea, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKGuidanceViewArea*>::value>::type> {
    static ::yandex::maps::mapkit::guidance::ViewArea from(
        PlatformType platformViewArea)
    {
        return ToNative<::yandex::maps::mapkit::guidance::ViewArea, YMKGuidanceViewArea>::from(
            platformViewArea);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::guidance::ViewArea> {
    static YMKGuidanceViewArea* from(
        const ::yandex::maps::mapkit::guidance::ViewArea& viewArea);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
