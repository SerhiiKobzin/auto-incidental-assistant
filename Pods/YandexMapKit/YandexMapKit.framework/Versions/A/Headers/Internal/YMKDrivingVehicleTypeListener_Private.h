#import <YandexMapKit/YMKDrivingVehicleTypeListener.h>

#import <yandex/maps/mapkit/driving/driving_router.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace driving {
namespace ios {

class VehicleTypeListenerBinding : public ::yandex::maps::mapkit::driving::VehicleTypeListener {
public:
    explicit VehicleTypeListenerBinding(
        id<YMKDrivingVehicleTypeListener> platformListener);

    virtual void onVehicleTypeUpdated() override;

    id<YMKDrivingVehicleTypeListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKDrivingVehicleTypeListener> platformListener_;
};

} // namespace ios
} // namespace driving
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener>, id<YMKDrivingVehicleTypeListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener> from(
        id<YMKDrivingVehicleTypeListener> platformVehicleTypeListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener> from(
        PlatformType platformVehicleTypeListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener>, id<YMKDrivingVehicleTypeListener>>::from(
            platformVehicleTypeListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener>> {
    static id<YMKDrivingVehicleTypeListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::driving::VehicleTypeListener>& nativeVehicleTypeListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
