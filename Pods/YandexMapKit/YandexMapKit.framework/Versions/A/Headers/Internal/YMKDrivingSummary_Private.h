#import <YandexMapKit/YMKDrivingSummary.h>

#import <yandex/maps/mapkit/driving/route.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::driving::Summary, YMKDrivingSummary, void> {
    static ::yandex::maps::mapkit::driving::Summary from(
        YMKDrivingSummary* platformSummary);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::driving::Summary, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingSummary*>::value>::type> {
    static ::yandex::maps::mapkit::driving::Summary from(
        PlatformType platformSummary)
    {
        return ToNative<::yandex::maps::mapkit::driving::Summary, YMKDrivingSummary>::from(
            platformSummary);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::driving::Summary> {
    static YMKDrivingSummary* from(
        const ::yandex::maps::mapkit::driving::Summary& summary);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
