#import <YandexMapKit/YMKDrivingRouteSerializer.h>

#import <yandex/maps/mapkit/driving/driving_router.h>

#import <memory>

@interface YMKDrivingRouteSerializer ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::driving::RouteSerializer>)native;

- (::yandex::maps::mapkit::driving::RouteSerializer *)nativeRouteSerializer;

@end
