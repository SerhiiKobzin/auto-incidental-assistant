#import <YandexMapKit/YMKReviewsEntrySession.h>

#import <yandex/maps/mapkit/reviews/reviews_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace reviews {
namespace ios {

EntrySession::OnReviewsEntryReceived onReviewsEntryReceived(
    YMKReviewsEntrySessionDataHandler handler);
EntrySession::OnReviewsEntryError onReviewsEntryError(
    YMKReviewsEntrySessionDataHandler handler);

} // namespace ios
} // namespace reviews
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKReviewsEntrySession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::reviews::EntrySession>)native;

- (::yandex::maps::mapkit::reviews::EntrySession *)nativeEntrySession;

@end
