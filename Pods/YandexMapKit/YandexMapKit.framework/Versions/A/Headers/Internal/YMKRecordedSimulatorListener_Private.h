#import <YandexMapKit/YMKRecordedSimulatorListener.h>

#import <yandex/maps/mapkit/guidance/recorded_simulator.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace guidance {
namespace ios {

class RecordedSimulatorListenerBinding : public ::yandex::maps::mapkit::guidance::RecordedSimulatorListener {
public:
    explicit RecordedSimulatorListenerBinding(
        id<YMKRecordedSimulatorListener> platformListener);

    virtual void onLocationUpdated() override;

    virtual void onRouteUpdated() override;

    virtual void onProblemMark() override;

    virtual void onFinish() override;

    id<YMKRecordedSimulatorListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKRecordedSimulatorListener> platformListener_;
};

} // namespace ios
} // namespace guidance
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener>, id<YMKRecordedSimulatorListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener> from(
        id<YMKRecordedSimulatorListener> platformRecordedSimulatorListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener> from(
        PlatformType platformRecordedSimulatorListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener>, id<YMKRecordedSimulatorListener>>::from(
            platformRecordedSimulatorListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener>> {
    static id<YMKRecordedSimulatorListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::guidance::RecordedSimulatorListener>& nativeRecordedSimulatorListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
