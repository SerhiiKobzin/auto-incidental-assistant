#import <YandexMapKit/YMKTaxiRideInfoSession.h>

#import <yandex/maps/mapkit/taxi/taxi_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace taxi {
namespace ios {

RideInfoSession::OnRideInfoReceived onRideInfoReceived(
    YMKTaxiRideInfoSessionResponseHandler handler);
RideInfoSession::OnRideInfoError onRideInfoError(
    YMKTaxiRideInfoSessionResponseHandler handler);

} // namespace ios
} // namespace taxi
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKTaxiRideInfoSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::taxi::RideInfoSession>)native;

- (::yandex::maps::mapkit::taxi::RideInfoSession *)nativeRideInfoSession;

@end
