#import <YandexMapKit/YMKPhotosSession.h>

#import <yandex/maps/mapkit/photos/photos_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace photos {
namespace ios {

PhotoSession::OnPhotosFeedReceived onPhotosFeedReceived(
    YMKPhotosSessionDataHandler handler);
PhotoSession::OnPhotosFeedError onPhotosFeedError(
    YMKPhotosSessionDataHandler handler);

} // namespace ios
} // namespace photos
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKPhotosSession ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(std::unique_ptr<::yandex::maps::mapkit::photos::PhotoSession>)native;

- (::yandex::maps::mapkit::photos::PhotoSession *)nativePhotoSession;

@end
