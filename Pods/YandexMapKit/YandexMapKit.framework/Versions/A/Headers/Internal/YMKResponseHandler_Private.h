#import <YandexMapKit/YMKResponseHandler.h>

#import <yandex/maps/mapkit/search_layer/search_layer.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace search_layer {
namespace ios {

class SearchResultListenerBinding : public ::yandex::maps::mapkit::search_layer::SearchResultListener {
public:
    explicit SearchResultListenerBinding(
        id<YMKResponseHandler> platformListener);

    virtual void onSearchStart() override;

    virtual void onSearchSuccess() override;

    virtual void onSearchError(
        ::yandex::maps::runtime::Error* error) override;

    id<YMKResponseHandler> platformReference() const { return platformListener_; }

private:
    __weak id<YMKResponseHandler> platformListener_;
};

} // namespace ios
} // namespace search_layer
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener>, id<YMKResponseHandler>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener> from(
        id<YMKResponseHandler> platformSearchResultListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener> from(
        PlatformType platformSearchResultListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener>, id<YMKResponseHandler>>::from(
            platformSearchResultListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener>> {
    static id<YMKResponseHandler> from(
        const std::shared_ptr<::yandex::maps::mapkit::search_layer::SearchResultListener>& nativeSearchResultListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
