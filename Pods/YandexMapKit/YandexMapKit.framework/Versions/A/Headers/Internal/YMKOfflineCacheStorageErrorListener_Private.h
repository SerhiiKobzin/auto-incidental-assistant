#import <YandexMapKit/YMKOfflineCacheStorageErrorListener.h>

#import <yandex/maps/mapkit/offline_cache/storage_error_listener.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace offline_cache {
namespace ios {

class StorageErrorListenerBinding : public ::yandex::maps::mapkit::offline_cache::StorageErrorListener {
public:
    explicit StorageErrorListenerBinding(
        id<YMKOfflineCacheStorageErrorListener> platformListener);

    virtual void onStorageCorrupted() override;

    virtual void onStorageFull() override;

    virtual void onStorageAccessDenied() override;

    id<YMKOfflineCacheStorageErrorListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKOfflineCacheStorageErrorListener> platformListener_;
};

} // namespace ios
} // namespace offline_cache
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener>, id<YMKOfflineCacheStorageErrorListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener> from(
        id<YMKOfflineCacheStorageErrorListener> platformStorageErrorListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener> from(
        PlatformType platformStorageErrorListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener>, id<YMKOfflineCacheStorageErrorListener>>::from(
            platformStorageErrorListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener>> {
    static id<YMKOfflineCacheStorageErrorListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::offline_cache::StorageErrorListener>& nativeStorageErrorListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
