#import <YandexMapKit/YMKVehicleData.h>

#import <yandex/maps/mapkit/masstransit/vehicle_data.h>

#import <memory>

@interface YMKVehicleData ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::masstransit::VehicleData>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::masstransit::VehicleData>)nativeVehicleData;
- (std::shared_ptr<::yandex::maps::mapkit::masstransit::VehicleData>)native;

@end
