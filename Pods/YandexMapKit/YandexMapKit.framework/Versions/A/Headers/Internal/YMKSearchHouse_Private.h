#import <YandexMapKit/YMKSearchHouse.h>

#import <yandex/maps/mapkit/search/house.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::search::House, YMKSearchHouse, void> {
    static ::yandex::maps::mapkit::search::House from(
        YMKSearchHouse* platformHouse);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::search::House, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSearchHouse*>::value>::type> {
    static ::yandex::maps::mapkit::search::House from(
        PlatformType platformHouse)
    {
        return ToNative<::yandex::maps::mapkit::search::House, YMKSearchHouse>::from(
            platformHouse);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::search::House> {
    static YMKSearchHouse* from(
        const ::yandex::maps::mapkit::search::House& house);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
