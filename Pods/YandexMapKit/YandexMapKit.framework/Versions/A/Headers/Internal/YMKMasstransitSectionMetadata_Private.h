#import <YandexMapKit/YMKMasstransitSectionMetadata.h>

#import <yandex/maps/mapkit/masstransit/route.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_native_fwd.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>
#import <yandex/maps/runtime/bindings/ios/to_platform_fwd.h>

#import <type_traits>



namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData, id, void> {
    static ::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData from(
        id platformSectionData);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, id>::value>::type> {
    static ::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData from(
        PlatformType platformSectionData)
    {
        return ToNative<::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData, id>::from(
            platformSectionData);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData> {
    static id from(
        const ::yandex::maps::mapkit::masstransit::SectionMetadata::SectionData& sectionData);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
