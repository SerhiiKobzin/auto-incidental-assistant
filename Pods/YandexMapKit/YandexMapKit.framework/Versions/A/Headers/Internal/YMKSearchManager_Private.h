#import <YandexMapKit/YMKSearchManager.h>

#import <yandex/maps/mapkit/search/search_manager.h>

#import <memory>

namespace yandex {
namespace maps {
namespace mapkit {
namespace search {
namespace ios {

SearchManager::OnSuggestResponse onSuggestResponse(
    YMKSearchManagerResponseHandler handler);
SearchManager::OnSuggestError onSuggestError(
    YMKSearchManagerResponseHandler handler);

} // namespace ios
} // namespace search
} // namespace mapkit
} // namespace maps
} // namespace yandex

@interface YMKSearchManager ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::search::SearchManager>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::search::SearchManager>)nativeSearchManager;
- (std::shared_ptr<::yandex::maps::mapkit::search::SearchManager>)native;

@end
