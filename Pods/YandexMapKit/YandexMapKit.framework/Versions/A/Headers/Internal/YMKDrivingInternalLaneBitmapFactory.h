#import <UIKit/UIKit.h>

#import <YandexMapKit/YMKDrivingLane.h>

@interface YMKDrivingInternalLaneBitmapFactory : NSObject

+ (UIImage *)createLaneBitmapWithLaneSigns:(NSArray *)laneSigns;

@end
