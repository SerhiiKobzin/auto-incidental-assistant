#import <YandexMapKit/YMKPanoramaChangeDelegate.h>

#import <yandex/maps/mapkit/panorama/player.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace panorama {
namespace ios {

class PanoramaChangeListenerBinding : public ::yandex::maps::mapkit::panorama::PanoramaChangeListener {
public:
    explicit PanoramaChangeListenerBinding(
        id<YMKPanoramaChangeDelegate> platformListener);

    virtual void onPanoramaChanged(
        ::yandex::maps::mapkit::panorama::Player* player) override;

    id<YMKPanoramaChangeDelegate> platformReference() const { return platformListener_; }

private:
    __weak id<YMKPanoramaChangeDelegate> platformListener_;
};

} // namespace ios
} // namespace panorama
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener>, id<YMKPanoramaChangeDelegate>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener> from(
        id<YMKPanoramaChangeDelegate> platformPanoramaChangeListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener> from(
        PlatformType platformPanoramaChangeListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener>, id<YMKPanoramaChangeDelegate>>::from(
            platformPanoramaChangeListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener>> {
    static id<YMKPanoramaChangeDelegate> from(
        const std::shared_ptr<::yandex::maps::mapkit::panorama::PanoramaChangeListener>& nativePanoramaChangeListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
