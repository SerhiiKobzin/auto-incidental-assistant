#import <YandexMapKit/YMKDrivingSpot.h>

#import <yandex/maps/mapkit/driving/spot.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::driving::Spot, YMKDrivingSpot, void> {
    static ::yandex::maps::mapkit::driving::Spot from(
        YMKDrivingSpot* platformSpot);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::driving::Spot, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKDrivingSpot*>::value>::type> {
    static ::yandex::maps::mapkit::driving::Spot from(
        PlatformType platformSpot)
    {
        return ToNative<::yandex::maps::mapkit::driving::Spot, YMKDrivingSpot>::from(
            platformSpot);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::driving::Spot> {
    static YMKDrivingSpot* from(
        const ::yandex::maps::mapkit::driving::Spot& spot);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
