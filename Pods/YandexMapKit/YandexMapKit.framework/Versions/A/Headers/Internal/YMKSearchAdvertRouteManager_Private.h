#import <YandexMapKit/YMKSearchAdvertRouteManager.h>

#import <YandexRuntime/YRTSubscription.h>

#import <yandex/maps/mapkit/search/advert_manager.h>
#import <yandex/maps/runtime/ios/object.h>

#import <memory>

@interface YMKSearchAdvertRouteManager ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::search::AdvertRouteManager>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::search::AdvertRouteManager>)nativeAdvertRouteManager;
- (std::shared_ptr<::yandex::maps::mapkit::search::AdvertRouteManager>)native;

@end
