#import <YandexMapKit/YMKCarparksEventsExpiryListener.h>

#import <yandex/maps/mapkit/carparks/carparks_events_layer.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace mapkit {
namespace carparks {
namespace ios {

class CarparksEventsExpiryListenerBinding : public ::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener {
public:
    explicit CarparksEventsExpiryListenerBinding(
        id<YMKCarparksEventsExpiryListener> platformListener);

    virtual void onCarparksEventsExpired() override;

    id<YMKCarparksEventsExpiryListener> platformReference() const { return platformListener_; }

private:
    __weak id<YMKCarparksEventsExpiryListener> platformListener_;
};

} // namespace ios
} // namespace carparks
} // namespace mapkit
} // namespace maps
} // namespace yandex

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener>, id<YMKCarparksEventsExpiryListener>, void> {
    static std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener> from(
        id<YMKCarparksEventsExpiryListener> platformCarparksEventsExpiryListener);
};
template <typename PlatformType>
struct ToNative<std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener>, PlatformType> {
    static std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener> from(
        PlatformType platformCarparksEventsExpiryListener)
    {
        return ToNative<std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener>, id<YMKCarparksEventsExpiryListener>>::from(
            platformCarparksEventsExpiryListener);
    }
};

template <>
struct ToPlatform<std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener>> {
    static id<YMKCarparksEventsExpiryListener> from(
        const std::shared_ptr<::yandex::maps::mapkit::carparks::CarparksEventsExpiryListener>& nativeCarparksEventsExpiryListener);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
