#import <YandexMapKit/YMKSize.h>

#import <yandex/maps/mapkit/search_layer/assets_provider.h>
#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::mapkit::search_layer::Size, YMKSize, void> {
    static ::yandex::maps::mapkit::search_layer::Size from(
        YMKSize* platformSize);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::mapkit::search_layer::Size, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YMKSize*>::value>::type> {
    static ::yandex::maps::mapkit::search_layer::Size from(
        PlatformType platformSize)
    {
        return ToNative<::yandex::maps::mapkit::search_layer::Size, YMKSize>::from(
            platformSize);
    }
};

template <>
struct ToPlatform<::yandex::maps::mapkit::search_layer::Size> {
    static YMKSize* from(
        const ::yandex::maps::mapkit::search_layer::Size& size);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
