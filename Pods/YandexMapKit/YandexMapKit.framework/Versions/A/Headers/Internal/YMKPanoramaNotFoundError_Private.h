#import <YandexMapKit/YMKPanoramaNotFoundError.h>

#import <yandex/maps/mapkit/panorama/errors.h>

#import <memory>

@interface YMKPanoramaNotFoundError ()

- (id)initWithWrappedNative:(NSValue *)native;
- (id)initWithNative:(const std::shared_ptr<::yandex::maps::mapkit::panorama::NotFoundError>&)native;

- (std::shared_ptr<::yandex::maps::mapkit::panorama::NotFoundError>)nativeNotFoundError;

@end
