#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKMasstransitRoute;

typedef void(^YMKMasstransitSessionRouteHandler)(
    NSArray<YMKMasstransitRoute *> *routes,
    NSError *error);

/**
 * Handler for an async request for mass transit routes.
 */
@interface YMKMasstransitSession : YRTPlatformBinding

/**
 * Tries to cancel the current request for mass transit routes.
 */
- (void)cancel;


/**
 * Retries the request for mass transit routes using the specified
 * callback.
 */
- (void)retryWithRouteHandler:(nullable YMKMasstransitSessionRouteHandler)routeHandler;


@end

