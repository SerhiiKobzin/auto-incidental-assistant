#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKPolylinePosition.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@interface YMKPolylineUtils : YRTPlatformBinding

+ (nonnull NSArray<YMKPolylinePosition *> *)positionsOfForkWithFirstPolyline:(nonnull YMKPolyline *)firstPolyline
                                                       firstPolylinePosition:(nonnull YMKPolylinePosition *)firstPolylinePosition
                                                              secondPolyline:(nonnull YMKPolyline *)secondPolyline
                                                      secondPolylinePosition:(nonnull YMKPolylinePosition *)secondPolylinePosition;


+ (nonnull YMKPolylinePosition *)advancePolylinePositionWithPolyline:(nonnull YMKPolyline *)polyline
                                                            position:(nonnull YMKPolylinePosition *)position
                                                            distance:(double)distance;


+ (nonnull YMKPoint *)pointByPolylinePositionWithGeometry:(nonnull YMKPolyline *)geometry
                                                 position:(nonnull YMKPolylinePosition *)position;


@end
