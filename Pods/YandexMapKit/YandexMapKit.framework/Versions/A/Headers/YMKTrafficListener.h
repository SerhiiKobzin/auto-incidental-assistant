#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKTrafficColor) {

    YMKTrafficColorRed,

    YMKTrafficColorYellow,

    YMKTrafficColorGreen
};

