#import <Foundation/Foundation.h>

/**
 * @attention This feature is not available in the free MapKit version.
 */
@protocol YMKOfflineCacheStorageErrorListener <NSObject>

/**
 * Called if local storage is corrupted.
 */
- (void)onStorageCorrupted;


/**
 * Called if local storage is full.
 */
- (void)onStorageFull;


/**
 * Called if the application cannot get write access to local storage.
 */
- (void)onStorageAccessDenied;


@end
