#import <Foundation/Foundation.h>

/**
 * The list of layer parameters.
 */
@interface YMKLayersParams : NSObject

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSString *> *params;


+ (nonnull YMKLayersParams *)paramsWithParams:(nonnull NSDictionary<NSString *, NSString *> *)params;


@end

