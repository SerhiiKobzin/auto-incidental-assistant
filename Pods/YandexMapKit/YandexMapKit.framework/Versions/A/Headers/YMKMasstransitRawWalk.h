#import <YandexMapKit/YMKMasstransitRawConstructions.h>
#import <YandexMapKit/YMKSpotsContainer.h>

@interface YMKMasstransitRawWalk : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitRawConstructions *constructions;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSpotsContainer *spots;


+ (nonnull YMKMasstransitRawWalk *)rawWalkWithConstructions:(nullable YMKMasstransitRawConstructions *)constructions
                                                      spots:(nullable YMKSpotsContainer *)spots;


@end

