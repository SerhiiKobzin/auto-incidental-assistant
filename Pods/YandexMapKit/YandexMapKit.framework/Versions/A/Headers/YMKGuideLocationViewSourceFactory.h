#import <YandexMapKit/YMKLocationViewSource.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKGuidanceGuide;

@interface YMKGuideLocationViewSourceFactory : YRTPlatformBinding

/// @cond EXCLUDE
+ (nullable YMKLocationViewSource *)createLocationViewSourceWithGuide:(nullable YMKGuidanceGuide *)guide;
/// @endcond


@end
