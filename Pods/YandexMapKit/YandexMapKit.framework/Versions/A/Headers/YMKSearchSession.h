#import <YandexMapKit/YMKGeometry.h>
#import <YandexMapKit/YMKSearchBusinessFilter.h>
#import <YandexMapKit/YMKSearchOptions.h>
#import <YandexMapKit/YMKSearchResponse.h>

#import <YandexRuntime/YRTPlatformBinding.h>

typedef void(^YMKSearchSessionResponseHandler)(
    YMKSearchResponse *response,
    NSError *error);

/**
 * Interface denoting ongoing search session. Allows search cancellation
 * and retry.  For many request types allows further searches.
 */
@interface YMKSearchSession : YRTPlatformBinding

/**
 * Cancels the current request.
 */
- (void)cancel;


/**
 * Retries the last request. If there is an active request, it is
 * cancelled.
 *
 * @param searchListener Listener to handle search result.
 */
- (void)retryWithResponseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


/**
 * Check the availability of the next result page.
 *
 * @return True if there are more search results and one can call
 * YMKSearchSession::fetchNextPageWithResponseHandler:, false otherwise.
 */
- (BOOL)hasNextPage;


/**
 * Request the next page of search results. Ignored if the current
 * request isn't ready. Will throw if called when
 * YMKSearchSession::hasNextPage is false.
 *
 * @param searchListener Listener to handle search result.
 */
- (void)fetchNextPageWithResponseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


/**
 * Set or clear filters for future resubmits; Full filter set is
 * available in the first search response.
 *
 * @param filters Filter set for future resubmits.
 */
- (void)setFiltersWithFilters:(nonnull NSArray<YMKSearchBusinessFilter *> *)filters;


/**
 * Requests sorting by rank for future resubmits. Mutually exclusive
 * with YMKSearchSession::setSortByDistanceWithOrigin:.
 */
- (void)setSortByRank;


/**
 * Requests sorting by distance for future resubmits. Supported geometry
 * types: point, polyline. Mutually exclusive with
 * YMKSearchSession::setSortByRank.
 *
 * @param origin Origin to sort by distance from.
 */
- (void)setSortByDistanceWithOrigin:(nonnull YMKGeometry *)origin;


/**
 * Sets the search area for future resubmits. Supported geometry types:
 * bounding box, polyline, polygon. Polygon is expected to be a search
 * window: 4 points in outer ring (or 5 if last point is equal to first)
 * and no inner rings.
 *
 * @param area Search area for future resubmits.
 */
- (void)setSearchAreaWithArea:(nonnull YMKGeometry *)area;


/**
 * Set searchOptions for future resubmits.
 *
 * @param searchOptions Additional search parameters, see
 * YMKSearchOptions. Supported options: YMKSearchOptions::origin,
 * YMKSearchOptions::userPosition.
 */
- (void)setSearchOptionsWithSearchOptions:(nonnull YMKSearchOptions *)searchOptions;


/**
 * Redo the last search with currently set values of search area, search
 * options, filters, sort type and sort origin. Isn't applicable to
 * reverse geosearch and URI resolving. Ignored it the current request
 * is the first one; cancels current request otherwise.
 *
 * @param searchListener Listener to handle search result.
 */
- (void)resubmitWithResponseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


@end

