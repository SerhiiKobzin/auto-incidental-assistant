#import <YandexMapKit/YMKOfflineCacheRegionData.h>

/**
 * @attention This feature is not available in the free MapKit version.
 */
@interface YMKOfflineCacheRegionList : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKOfflineCacheRegionData *> *regions;

@property (nonatomic, readonly, nonnull) NSString *etag;


+ (nonnull YMKOfflineCacheRegionList *)regionListWithRegions:(nonnull NSArray<YMKOfflineCacheRegionData *> *)regions
                                                        etag:(nonnull NSString *)etag;


@end

