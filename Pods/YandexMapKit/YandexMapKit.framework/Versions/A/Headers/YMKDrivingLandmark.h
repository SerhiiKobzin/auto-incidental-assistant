#import <Foundation/Foundation.h>

/**
 * Information about an upcoming landmark.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingLandmark) {

    YMKDrivingLandmarkUnknown,

    YMKDrivingLandmarkAtTrafficLights,

    YMKDrivingLandmarkBeforeTrafficLights,

    YMKDrivingLandmarkBeforeBridge,

    YMKDrivingLandmarkBeforeTunnel,

    YMKDrivingLandmarkAfterBridge,

    YMKDrivingLandmarkAfterTunnel,

    YMKDrivingLandmarkToBridge,

    YMKDrivingLandmarkIntoTunnel,

    YMKDrivingLandmarkIntoCourtyard,

    YMKDrivingLandmarkToFrontageRoad
};

