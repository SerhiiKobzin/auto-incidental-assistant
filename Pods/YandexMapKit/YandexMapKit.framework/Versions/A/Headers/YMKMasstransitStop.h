#import <Foundation/Foundation.h>

/**
 * Describes a public transport stop.
 */
@interface YMKMasstransitStop : NSObject

/**
 * Stop ID.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Stop name.
 */
@property (nonatomic, readonly, nonnull) NSString *name;


+ (nonnull YMKMasstransitStop *)stopWithId:(nonnull NSString *)id
                                      name:(nonnull NSString *)name;


@end

