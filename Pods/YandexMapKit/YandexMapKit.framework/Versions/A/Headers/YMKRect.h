#import <UIKit/UIKit.h>

@interface YMKRect : NSObject

@property (nonatomic, readonly) CGPoint min;

@property (nonatomic, readonly) CGPoint max;


+ (nonnull YMKRect *)rectWithMin:( CGPoint)min
                             max:( CGPoint)max;


@end

