#import <YandexMapKit/YMKSpot.h>

/**
 * Defines a vector of points on a pedestrian path.
 */
@interface YMKSpotsContainer : NSObject

/**
 * Vector of points on a pedestrian path.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSpot *> *spots;


+ (nonnull YMKSpotsContainer *)spotsContainerWithSpots:(nonnull NSArray<YMKSpot *> *)spots;


@end

