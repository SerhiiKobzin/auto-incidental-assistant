#import <Foundation/Foundation.h>

/**
 * Overall characteristics of the route.
 */
@interface YMKDrivingFlags : NSObject

@property (nonatomic, readonly) BOOL blocked;

@property (nonatomic, readonly) BOOL hasFerries;

@property (nonatomic, readonly) BOOL hasTolls;

@property (nonatomic, readonly) BOOL guidanceForbidden;

@property (nonatomic, readonly) BOOL crossesBorders;

@property (nonatomic, readonly) BOOL requiresAccessPass;

@property (nonatomic, readonly) BOOL forParking;

@property (nonatomic, readonly) BOOL futureBlocked;

@property (nonatomic, readonly) BOOL deadJam;

@property (nonatomic, readonly) BOOL builtOffline;

@property (nonatomic, readonly) BOOL predicted;


+ (nonnull YMKDrivingFlags *)flagsWithBlocked:( BOOL)blocked
                                   hasFerries:( BOOL)hasFerries
                                     hasTolls:( BOOL)hasTolls
                            guidanceForbidden:( BOOL)guidanceForbidden
                               crossesBorders:( BOOL)crossesBorders
                           requiresAccessPass:( BOOL)requiresAccessPass
                                   forParking:( BOOL)forParking
                                futureBlocked:( BOOL)futureBlocked
                                      deadJam:( BOOL)deadJam
                                 builtOffline:( BOOL)builtOffline
                                    predicted:( BOOL)predicted;


@end

