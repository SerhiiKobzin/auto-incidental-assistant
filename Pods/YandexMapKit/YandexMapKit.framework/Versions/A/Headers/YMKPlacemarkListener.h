#import <Foundation/Foundation.h>

@class YMKSearchResultItem;

@protocol YMKPlacemarkListener <NSObject>

- (BOOL)onTapWithSearchResultItem:(nullable YMKSearchResultItem *)searchResultItem;


@end
