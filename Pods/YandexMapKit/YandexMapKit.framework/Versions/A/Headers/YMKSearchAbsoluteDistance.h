#import <YandexMapKit/YMKSearchTransitInfo.h>
#import <YandexMapKit/YMKSearchTravelInfo.h>

/**
 * Describes absolute distance info.
 */
@interface YMKSearchAbsoluteDistance : NSObject

/**
 * Travel info for driving route.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchTravelInfo *driving;

/**
 * Travel info for walking route.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchTravelInfo *walking;

/**
 * Travel info for masstransit route.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchTransitInfo *transit;


+ (nonnull YMKSearchAbsoluteDistance *)absoluteDistanceWithDriving:(nullable YMKSearchTravelInfo *)driving
                                                           walking:(nullable YMKSearchTravelInfo *)walking
                                                           transit:(nullable YMKSearchTransitInfo *)transit;


@end

