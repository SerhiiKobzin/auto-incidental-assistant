#import <YandexMapKit/YMKRoadEventsRoadEvents.h>
#import <YandexMapKit/YMKTrafficDelegate.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@interface YMKTrafficLayer : YRTPlatformBinding

- (BOOL)isTrafficVisible;


- (void)setTrafficVisibleWithOn:(BOOL)on;


/**
 * Applies JSON style transformations to the traffic layer. Set to null
 * to clear any previous custom styling. Returns true if the style was
 * successfully parsed; false otherwise. If the return value is false,
 * the current traffic style remains unchanged.
 */
- (BOOL)setTrafficStyleWithStyle:(nonnull NSString *)style;


- (BOOL)isRoadEventVisibleWithType:(YMKRoadEventsEventType)type;


- (void)setRoadEventVisibleWithType:(YMKRoadEventsEventType)type
                                 on:(BOOL)on;


- (void)addTrafficListenerWithTrafficListener:(nullable id<YMKTrafficDelegate>)trafficListener;


- (void)removeTrafficListenerWithTrafficListener:(nullable id<YMKTrafficDelegate>)trafficListener;


/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

