#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKSearchAddress.h>
#import <YandexMapKit/YMKSearchDataTypes.h>

/**
 * Additional data for toponym objects.
 */
@interface YMKSearchToponymObjectMetadata : NSObject

/**
 * Structured toponym address
 */
@property (nonatomic, readonly, nonnull) YMKSearchAddress *address;

/**
 * Toponym precision.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *precision;

/**
 * Former name for toponym if any.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *formerName;

/**
 * Point where balloon for the toponym should be shown. Differs for
 * direct and reverse search modes: Direct mode -- toponym center.
 * Reverse mode -- toponym nearest point to the given coordinates.
 */
@property (nonatomic, readonly, nonnull) YMKPoint *balloonPoint;

/**
 * Persistent toponym id (available for Yandex-owned regions).
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *id;


+ (nonnull YMKSearchToponymObjectMetadata *)toponymObjectMetadataWithAddress:(nonnull YMKSearchAddress *)address
                                                                   precision:(nullable NSNumber *)precision
                                                                  formerName:(nullable NSString *)formerName
                                                                balloonPoint:(nonnull YMKPoint *)balloonPoint
                                                                          id:(nullable NSString *)id;


@end

