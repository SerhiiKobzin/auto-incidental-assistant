#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * Listener to be notified when new route advertisement is received
 */
@protocol YMKSearchAdvertRouteListener <NSObject>

/**
 * Method to be called on listener to notify of new ad.
 */
- (void)onRouteAdvertReceived;


@end
/// @endcond
