#import <YandexMapKit/YMKAtomEntry.h>
#import <YandexMapKit/YMKPhotosImage.h>

/// @cond EXCLUDE
/**
 * Photo info with atom metainfo. Contains images of several sizes. The
 * image with ORIG size is always present.
 */
@interface YMKPhotosEntry : NSObject

/**
 * Atom part of the entry.
 */
@property (nonatomic, readonly, nonnull) YMKAtomEntry *atomEntry;

/**
 * Different size images of this photo.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKPhotosImage *> *images;

/**
 * Tags for this photo.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *tags;

/**
 * Flag for images that are still under moderation.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *pending;


+ (nonnull YMKPhotosEntry *)entryWithAtomEntry:(nonnull YMKAtomEntry *)atomEntry
                                        images:(nonnull NSArray<YMKPhotosImage *> *)images
                                          tags:(nonnull NSArray<NSString *> *)tags
                                       pending:(nullable NSNumber *)pending;


@end
/// @endcond

