#import <YandexMapKit/YMKSearchAbsoluteDistance.h>
#import <YandexMapKit/YMKSearchRelativeDistance.h>

/**
 * Snippet data to get route distance info.
 */
@interface YMKSearchRouteDistancesObjectMetadata : NSObject

/**
 * Absolute distance info.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchAbsoluteDistance *absolute;

/**
 * Relative distance info.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchRelativeDistance *relative;


+ (nonnull YMKSearchRouteDistancesObjectMetadata *)routeDistancesObjectMetadataWithAbsolute:(nullable YMKSearchAbsoluteDistance *)absolute
                                                                                   relative:(nullable YMKSearchRelativeDistance *)relative;


@end

