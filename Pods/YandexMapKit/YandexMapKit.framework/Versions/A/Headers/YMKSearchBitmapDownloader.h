#import <YandexMapKit/YMKBitmapSession.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
/**
 * Allows to download advert bitmap images.
 */
@interface YMKSearchBitmapDownloader : YRTPlatformBinding

/**
 * Requests an image to display.
 *
 * @param id Image identifier.
 * @param scale Scale of resulting image.
 * @param bitmapListener Receive resulting image using this listener.
 *
 * @return Session handle that should be stored until image is received.
 */
- (nullable YMKBitmapSession *)requestBitmapWithId:(nonnull NSString *)id
                                             scale:(float)scale
                                    bitmapListener:(nullable YMKBitmapSessionBitmapListener)bitmapListener;


@end
/// @endcond

