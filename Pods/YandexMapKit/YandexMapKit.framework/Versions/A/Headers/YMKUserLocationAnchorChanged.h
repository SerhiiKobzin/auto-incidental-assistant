#import <YandexMapKit/YMKObjectEvent.h>
#import <YandexMapKit/YMKUserLocation.h>

/**
 * Event for a change in the user location icon anchor.
 */
@interface YMKUserLocationAnchorChanged : YMKObjectEvent

@property (nonatomic, readonly) YMKUserLocationAnchorType anchorType;

@end

