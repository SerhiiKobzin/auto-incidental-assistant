#import <YandexMapKit/YMKDrivingAnnotationLang.h>
#import <YandexMapKit/YMKDrivingDrivingOptions.h>
#import <YandexMapKit/YMKDrivingRequestPoint.h>
#import <YandexMapKit/YMKDrivingSession.h>
#import <YandexMapKit/YMKDrivingSummarySession.h>
#import <YandexMapKit/YMKDrivingVehicleType.h>
#import <YandexMapKit/YMKDrivingVehicleTypeListener.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolylinePosition.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKDrivingRoute;
@class YMKDrivingRouteSerializer;

@interface YMKDrivingRouter : YRTPlatformBinding

- (nullable YMKDrivingSession *)requestRoutesWithPoints:(nonnull NSArray<YMKDrivingRequestPoint *> *)points
                                         drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                           routeHandler:(nullable YMKDrivingSessionRouteHandler)routeHandler;


- (nullable YMKDrivingSummarySession *)requestRoutesSummaryWithPoints:(nonnull NSArray<YMKDrivingRequestPoint *> *)points
                                                       drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                                       summaryHandler:(nullable YMKDrivingSummarySessionSummaryHandler)summaryHandler;


- (nullable YMKDrivingSession *)requestAlternativesForRouteWithRoute:(nullable YMKDrivingRoute *)route
                                                       routePosition:(nonnull YMKPolylinePosition *)routePosition
                                                      drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                                        routeHandler:(nullable YMKDrivingSessionRouteHandler)routeHandler;


/// @cond EXCLUDE
/**
 *
 *
 * Remark:
 * @param finish has optional type, it may be uninitialized.
 */
- (nullable YMKDrivingSession *)requestParkingRoutesWithLocation:(nonnull YMKPoint *)location
                                                          finish:(nullable YMKPoint *)finish
                                                  drivingOptions:(nonnull YMKDrivingDrivingOptions *)drivingOptions
                                                    routeHandler:(nullable YMKDrivingSessionRouteHandler)routeHandler;
/// @endcond


- (nullable YMKDrivingRouteSerializer *)routeSerializer;


- (void)setAnnotationLanguageWithAnnotationLanguage:(YMKDrivingAnnotationLanguage)annotationLanguage;


/**
 * @attention This feature is not available in the free MapKit version.
 *
 *
 * Enables or disables offline routing. Disabled by default.
 */
- (void)setOfflineRoutingEnabledWithOn:(BOOL)on;


/// @cond EXCLUDE
/**
 * Sets the vehicle type for special routing.
 */
- (void)setVehicleTypeWithVehicleType:(YMKDrivingVehicleType)vehicleType;
/// @endcond


/// @cond EXCLUDE
- (YMKDrivingVehicleType)vehicleType;
/// @endcond


/// @cond EXCLUDE
- (void)addVehicleTypeListenerWithVehicleTypeListener:(nullable id<YMKDrivingVehicleTypeListener>)vehicleTypeListener;
/// @endcond


/// @cond EXCLUDE
- (void)removeVehicleTypeListenerWithVehicleTypeListener:(nullable id<YMKDrivingVehicleTypeListener>)vehicleTypeListener;
/// @endcond


/**
 * Suspends all conditions in the request for routes received by this
 * DrivingRouter.
 */
- (void)suspend;


/**
 * Resumes all conditions in the request for routes received by this
 * DrivingRouter. This is the default state.
 */
- (void)resume;


@end

