#import <YandexMapKit/YMKDrivingLane.h>
#import <YandexMapKit/YMKPolylinePosition.h>

/**
 * The sign showing the lane.
 */
@interface YMKDrivingLaneSign : NSObject

@property (nonatomic, readonly, nonnull) YMKPolylinePosition *position;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingLane *> *lanes;


+ (nonnull YMKDrivingLaneSign *)laneSignWithPosition:(nonnull YMKPolylinePosition *)position
                                               lanes:(nonnull NSArray<YMKDrivingLane *> *)lanes;


@end

