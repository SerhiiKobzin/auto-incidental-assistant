#import <YandexMapKit/YMKSearchCurrencyExchangeType.h>

/**
 * Currency exchange snippet.
 */
@interface YMKSearchCurrencyExchangeMetadata : NSObject

/**
 * Available currency exchange rates.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchCurrencyExchangeType *> *currencies;


+ (nonnull YMKSearchCurrencyExchangeMetadata *)currencyExchangeMetadataWithCurrencies:(nonnull NSArray<YMKSearchCurrencyExchangeType *> *)currencies;


@end

