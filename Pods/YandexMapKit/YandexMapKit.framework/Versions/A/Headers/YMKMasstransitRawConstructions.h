#import <YandexMapKit/YMKMasstransitRawConstruction.h>

@interface YMKMasstransitRawConstructions : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitRawConstruction *> *constructions;


+ (nonnull YMKMasstransitRawConstructions *)rawConstructionsWithConstructions:(nonnull NSArray<YMKMasstransitRawConstruction *> *)constructions;


@end

