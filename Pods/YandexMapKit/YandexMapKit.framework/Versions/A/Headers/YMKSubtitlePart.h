#import <YandexMapKit/YMKSubtitleParts.h>

/**
 * Subtitle part describes the specific attribute of the organization
 */
@interface YMKSubtitlePart : NSObject

/**
 * Type of the subtitle part
 */
@property (nonatomic, readonly) YMKSubtitlePartType type;

/**
 * Text to display
 */
@property (nonatomic, readonly, nonnull) NSString *text;

/**
 * Subtitle tag (optional)
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *tag;


+ (nonnull YMKSubtitlePart *)subtitlePartWithType:( YMKSubtitlePartType)type
                                             text:(nonnull NSString *)text
                                              tag:(nullable NSNumber *)tag;


@end

