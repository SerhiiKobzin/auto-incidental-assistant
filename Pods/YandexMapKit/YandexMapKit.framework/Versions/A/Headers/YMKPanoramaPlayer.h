#import <YandexMapKit/YMKDirection.h>
#import <YandexMapKit/YMKLogo.h>
#import <YandexMapKit/YMKPanoramaChangeDelegate.h>
#import <YandexMapKit/YMKPanoramaDirectionChangeDelegate.h>
#import <YandexMapKit/YMKPanoramaErrorDelegate.h>
#import <YandexMapKit/YMKPanoramaSpanChangeDelegate.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKSpan.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@interface YMKPanoramaPlayer : YRTPlatformBinding

/**
 * Opens the panorama with the given ID.
 *
 * @param panoramaId The panoramaId that specifies the panorama to open.
 * You can get the panoramaId by using the PanoramaService methods.
 */
- (void)openPanoramaWithPanoramaId:(nonnull NSString *)panoramaId;


/**
 * Opened the panorama with the given ID.
 *
 * @return PanoramaId of the currently opened panorama. Empty if no
 * panorama is open.
 */
- (nonnull NSString *)panoramaId;


/**
 * Geo position of current panorama.
 *
 * @return Geo position of the currently opened panorama. Empty if no
 * panorama is open.
 */
- (nonnull YMKPoint *)position;


/**
 * Sets the view direction to the center of the given geo position.
 *
 * @param position The position to look at.
 */
- (void)lookAtWithPosition:(nonnull YMKPoint *)position;


/**
 * View direction of the opened panorama.
 *
 * @return View direction of the opened panorama. Empty if no panorama
 * is open.
 */
- (nonnull YMKDirection *)direction;


/**
 * Sets the view direction bearing and tilt.
 *
 * @param direction View direction.
 */
- (void)setDirectionWithDirection:(nonnull YMKDirection *)direction;


/**
 * View span of the opened panorama.
 *
 * @return Current view span of the opened panorama. May be different
 * from the span provided by the setSpan(span) method.
 */
- (nonnull YMKSpan *)span;


/**
 * Sets the view area span. Invalid values are adjusted by the player to
 * the closest valid values.
 *
 * @param span View area span. May be adjusted by the player.
 */
- (void)setSpanWithSpan:(nonnull YMKSpan *)span;


/**
 * Closes the opened panorama and stops all player actions.
 */
- (void)reset;


/**
 * Enables player zoom controls.
 */
- (void)enableZoom;


/**
 * Disables player zoom controls.
 */
- (void)disableZoom;


/**
 * Checks if zoom controls are enabled.
 */
- (BOOL)zoomEnabled;


/**
 * Shows transition arrows and allows switching panoramas.
 */
- (void)enableMove;


/**
 * Hides transition arrows and disallows switching panoramas.
 */
- (void)disableMove;


/**
 * Checks if switching panoramas is enabled.
 */
- (BOOL)moveEnabled;


/**
 * Allows the user to rotate panoramas.
 */
- (void)enableRotation;


/**
 * Disallows the user to rotate panoramas.
 */
- (void)disableRotation;


/**
 * Checks if player rotation is enabled.
 */
- (BOOL)rotationEnabled;


/**
 * Allows markers (house numbers, railway stations, airports) to be
 * shown.
 */
- (void)enableMarkers;


/**
 * Disallows markers (house numbers, railway stations, airports) to be
 * shown.
 */
- (void)disableMarkers;


/**
 * Checks if markers are enabled.
 */
- (BOOL)markersEnabled;


/**
 * Adds a panorama change listener.
 *
 * @param panoramaChangeListener Panorama change listener.
 */
- (void)addPanoramaChangeListenerWithPanoramaChangeListener:(nullable id<YMKPanoramaChangeDelegate>)panoramaChangeListener;


- (void)removePanoramaChangeListenerWithPanoramaChangeListener:(nullable id<YMKPanoramaChangeDelegate>)panoramaChangeListener;


/**
 * Adds direction listener.
 *
 * @param directionChangeListener Panorama direction listener.
 */
- (void)addDirectionChangeListenerWithDirectionChangeListener:(nullable id<YMKPanoramaDirectionChangeDelegate>)directionChangeListener;


- (void)removeDirectionChangeListenerWithDirectionChangeListener:(nullable id<YMKPanoramaDirectionChangeDelegate>)directionChangeListener;


/**
 * Adds span listener.
 *
 * @param spanChangeListener Panorama span listener.
 */
- (void)addSpanChangeListenerWithSpanChangeListener:(nullable id<YMKPanoramaSpanChangeDelegate>)spanChangeListener;


- (void)removeSpanChangeListenerWithSpanChangeListener:(nullable id<YMKPanoramaSpanChangeDelegate>)spanChangeListener;


/**
 * Adds error listener.
 *
 * @param errorListener Listeners that notify when a panorama failed to
 * open.
 */
- (void)addErrorListenerWithErrorListener:(nullable id<YMKPanoramaErrorDelegate>)errorListener;


- (void)removeErrorListenerWithErrorListener:(nullable id<YMKPanoramaErrorDelegate>)errorListener;


/**
 * Allows showing loading wheels.
 */
- (void)enableLoadingWheel;


/**
 * Disallows showing loading wheels.
 */
- (void)disableLoadingWheel;


/**
 * Checks if loading wheels can be shown while the panorama is opening.
 */
- (BOOL)loadingWheelEnabled;


/**
 * Yandex logo object.
 */
@property (nonatomic, readonly, nullable) YMKLogo *logo;

/**
 * Called when a memory warning happens.
 */
- (void)onMemoryWarning;


/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

