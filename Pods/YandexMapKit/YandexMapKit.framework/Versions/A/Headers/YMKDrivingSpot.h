#import <YandexMapKit/YMKPolylinePosition.h>

typedef NS_ENUM(NSUInteger, YMKDrivingSpotType) {

    YMKDrivingSpotTypeUnknown,

    YMKDrivingSpotTypeAccessPassRestriction
};


@interface YMKDrivingSpot : NSObject

@property (nonatomic, readonly) YMKDrivingSpotType type;

@property (nonatomic, readonly, nonnull) YMKPolylinePosition *position;


+ (nonnull YMKDrivingSpot *)spotWithType:( YMKDrivingSpotType)type
                                position:(nonnull YMKPolylinePosition *)position;


@end

