#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKSearchAdvertMenuInfo.h>
#import <YandexMapKit/YMKSearchAdvertMenuListener.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
/**
 * An interface for receiving advertised menu items for the application
 * menu. Application sets the user position and the relevant menu
 * adverts are provided. Ads are received in the background, the
 * application is notified when new menu adverts become available.
 */
@interface YMKSearchAdvertMenuManager : YRTPlatformBinding

/**
 * Add listener to receive menu adverts. Listener will be notified after
 * calls to setPosition.
 *
 * @param menuListener listener to add
 */
- (void)addListenerWithMenuListener:(nullable id<YMKSearchAdvertMenuListener>)menuListener;


/**
 * Remove menu adverts listener.
 *
 * @param menuListener listener to remove
 */
- (void)removeListenerWithMenuListener:(nullable id<YMKSearchAdvertMenuListener>)menuListener;


/**
 * Set current position.
 *
 * @param point new position value
 */
- (void)setPositionWithPoint:(nonnull YMKPoint *)point;


/**
 * Currently available menu adverts.
 *
 * @return collection of menu advertisment items
 */
@property (nonatomic, readonly, nonnull) YMKSearchAdvertMenuInfo *advertMenuInfo;

@end
/// @endcond

