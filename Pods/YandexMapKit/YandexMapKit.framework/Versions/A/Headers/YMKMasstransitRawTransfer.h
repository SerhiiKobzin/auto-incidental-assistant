#import <YandexMapKit/YMKMasstransitRawConstructions.h>

@interface YMKMasstransitRawTransfer : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKMasstransitRawConstructions *constructions;


+ (nonnull YMKMasstransitRawTransfer *)rawTransferWithConstructions:(nullable YMKMasstransitRawConstructions *)constructions;


@end

