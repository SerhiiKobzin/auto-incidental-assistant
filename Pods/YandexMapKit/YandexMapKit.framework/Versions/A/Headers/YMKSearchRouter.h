#import <Foundation/Foundation.h>

/**
 * Router type.
 */
typedef NS_ENUM(NSUInteger, YMKSearchRouterType) {

    /**
     * Driving router.
     */
    YMKSearchRouterTypeAuto,

    /**
     * Masstransit router.
     */
    YMKSearchRouterTypeMassTransit
};


/**
 * Router info.
 */
@interface YMKSearchRouter : NSObject

/**
 * List of router types.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *types;


+ (nonnull YMKSearchRouter *)routerWithTypes:(nonnull NSArray<NSNumber *> *)types;


@end

