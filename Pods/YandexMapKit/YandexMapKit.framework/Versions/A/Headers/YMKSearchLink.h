#import <YandexMapKit/YMKAttribution.h>

/**
 * Link type.
 */
typedef NS_ENUM(NSUInteger, YMKSearchLinkType) {

    /**
     * Unknown link type.
     */
    YMKSearchLinkTypeUnknown,

    /**
     * Personal page.
     */
    YMKSearchLinkTypeSelf,

    /**
     * Page on social network.
     */
    YMKSearchLinkTypeSocial,

    /**
     * Business info on partner resource.
     */
    YMKSearchLinkTypeAttribution,

    /**
     * Direct link with time tables.
     */
    YMKSearchLinkTypeShowtimes,

    /**
     * Direct link for booking.
     */
    YMKSearchLinkTypeBooking
};


/**
 * Link structure. Combines actual link with attribution and type info.
 */
@interface YMKSearchLink : NSObject

/**
 * Attribution reference.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *aref;

/**
 * Link type, see YMKSearchLinkType, obsolete.
 */
@property (nonatomic, readonly) YMKSearchLinkType type;

/**
 * Actual link.
 */
@property (nonatomic, readonly, nonnull) YMKAttributionLink *link;

/**
 * Link tag (to use instead of type).
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *tag;


+ (nonnull YMKSearchLink *)linkWithAref:(nullable NSString *)aref
                                   type:( YMKSearchLinkType)type
                                   link:(nonnull YMKAttributionLink *)link
                                    tag:(nullable NSString *)tag;


@end

