#import <YandexRuntime/YRTError.h>

/**
 * @attention This feature is not available in the free MapKit version.
 */
@interface YMKOfflineCacheRegionOutdatedError : YRTError

@end

