#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKSearchDataTypes.h>

/**
 * Struct to fine-tune search request.
 */
@interface YMKSearchOptions : NSObject

/**
 * The search type can be one of YMKSearchType values or their bitwise
 * 'OR' combination. If searchType is not initialized, it means to
 * search in all the sources.
 */
@property (nonatomic, assign) YMKSearchType searchTypes;

/**
 * Maximum number of search results per page.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *resultPageSize;

/**
 * Snippets that will be requested. The value should be one of
 * YMKSearchSnippet, or their bitwise 'OR' combination.
 */
@property (nonatomic, assign) YMKSearchSnippet snippets;

/**
 * The server uses the user position to calculate the distance from the
 * user to search results.
 *
 * Optional property, can be null.
 */
@property (nonatomic, strong) YMKPoint *userPosition;

/**
 * String that sets an identifier for the request source.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *origin;

/**
 * The landing page ID for Yandex.Direct. If set, the banners may be
 * present in the search response.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *directPageId;

/**
 * Statistics ID for Yandex.Direct.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *directStatId;

/**
 * The context from an Apple-directed session.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *appleCtx;

/**
 * Enables searching for all businesses. Closed companies and companies
 * from unreliable sources will be included.
 */
@property (nonatomic, assign) BOOL searchClosed;

/**
 * Adds the geometry to the server response.
 */
@property (nonatomic, assign) BOOL geometry;

/**
 * Maximum number of advertised business objects in the response.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *maxAdverts;

/**
 * The landing page ID for ads. If set, each ad will contain
 * displayClick and searchClick YABS counters.
 *
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *advertPageId;

/**
 * Enable word-by-word suggestion items.
 */
@property (nonatomic, assign) BOOL suggestWords;

+ (nonnull YMKSearchOptions *)searchOptionsWithSearchTypes:( YMKSearchType)searchTypes
                                            resultPageSize:(nullable NSNumber *)resultPageSize
                                                  snippets:( YMKSearchSnippet)snippets
                                              userPosition:(nullable YMKPoint *)userPosition
                                                    origin:(nullable NSString *)origin
                                              directPageId:(nullable NSString *)directPageId
                                              directStatId:(nullable NSString *)directStatId
                                                  appleCtx:(nullable NSString *)appleCtx
                                              searchClosed:( BOOL)searchClosed
                                                  geometry:( BOOL)geometry
                                                maxAdverts:(nullable NSNumber *)maxAdverts
                                              advertPageId:(nullable NSString *)advertPageId
                                              suggestWords:( BOOL)suggestWords;


@end
