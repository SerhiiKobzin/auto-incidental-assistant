#import <YandexMapKit/YMKTaxiRideInfo.h>

#import <YandexRuntime/YRTNotFoundError.h>
#import <YandexRuntime/YRTPlatformBinding.h>
#import <YandexRuntime/YRTRemoteError.h>

/// @cond EXCLUDE
typedef void(^YMKTaxiRideInfoSessionResponseHandler)(
    YMKTaxiRideInfo *rideInfo,
    NSError *error);

/**
 * Session for requesting taxi ride info
 */
@interface YMKTaxiRideInfoSession : YRTPlatformBinding

/**
 * Error can be one of the following: YRTNotFoundError,
 * {@runtime.network.NetworkError}, YRTRemoteError
 */
- (void)retryWithResponseHandler:(nullable YMKTaxiRideInfoSessionResponseHandler)responseHandler;


- (void)cancel;


@end
/// @endcond

