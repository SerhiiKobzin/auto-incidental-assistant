#import <YandexMapKit/YMKDrivingAnnotation.h>
#import <YandexMapKit/YMKDrivingWeight.h>

@interface YMKDrivingSectionMetadata : NSObject

/**
 * A leg is a section of the route between two consecutive waypoints.
 */
@property (nonatomic, readonly) NSUInteger legIndex;

@property (nonatomic, readonly, nonnull) YMKDrivingWeight *weight;

@property (nonatomic, readonly, nonnull) YMKDrivingAnnotation *annotation;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *schemeId;

/**
 * Throughpoints can appear only at nodes of the section's geometry. The
 * vector contains the positions of all corresponding nodes. These
 * positions should be listed in ascending order.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *viaPointPositions;


+ (nonnull YMKDrivingSectionMetadata *)sectionMetadataWithLegIndex:( NSUInteger)legIndex
                                                            weight:(nonnull YMKDrivingWeight *)weight
                                                        annotation:(nonnull YMKDrivingAnnotation *)annotation
                                                          schemeId:(nullable NSNumber *)schemeId
                                                 viaPointPositions:(nonnull NSArray<NSNumber *> *)viaPointPositions;


@end

