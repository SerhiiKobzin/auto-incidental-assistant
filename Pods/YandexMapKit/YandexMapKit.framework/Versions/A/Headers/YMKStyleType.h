#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * The style of the displayed map.
 */
typedef NS_ENUM(NSUInteger, YMKStyleType) {

    /**
     * Default style for mobile maps applications.
     */
    YMKStyleTypeDefault,

    /**
     * Style for Yandex.Navigator.
     */
    YMKStyleTypeNavi,

    /**
     * Old Yandex.Navigator style.
     */
    YMKStyleTypeLegacyNavi
};
/// @endcond

