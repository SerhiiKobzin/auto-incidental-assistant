#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, YMKLogoHorizontalAlignment) {

    YMKLogoHorizontalAlignmentLeft,

    YMKLogoHorizontalAlignmentCenter,

    YMKLogoHorizontalAlignmentRight
};


typedef NS_ENUM(NSUInteger, YMKLogoVerticalAlignment) {

    YMKLogoVerticalAlignmentTop,

    YMKLogoVerticalAlignmentBottom
};


@interface YMKLogoAlignment : NSObject

@property (nonatomic, readonly) YMKLogoHorizontalAlignment horizontalAlignment;

@property (nonatomic, readonly) YMKLogoVerticalAlignment verticalAlignment;


+ (nonnull YMKLogoAlignment *)alignmentWithHorizontalAlignment:( YMKLogoHorizontalAlignment)horizontalAlignment
                                             verticalAlignment:( YMKLogoVerticalAlignment)verticalAlignment;


@end

