#import <YandexMapKit/YMKMasstransitThread.h>
#import <YandexMapKit/YMKMasstransitThreadInfo.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKMasstransitThreadSessionThreadHandler)(
    YMKMasstransitThreadInfo *threadInfo,
    NSError *error);

/**
 * Handler for a masstransit YMKMasstransitThread async request.
 */
@interface YMKMasstransitThreadSession : YRTPlatformBinding

/**
 * Tries to cancel the current mass transit thread request.
 */
- (void)cancel;


/**
 * Retries the mass transit thread request using the specified callback.
 */
- (void)retryWithThreadHandler:(nullable YMKMasstransitThreadSessionThreadHandler)threadHandler;


@end
/// @endcond

