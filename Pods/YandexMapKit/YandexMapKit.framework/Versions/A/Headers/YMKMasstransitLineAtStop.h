#import <YandexMapKit/YMKMasstransitLine.h>
#import <YandexMapKit/YMKMasstransitThreadAtStop.h>

/// @cond EXCLUDE
/**
 * @brief Contains information about the mass transit line and the
 * collection of mass transit threads on the line that go through the
 * specified stop.
 */
@interface YMKMasstransitLineAtStop : NSObject

/**
 * Mass transit line.
 */
@property (nonatomic, readonly, nonnull) YMKMasstransitLine *line;

/**
 * @brief Information about mass transit threads of the specified line
 * which go through the specified stop.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKMasstransitThreadAtStop *> *threadsAtStop;


+ (nonnull YMKMasstransitLineAtStop *)lineAtStopWithLine:(nonnull YMKMasstransitLine *)line
                                           threadsAtStop:(nonnull NSArray<YMKMasstransitThreadAtStop *> *)threadsAtStop;


@end
/// @endcond

