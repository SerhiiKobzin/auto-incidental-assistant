#import <YandexMapKit/YMKConstruction.h>

@interface YMKMasstransitRawConstruction : NSObject

@property (nonatomic, readonly) YMKPedestrianConstructionID construction_id;

@property (nonatomic, readonly) NSInteger count;


+ (nonnull YMKMasstransitRawConstruction *)rawConstructionWithConstruction_id:( YMKPedestrianConstructionID)construction_id
                                                                        count:( NSInteger)count;


@end

