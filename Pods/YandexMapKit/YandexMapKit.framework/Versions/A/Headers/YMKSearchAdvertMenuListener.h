#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * Listener to be notified when a new menu advertisement is received.
 */
@protocol YMKSearchAdvertMenuListener <NSObject>

/**
 * Gets called upon receiving new menu data.
 */
- (void)onMenuAdvertReceived;


@end
/// @endcond
