#import <Foundation/Foundation.h>

@interface YMKSearchReferenceType : NSObject

@property (nonatomic, readonly, nonnull) NSString *id;

@property (nonatomic, readonly, nonnull) NSString *scope;


+ (nonnull YMKSearchReferenceType *)referenceTypeWithId:(nonnull NSString *)id
                                                  scope:(nonnull NSString *)scope;


@end

