#import <YandexMapKit/YMKSearchExperimentalStorage.h>

/**
 * Snippet with experimental data.
 */
@interface YMKSearchExperimentalMetadata : NSObject

/**
 * Experimental data storage.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchExperimentalStorage *experimentalStorage;


+ (nonnull YMKSearchExperimentalMetadata *)experimentalMetadataWithExperimentalStorage:(nullable YMKSearchExperimentalStorage *)experimentalStorage;


@end

