#import <YandexMapKit/YMKPhotosFeed.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKPhotosSessionDataHandler)(
    YMKPhotosFeed *feed,
    NSError *error);

/**
 * Provides a photo feed of a particular business by page.
 */
@interface YMKPhotosSession : YRTPlatformBinding

/**
 * Checks if there is a following page. The first page is always
 * present. Returns true if there is a pending fetchNextPage operation.
 */
- (BOOL)hasNextPage;


/**
 * Requests the next feed page. If the previous hasNextPage() call
 * returned false, behavior is undefined. If there is a fetch operation
 * already pending, the call is ignored.
 */
- (void)fetchNextPageWithDataHandler:(nullable YMKPhotosSessionDataHandler)dataHandler;


/**
 * Cancels the pending operation, if there is one. If the fetch
 * operation is cancelled before any user notification is sent, the next
 * fetchNextPage() call will request the same page.
 */
- (void)cancel;


@end
/// @endcond

