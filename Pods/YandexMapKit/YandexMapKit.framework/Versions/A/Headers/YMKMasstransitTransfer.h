#import <YandexMapKit/YMKConstruction.h>

/**
 * Represents a transfer to another mass transit line. For example,
 * transfer from one underground line to another.
 */
@interface YMKMasstransitTransfer : NSObject

/**
 * List of pedestrian constuctions along the transfer path.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *constructions;


+ (nonnull YMKMasstransitTransfer *)transferWithConstructions:(nonnull NSArray<NSNumber *> *)constructions;


@end

