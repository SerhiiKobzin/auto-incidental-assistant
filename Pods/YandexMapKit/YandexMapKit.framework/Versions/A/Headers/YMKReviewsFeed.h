#import <YandexMapKit/YMKAtomFeed.h>
#import <YandexMapKit/YMKReviewsEntry.h>

/// @cond EXCLUDE
/**
 * Reviews feed page.
 */
@interface YMKReviewsFeed : NSObject

/**
 * Atom feed part.
 */
@property (nonatomic, readonly, nonnull) YMKAtomFeed *atomFeed;

/**
 * Reviews on this feed page.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKReviewsEntry *> *entries;


+ (nonnull YMKReviewsFeed *)feedWithAtomFeed:(nonnull YMKAtomFeed *)atomFeed
                                     entries:(nonnull NSArray<YMKReviewsEntry *> *)entries;


@end
/// @endcond

