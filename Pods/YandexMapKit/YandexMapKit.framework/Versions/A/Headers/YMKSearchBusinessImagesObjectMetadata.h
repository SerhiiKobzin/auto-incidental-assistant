#import <Foundation/Foundation.h>

@class YMKSearchBusinessImagesObjectMetadataLogo;

/// @cond EXCLUDE
/**
 * Snippet for images related to the given company.
 */
@interface YMKSearchBusinessImagesObjectMetadata : NSObject

/**
 * Company logo.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchBusinessImagesObjectMetadataLogo *logo;


+ (nonnull YMKSearchBusinessImagesObjectMetadata *)businessImagesObjectMetadataWithLogo:(nullable YMKSearchBusinessImagesObjectMetadataLogo *)logo;


@end
/// @endcond


/**
 * Logo for the company.
 */
@interface YMKSearchBusinessImagesObjectMetadataLogo : NSObject

/**
 * urlTemplate for the company logo. Available sizes are listed here:
 * http://api.yandex.ru/fotki/doc/format-ref/f-img.xml
 */
@property (nonatomic, readonly, nonnull) NSString *urlTemplate;


+ (nonnull YMKSearchBusinessImagesObjectMetadataLogo *)logoWithUrlTemplate:(nonnull NSString *)urlTemplate;


@end

