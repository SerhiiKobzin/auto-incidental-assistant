#import <Foundation/Foundation.h>

@protocol YMKResponseHandler <NSObject>

- (void)onSearchStart;


- (void)onSearchSuccess;


- (void)onSearchErrorWithError:(nullable NSError *)error;


@end
