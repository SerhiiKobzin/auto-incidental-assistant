#import <YandexMapKit/YMKLocalizedValue.h>
#import <YandexMapKit/YMKTaxiMoney.h>

/// @cond EXCLUDE
@interface YMKTaxiRideOption : NSObject

@property (nonatomic, readonly, nonnull) YMKLocalizedValue *waitingTime;

@property (nonatomic, readonly, nonnull) YMKTaxiMoney *cost;


+ (nonnull YMKTaxiRideOption *)rideOptionWithWaitingTime:(nonnull YMKLocalizedValue *)waitingTime
                                                    cost:(nonnull YMKTaxiMoney *)cost;


@end
/// @endcond

