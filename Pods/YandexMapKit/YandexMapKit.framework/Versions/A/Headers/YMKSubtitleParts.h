#import <Foundation/Foundation.h>

/**
 * Subtitles could be displayed on client to enrich the search results.
 * The list of subtitle parts could be obtained from SearchLayer Tags
 * are used to make the subtitle more specific
 *
 * SubtitlePartType enum denotes the possible types of subtitle parts
 */
typedef NS_ENUM(NSUInteger, YMKSubtitlePartType) {

    /**
     * Plain text
     */
    YMKSubtitlePartTypeText,

    /**
     * Features of the company
     */
    YMKSubtitlePartTypeFeature,

    /**
     * Rating of the organization
     */
    YMKSubtitlePartTypeRating,

    /**
     * Time to get to the place
     */
    YMKSubtitlePartTypeTravelTime,

    /**
     * Working hours of the organization
     */
    YMKSubtitlePartTypeWorkingHours
};


/**
 * Possible tags for the subtitle parts
 */
typedef NS_ENUM(NSUInteger, YMKSubtitleTag) {

    /**
     * Average bill
     */
    YMKSubtitleTagFeatureAverageBill2,

    /**
     * Price of the beer
     */
    YMKSubtitleTagFeatureBeerPrice,

    /**
     * Price of the car wash
     */
    YMKSubtitleTagFeatureCarBodyCleaningCost,

    /**
     * Price of the manicure
     */
    YMKSubtitleTagFeatureClassicFemaleManicureCoated,

    /**
     * Payment by credit card
     */
    YMKSubtitleTagFeaturePaymentByCreditCard,

    /**
     * Price for one item
     */
    YMKSubtitleTagFeaturePrice1,

    /**
     * Minimal price for one item
     */
    YMKSubtitleTagFeaturePrice1Min,

    /**
     * Price for the bed in the hotel
     */
    YMKSubtitleTagFeaturePriceBed,

    /**
     * Price for the cappucino cup
     */
    YMKSubtitleTagFeaturePriceCupCappuccino,

    /**
     * Price for the evening
     */
    YMKSubtitleTagFeaturePriceEvening,

    /**
     * Price for the hour
     */
    YMKSubtitleTagFeaturePriceHour,

    /**
     * Price for the hour in sauna
     */
    YMKSubtitleTagFeaturePricePerHourSauna2,

    /**
     * Price for the month
     */
    YMKSubtitleTagFeaturePricePerMonth2,

    /**
     * Price for the rolls
     */
    YMKSubtitleTagFeaturePriceRolls,

    /**
     * Price for the room
     */
    YMKSubtitleTagFeaturePriceRoom,

    /**
     * Rating (in stars)
     */
    YMKSubtitleTagFeatureStar,

    /**
     * Price for the ticket
     */
    YMKSubtitleTagFeatureTickets,

    /**
     * Price for the haircut
     */
    YMKSubtitleTagFeatureWomenHaircut,

    /**
     * Time to drive by car
     */
    YMKSubtitleTagTravelTimeCar,

    /**
     * Time to arrive on taxi
     */
    YMKSubtitleTagTravelTimeTaxi,

    /**
     * Time to walk
     */
    YMKSubtitleTagTravelTimePedestrian,

    /**
     * Time to go on public transport
     */
    YMKSubtitleTagTravelTimeMasstransit
};

