#import <YandexMapKit/YMKMasstransitOptions.h>
#import <YandexMapKit/YMKMasstransitSession.h>
#import <YandexMapKit/YMKMasstransitSummarySession.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKTimeOptions.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/**
 * Provides methods for submitting mass transit routing requests.
 */
@interface YMKMasstransitRouter : YRTPlatformBinding

/**
 * Submits a request to find mass transit routes between two points.
 *
 * @param source Origin point of the route to find.
 * @param destination Destination point of the route to find.
 * @param masstransitOptions Additional parameters for building routes.
 * @param routeListener Listener to retrieve a list of MasstransitRoute
 * objects.
 */
- (nullable YMKMasstransitSession *)requestRoutesWithSource:(nonnull YMKPoint *)source
                                                destination:(nonnull YMKPoint *)destination
                                         masstransitOptions:(nonnull YMKMasstransitOptions *)masstransitOptions
                                               routeHandler:(nullable YMKMasstransitSessionRouteHandler)routeHandler;


/**
 * Submits a request to fetch a brief summary of the mass transit routes
 * between two points.
 */
- (nullable YMKMasstransitSummarySession *)requestRoutesSummaryWithSource:(nonnull YMKPoint *)source
                                                              destination:(nonnull YMKPoint *)destination
                                                       masstransitOptions:(nonnull YMKMasstransitOptions *)masstransitOptions
                                                           summaryHandler:(nullable YMKMasstransitSummarySessionSummaryHandler)summaryHandler;


/**
 * Submits a request to retrieve detailed information on a mass transit
 * route by URI.
 *
 * @param uri The URI of the mass transit route. Starts with
 * "ymapsbm1://route/transit".
 * @param timeOptions Desired departure/arrival time settings. Empty
 * YMKTimeOptions for requests that are not time-dependent.
 * @param routeListener Listener to retrieve a list of MasstransitRoute
 * objects.
 */
- (nullable YMKMasstransitSession *)resolveUriWithUri:(nonnull NSString *)uri
                                          timeOptions:(nonnull YMKTimeOptions *)timeOptions
                                         routeHandler:(nullable YMKMasstransitSessionRouteHandler)routeHandler;


/**
 * Submits a request to retrieve alternatives for the mass transit route
 * with the specified URI.
 *
 * @param uri URI of the mass transit route.
 * @param routeListener Listener to retrieve a list of MasstransitRoute
 * objects.
 */
- (nullable YMKMasstransitSession *)alternativesWithUri:(nonnull NSString *)uri
                                           routeHandler:(nullable YMKMasstransitSessionRouteHandler)routeHandler;


@end

