#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * Rating "view". For example for the restaurant one can separately rate
 * food, restaurant interior and service quality. All of these are
 * available as facets.
 */
@interface YMKSearchFacet : NSObject

/**
 * Facet class.
 */
@property (nonatomic, readonly, nonnull) NSString *facetClass;

/**
 * Facet name.
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * Number of ratings for the facet.
 */
@property (nonatomic, readonly) NSUInteger ratings;

/**
 * Facet score.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *score;


+ (nonnull YMKSearchFacet *)facetWithFacetClass:(nonnull NSString *)facetClass
                                           name:(nonnull NSString *)name
                                        ratings:( NSUInteger)ratings
                                          score:(nullable NSNumber *)score;


@end
/// @endcond

