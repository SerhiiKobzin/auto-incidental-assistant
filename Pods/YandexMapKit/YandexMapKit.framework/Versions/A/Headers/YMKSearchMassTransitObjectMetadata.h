#import <YandexMapKit/YMKSearchStop.h>

/// @cond EXCLUDE
/**
 * Snippet data to get masstransit stop info.
 */
@interface YMKSearchMassTransitObjectMetadata : NSObject

/**
 * List of neareast masstransit stops.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchStop *> *stops;


+ (nonnull YMKSearchMassTransitObjectMetadata *)massTransitObjectMetadataWithStops:(nonnull NSArray<YMKSearchStop *> *)stops;


@end
/// @endcond

