#import <Foundation/Foundation.h>

@class YMKPanoramaPlayer;

@protocol YMKPanoramaDirectionChangeDelegate <NSObject>

/**
 * Called if the panorama direction was changed by the user or by the
 * setDirection() method.
 *
 * @param player Panorama player that sent the event.
 */
- (void)onPanoramaDirectionChangedWithPlayer:(nullable YMKPanoramaPlayer *)player;


@end
