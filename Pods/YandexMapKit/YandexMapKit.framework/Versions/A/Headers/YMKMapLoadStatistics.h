#import <Foundation/Foundation.h>

@interface YMKMapLoadStatistics : NSObject

@property (nonatomic, readonly) NSTimeInterval previewLoaded;

@property (nonatomic, readonly) NSTimeInterval geometryLoaded;

@property (nonatomic, readonly) NSTimeInterval delayedGeometryLoaded;

@property (nonatomic, readonly) NSTimeInterval labelsLoaded;

@property (nonatomic, readonly) NSTimeInterval placemarksLoaded;

@property (nonatomic, readonly) NSTimeInterval modelsLoaded;

@property (nonatomic, readonly) NSTimeInterval fullyLoaded;

@property (nonatomic, readonly) NSTimeInterval fullyAppeared;


+ (nonnull YMKMapLoadStatistics *)mapLoadStatisticsWithPreviewLoaded:( NSTimeInterval)previewLoaded
                                                      geometryLoaded:( NSTimeInterval)geometryLoaded
                                               delayedGeometryLoaded:( NSTimeInterval)delayedGeometryLoaded
                                                        labelsLoaded:( NSTimeInterval)labelsLoaded
                                                    placemarksLoaded:( NSTimeInterval)placemarksLoaded
                                                        modelsLoaded:( NSTimeInterval)modelsLoaded
                                                         fullyLoaded:( NSTimeInterval)fullyLoaded
                                                       fullyAppeared:( NSTimeInterval)fullyAppeared;


@end

