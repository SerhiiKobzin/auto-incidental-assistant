#import <YandexMapKit/YMKDrivingAnnotationLang.h>
#import <YandexMapKit/YMKDrivingRoute.h>
#import <YandexMapKit/YMKDrivingVehicleType.h>
#import <YandexMapKit/YMKGuidanceClassifiedLocation.h>
#import <YandexMapKit/YMKGuidanceDisplayedAnnotations.h>
#import <YandexMapKit/YMKGuidanceGuidanceHandler.h>
#import <YandexMapKit/YMKGuidanceLocalizedSpeaker.h>
#import <YandexMapKit/YMKGuidanceViewArea.h>
#import <YandexMapKit/YMKLocalizedValue.h>
#import <YandexMapKit/YMKLocationManager.h>
#import <YandexMapKit/YMKPolylinePosition.h>
#import <YandexMapKit/YMKRoadEventsRoadEvents.h>

#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKFasterAlternative;
@class YMKSpeedingPolicy;

/// @cond EXCLUDE
@interface YMKGuidanceGuide : YRTPlatformBinding

- (void)subscribeWithGuidanceListener:(nullable id<YMKGuidanceGuidanceHandler>)guidanceListener;


- (void)unsubscribeWithGuidanceListener:(nullable id<YMKGuidanceGuidanceHandler>)guidanceListener;


- (void)setLocationManagerWithLocationManager:(nullable YMKLocationManager *)locationManager;


- (void)resetSpeaker;


- (void)setLocalizedSpeakerWithSpeaker:(nullable id<YMKGuidanceLocalizedSpeaker>)speaker
                              language:(YMKDrivingAnnotationLanguage)language;


- (void)setSpeakerLanguageWithLanguage:(YMKDrivingAnnotationLanguage)language;


/**
 * Suspend running Guide. Android-specific.
 */
- (void)suspend;


/**
 * Resume running Guide after suspend(). Android-specific.
 */
- (void)resume;


- (void)mute;


- (void)unmute;


- (void)setRouteWithRoute:(nullable YMKDrivingRoute *)route;


- (void)resetRoute;


- (void)setReroutingEnabledWithOn:(BOOL)on;


- (void)setFasterAlternativeEnabledWithOn:(BOOL)on;


- (void)setParkingRoutesEnabledWithOn:(BOOL)on;


- (void)setAlternativesEnabledWithOn:(BOOL)on;


- (void)setRouteActionsAnnotatedWithOn:(BOOL)on;


- (void)setRoadEventsAnnotatedWithOn:(BOOL)on;


- (void)setRoadEventTypeAnnotatedWithType:(YMKRoadEventsEventType)type
                                       on:(BOOL)on;


- (void)setSpeedLimitExceededAnnotatedWithOn:(BOOL)on;


- (void)setParkingRoutesAnnotatedWithOn:(BOOL)on;


- (void)setStreetsAnnotatedWithOn:(BOOL)on;


/**
 * Enables annotations in free-driving mode (default: false).
 */
- (void)setFreeDrivingAnnotationsEnabledWithOn:(BOOL)on;


/**
 * Enables toll avoidance for route requests performed by the Guide
 * (e.g. faster alternatives and rerouting).
 *
 * The default value is 'false'.
 */
- (void)setTollAvoidanceEnabledWithOn:(BOOL)on;


/**
 * Set vehicle type for special routing (e.g. Taxi).
 */
- (void)setVehicleTypeWithVehicleType:(YMKDrivingVehicleType)vehicleType;


/**
 * Enables background guidance mode. If this method is called for the
 * iOS `location` flag in `UIBackgroundModes`, it is required in your
 * application.
 */
- (void)setBackgroundModeEnabledWithOn:(BOOL)on;


/**
 * Changes after rerouting (automatically) or by setRoute/resetRoute (by
 * user).
 */
@property (nonatomic, readonly, nullable) YMKDrivingRoute *route;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKPolylinePosition *routePosition;

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRoute *> *alternatives;

/**
 * Difference between ETA of current and alternative route (current
 * route time  -  alternative route time).
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKLocalizedValue *> *alternativesTimeDifference;

/**
 * Last known location adjusted to the road network.
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKGuidanceClassifiedLocation *location;

/**
 * The annotations to display at the moment.
 */
@property (nonatomic, readonly, nonnull) YMKGuidanceDisplayedAnnotations *displayedAnnotations;

/**
 * Caution: may be null if the region is not yet available.
 */
@property (nonatomic, readonly, nullable) YMKSpeedingPolicy *speedingPolicy;

@property (nonatomic) double speedingToleranceRatio;

/**
 * Available in guidance mode only
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKLocalizedValue *speedLimit;

/**
 * Available in guidance mode only true if - current speed > speedLimit
 * + allowed speed excess - speedLimit < current speed <= speedLimit +
 * allowed speed excess and no more than 3 seconds have passed since
 * last overspeeding, when current speed > speedLimit + allowed speed
 * excess.
 */
@property (nonatomic, readonly, getter=isSpeedLimitExceeded) BOOL speedLimitExceeded;

/**
 * Current zoom area. Updated on each event: onLocationUpdated
 */
@property (nonatomic, readonly, nonnull) YMKGuidanceViewArea *viewArea;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSString *roadName;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKFasterAlternative *fasterAlternative;

/**
 * Enables/disables snapping the location to the road network.
 */
@property (nonatomic, getter=isSnapToRoadsEnabled) BOOL snapToRoadsEnabled;

/**
 * Parking route
 */
@property (nonatomic, readonly, nullable) YMKDrivingRoute *parkingRoute;

/**
 * True if the GPS signal is currently being spoofed.
 */
@property (nonatomic, readonly, getter=isGpsSpoofed) BOOL gpsSpoofed;

/**
 * Last via point we passed.
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKPolylinePosition *lastViaPosition;

@end
/// @endcond

