#import <Foundation/Foundation.h>

@class YMKSearchPropertiesItem;

/**
 * Generic key-value property storage.
 */
@interface YMKSearchProperties : NSObject

/**
 * Property list.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSearchPropertiesItem *> *items;


+ (nonnull YMKSearchProperties *)propertiesWithItems:(nonnull NSArray<YMKSearchPropertiesItem *> *)items;


@end


/**
 * Single property item.
 */
@interface YMKSearchPropertiesItem : NSObject

/**
 * Item key.
 */
@property (nonatomic, readonly, nonnull) NSString *key;

/**
 * Item value.
 */
@property (nonatomic, readonly, nonnull) NSString *value;


+ (nonnull YMKSearchPropertiesItem *)itemWithKey:(nonnull NSString *)key
                                           value:(nonnull NSString *)value;


@end

