#import <YandexMapKit/YMKTaxiMoney.h>

/**
 * Fuel name and price.
 */
@interface YMKSearchFuelType : NSObject

/**
 * Fuel name.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *name;

/**
 * Fuel price.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKTaxiMoney *price;


+ (nonnull YMKSearchFuelType *)fuelTypeWithName:(nullable NSString *)name
                                          price:(nullable YMKTaxiMoney *)price;


@end

