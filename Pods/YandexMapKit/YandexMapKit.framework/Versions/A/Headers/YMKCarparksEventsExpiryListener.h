#import <Foundation/Foundation.h>

/// @cond EXCLUDE
@protocol YMKCarparksEventsExpiryListener <NSObject>

/**
 * Called when the layer data becomes obsolete. For instance, the
 * subscriber might decide to clear the layer on an event.
 */
- (void)onCarparksEventsExpired;


@end
/// @endcond
