#import <UIKit/UIKit.h>

@interface YMKManeuverStyle : NSObject

/**
 * Fill color of the arrow.
 */
@property (nonatomic, readonly, nonnull) UIColor *fillColor;

/**
 * Color of the arrow's outline.
 */
@property (nonatomic, readonly, nonnull) UIColor *outlineColor;

/**
 * Width of the arrow's outline in units.
 */
@property (nonatomic, readonly) float outlineWidth;

/**
 * Overall length of the arrow (including the tip) in units.
 */
@property (nonatomic, readonly) float length;

/**
 * Height of the arrow tip in units.
 */
@property (nonatomic, readonly) float triangleHeight;

/**
 * Enables/disables maneuvers.
 */
@property (nonatomic, readonly) BOOL enabled;


+ (nonnull YMKManeuverStyle *)maneuverStyleWithFillColor:(nonnull UIColor *)fillColor
                                            outlineColor:(nonnull UIColor *)outlineColor
                                            outlineWidth:( float)outlineWidth
                                                  length:( float)length
                                          triangleHeight:( float)triangleHeight
                                                 enabled:( BOOL)enabled;


@end

