#import <YandexMapKit/YMKDrivingRoute.h>
#import <YandexMapKit/YMKLocation.h>
#import <YandexMapKit/YMKLocationManager.h>
#import <YandexMapKit/YMKRecordedSimulatorListener.h>

/// @cond EXCLUDE
@interface YMKRecordedSimulator : YMKLocationManager

@property (nonatomic, nonnull) NSDate *timestamp;

@property (nonatomic) NSInteger clockRate;

/**
 * true if simulator is not suspended
 */
@property (nonatomic, readonly, getter=isActive) BOOL active;

/**
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) YMKLocation *location;

@property (nonatomic, readonly, nullable) YMKDrivingRoute *route;

- (void)subscribeForSimulatorEventsWithSimulatorListener:(nullable id<YMKRecordedSimulatorListener>)simulatorListener;


- (void)unsubscribeFromSimulatorEventsWithSimulatorListener:(nullable id<YMKRecordedSimulatorListener>)simulatorListener;


@end
/// @endcond

