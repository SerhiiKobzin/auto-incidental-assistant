#import <YandexMapKit/YMKPoint.h>

@class YMKDrivingRoute;

/**
 * The waypoint and a point the path must go through.
 */
typedef NS_ENUM(NSUInteger, YMKDrivingRequestPointType) {

    YMKDrivingRequestPointTypeWaypoint,

    YMKDrivingRequestPointTypeViapoint
};


/**
 * There are two types of request points. A waypoint is a destination.
 * Use it when you plan to stop there. Via points correct the route to
 * make it pass through all viapoints. Waypoints are guaranteed to be
 * between sections in the resulting YMKDrivingRoute. Via points are
 * embedded into sections.
 *
 * For each request point, you can provide a list of arrival points (for
 * example, if the request point is a building, you can provide a list
 * of entrances to that building). If there is at least one arrival
 * point, then the router will choose one of them and use it for
 * routing. The point member will be used only as a suggestion.
 * Otherwise, the point itself will be used for routing.
 */
@interface YMKDrivingRequestPoint : NSObject

@property (nonatomic, readonly, nonnull) YMKPoint *point;

@property (nonatomic, readonly, nonnull) NSArray<YMKPoint *> *arrivalPoints;

@property (nonatomic, readonly) YMKDrivingRequestPointType type;


+ (nonnull YMKDrivingRequestPoint *)requestPointWithPoint:(nonnull YMKPoint *)point
                                            arrivalPoints:(nonnull NSArray<YMKPoint *> *)arrivalPoints
                                                     type:( YMKDrivingRequestPointType)type;


@end

