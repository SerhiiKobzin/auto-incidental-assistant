#import <YandexMapKit/YMKDrivingJamSegment.h>

@interface YMKDrivingRawJams : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingJamSegment *> *segments;


+ (nonnull YMKDrivingRawJams *)rawJamsWithSegments:(nonnull NSArray<YMKDrivingJamSegment *> *)segments;


@end

