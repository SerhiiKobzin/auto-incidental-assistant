#import <Foundation/Foundation.h>

/// @cond EXCLUDE
/**
 * Represents an advertised menu item. A tap on the item leads to a
 * corresponding search action.
 */
@interface YMKSearchAdvertMenuItem : NSObject

/**
 * Advertised menu item name.
 */
@property (nonatomic, readonly, nonnull) NSString *title;

/**
 * Search query to execute when user clicks on the menu item.
 */
@property (nonatomic, readonly, nonnull) NSString *searchQuery;

/**
 * Style that is used to create an identifier for an icon (see
 * BitmapDownloader.requestBitmap).
 */
@property (nonatomic, readonly, nonnull) NSString *style;

/**
 * Desired position in search menu.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *position;

/**
 * Identifier for logging.
 */
@property (nonatomic, readonly, nonnull) NSString *logId;


+ (nonnull YMKSearchAdvertMenuItem *)advertMenuItemWithTitle:(nonnull NSString *)title
                                                 searchQuery:(nonnull NSString *)searchQuery
                                                       style:(nonnull NSString *)style
                                                    position:(nullable NSNumber *)position
                                                       logId:(nonnull NSString *)logId;


@end
/// @endcond

