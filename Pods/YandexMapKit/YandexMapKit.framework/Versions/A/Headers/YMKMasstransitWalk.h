#import <YandexMapKit/YMKConstruction.h>
#import <YandexMapKit/YMKSpot.h>

/**
 * Represents a pedestrian section of a route.
 */
@interface YMKMasstransitWalk : NSObject

/**
 * List of pedestrian constructions along the pedestrian path.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *constructions;

/**
 * @brief List of pedestrian spots with their coordinates along the
 * pedestrian path.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKSpot *> *spots;


+ (nonnull YMKMasstransitWalk *)walkWithConstructions:(nonnull NSArray<NSNumber *> *)constructions
                                                spots:(nonnull NSArray<YMKSpot *> *)spots;


@end

