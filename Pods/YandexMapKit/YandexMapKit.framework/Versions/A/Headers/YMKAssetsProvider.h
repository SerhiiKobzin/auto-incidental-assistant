#import <YandexMapKit/YMKIconStyle.h>
#import <YandexMapKit/YMKSize.h>

#import <UIKit/UIKit.h>

@class YMKSearchResultItem;

/**
 * Possible placemark icon types
 */
typedef NS_ENUM(NSUInteger, YMKPlacemarkIconType) {

    /**
     * No icon
     */
    YMKPlacemarkIconTypeNone,

    /**
     * Dust
     */
    YMKPlacemarkIconTypeDust,

    /**
     * Dust, search result is already visited
     */
    YMKPlacemarkIconTypeDustVisited,

    /**
     * Dust, organization is closed
     */
    YMKPlacemarkIconTypeDustClosed,

    /**
     * Icon
     */
    YMKPlacemarkIconTypeIcon,

    /**
     * Icon, search result is already visited
     */
    YMKPlacemarkIconTypeIconVisited,

    /**
     * Icon, organization is closed
     */
    YMKPlacemarkIconTypeIconClosed,

    /**
     * One-line label to the left of the icon
     */
    YMKPlacemarkIconTypeLabelShortLeft,

    /**
     * One-line label to the right of the icon
     */
    YMKPlacemarkIconTypeLabelShortRight,

    /**
     * Detailed label to the left of the icon
     */
    YMKPlacemarkIconTypeLabelDetailedLeft,

    /**
     * Detailed label to the right of the icon
     */
    YMKPlacemarkIconTypeLabelDetailedRight,

    /**
     * Search result is selected
     */
    YMKPlacemarkIconTypeSelected
};


/**
 * Interface for providing images, image sizes and icon styles to the
 * search layer
 */
@protocol YMKAssetsProvider <NSObject>

/**
 * Returns image for certain placemark type with given search result
 */
- (nullable UIImage *)imageWithSearchResult:(nullable YMKSearchResultItem *)searchResult
                                       type:(YMKPlacemarkIconType)type;


/**
 * Returns the size of the icon of certain placemark type with given
 * search result
 */
- (nonnull YMKSize *)sizeWithSearchResult:(nullable YMKSearchResultItem *)searchResult
                                     type:(YMKPlacemarkIconType)type;


/**
 * Returns the icon style for certain placemark type with given search
 * result
 */
- (nonnull YMKIconStyle *)iconStyleWithSearchResult:(nullable YMKSearchResultItem *)searchResult
                                               type:(YMKPlacemarkIconType)type;


@end
