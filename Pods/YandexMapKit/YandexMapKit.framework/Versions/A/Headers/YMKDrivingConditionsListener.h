#import <Foundation/Foundation.h>

@protocol YMKDrivingConditionsListener <NSObject>

- (void)onConditionsUpdated;


- (void)onConditionsOutdated;


@end
