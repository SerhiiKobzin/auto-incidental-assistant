#import <YandexMapKit/YMKTrafficLevel.h>

@protocol YMKTrafficDelegate <NSObject>

- (void)onTrafficChangedWithTrafficLevel:(nullable YMKTrafficLevel *)trafficLevel;


- (void)onTrafficLoading;


- (void)onTrafficExpired;


@end
