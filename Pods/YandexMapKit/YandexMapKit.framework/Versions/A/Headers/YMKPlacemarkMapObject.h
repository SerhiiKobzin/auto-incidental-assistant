#import <YandexMapKit/YMKCallback.h>
#import <YandexMapKit/YMKIconStyle.h>
#import <YandexMapKit/YMKMapObject.h>
#import <YandexMapKit/YMKModelStyle.h>
#import <YandexMapKit/YMKPoint.h>

#import <UIKit/UIKit.h>

@class YMKAnimatedIcon;
@class YMKCompositeIcon;

/**
 * Represents a geo-positioned object on the map.
 */
@interface YMKPlacemarkMapObject : YMKMapObject

/**
 * Position of the object.
 */
@property (nonatomic, nonnull) YMKPoint *geometry;

/**
 * Angle between the direction of an object and the direction to north.
 * Measured in degrees. Default: 0.f.
 */
@property (nonatomic) float direction;

/**
 * Opacity multiplicator for the placemark content. Values below 0 will
 * be set to 0. Default: 1.
 */
@property (nonatomic) float opacity;

/**
 * Sets an icon with the default style for the placemark. Resets the
 * animated icon, the composite icon and the model.
 */
- (void)setIconWithImage:(nullable UIImage *)image;


/**
 * Sets an icon with the given style for the placemark. Resets the
 * animated icon, the composite icon and the model.
 */
- (void)setIconWithImage:(nullable UIImage *)image
                   style:(nonnull YMKIconStyle *)style;


/**
 * Sets an icon with the default style for the placemark. Resets the
 * animated icon, the composite icon and the model. The callback is
 * called immediately after the image finished loading. This means you
 * can, for example, change the placemark visibility with a new icon.
 *
 * @param onFinished Called when the icon is loaded.
 */
- (void)setIconWithImage:(nullable UIImage *)image
                callback:(nullable YMKCallback)callback;


/**
 * Sets an icon with the given style for the placemark. Resets the
 * animated icon, the composite icon and the model. The callback is
 * called immediately after the image finished loading. This means you
 * can, for example, change the placemark visibility with a new icon.
 *
 * @param onFinished Called when the icon is loaded.
 */
- (void)setIconWithImage:(nullable UIImage *)image
                   style:(nonnull YMKIconStyle *)style
                callback:(nullable YMKCallback)callback;


/**
 * Changes the icon style. Valid only for the single icon and the
 * animated icon.
 */
- (void)setIconStyleWithStyle:(nonnull YMKIconStyle *)style;


/**
 * Sets and returns the composite icon. Resets the single icon, the
 * animated icon and the model.
 */
- (nullable YMKCompositeIcon *)useCompositeIcon;


/**
 * Sets and returns the animated icon. Resets thesingle icon, the
 * composite icon and the model.
 */
- (nullable YMKAnimatedIcon *)useAnimatedIcon;


/**
 * Changes the model style.
 */
- (void)setModelStyleWithModelStyle:(nonnull YMKModelStyle *)modelStyle;


/**
 * Sets the model. Resets icons.
 */
- (void)setModelWithObj:(nonnull NSString *)obj
        textureProvider:(nullable UIImage *)textureProvider
                  style:(nonnull YMKModelStyle *)style;


/**
 * Sets the model. Resets icons. The callback will be called immediately
 * after model loading finishes.
 */
- (void)setModelWithObj:(nonnull NSString *)obj
        textureProvider:(nullable UIImage *)textureProvider
                  style:(nonnull YMKModelStyle *)style
               callback:(nullable YMKCallback)callback;


@end

