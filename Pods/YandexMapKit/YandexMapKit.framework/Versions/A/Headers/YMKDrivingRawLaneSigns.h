#import <YandexMapKit/YMKDrivingRawLaneSign.h>

@interface YMKDrivingRawLaneSigns : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRawLaneSign *> *laneSigns;


+ (nonnull YMKDrivingRawLaneSigns *)rawLaneSignsWithLaneSigns:(nonnull NSArray<YMKDrivingRawLaneSign *> *)laneSigns;


@end

