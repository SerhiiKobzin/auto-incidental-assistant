#import <YandexMapKit/YMKBoundingBox.h>
#import <YandexMapKit/YMKDirectBanner.h>
#import <YandexMapKit/YMKGeoObject.h>
#import <YandexMapKit/YMKSearchBusinessResultMetadata.h>
#import <YandexMapKit/YMKSearchDisplayType.h>
#import <YandexMapKit/YMKSearchSort.h>
#import <YandexMapKit/YMKSearchToponymResultMetadata.h>

/**
 * Additional info for search response;
 */
@interface YMKSearchMetadata : NSObject

/**
 * Approximate number of found objects.
 */
@property (nonatomic, readonly) NSInteger found;

/**
 * Display type.
 */
@property (nonatomic, readonly) YMKSearchDisplayType displayType;

/**
 * Bounding box of the response as a whole.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKBoundingBox *boundingBox;

/**
 * Server-chosen sorting.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchSort *sort;

/**
 * Geocoder response to the toponym part of the query.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKGeoObject *toponym;

/**
 * Additional info for response from toponym search.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchToponymResultMetadata *toponymResultMetadata;

/**
 * Additional info for response from organization search.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YMKSearchBusinessResultMetadata *businessResultMetadata;

/**
 * Server-generated request id.
 */
@property (nonatomic, readonly, nonnull) NSString *reqid;

/**
 * Yandex.Direct banners.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKDirectBanner *> *banners;

/**
 * Server-generated request context.
 */
@property (nonatomic, readonly, nonnull) NSString *context;

/**
 * Initial request text.
 */
@property (nonatomic, readonly, nonnull) NSString *requestText;


+ (nonnull YMKSearchMetadata *)searchMetadataWithFound:( NSInteger)found
                                           displayType:( YMKSearchDisplayType)displayType
                                           boundingBox:(nullable YMKBoundingBox *)boundingBox
                                                  sort:(nullable YMKSearchSort *)sort
                                               toponym:(nullable YMKGeoObject *)toponym
                                 toponymResultMetadata:(nullable YMKSearchToponymResultMetadata *)toponymResultMetadata
                                businessResultMetadata:(nullable YMKSearchBusinessResultMetadata *)businessResultMetadata
                                                 reqid:(nonnull NSString *)reqid
                                               banners:(nonnull NSArray<YMKDirectBanner *> *)banners
                                               context:(nonnull NSString *)context
                                           requestText:(nonnull NSString *)requestText;


@end

