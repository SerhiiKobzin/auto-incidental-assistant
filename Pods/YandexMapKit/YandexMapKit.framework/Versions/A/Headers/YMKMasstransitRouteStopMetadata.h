#import <YandexMapKit/YMKMasstransitStop.h>

@interface YMKMasstransitRouteStopMetadata : NSObject

@property (nonatomic, readonly, nonnull) YMKMasstransitStop *stop;


+ (nonnull YMKMasstransitRouteStopMetadata *)routeStopMetadataWithStop:(nonnull YMKMasstransitStop *)stop;


@end

