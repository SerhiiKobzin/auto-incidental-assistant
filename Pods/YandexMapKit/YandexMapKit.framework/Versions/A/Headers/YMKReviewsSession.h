#import <YandexMapKit/YMKReviewsFeed.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKReviewsSessionDataHandler)(
    YMKReviewsFeed *feed,
    NSError *error);

/**
 * Provides a feed of reviews of the particular business by pages.
 */
@interface YMKReviewsSession : YRTPlatformBinding

/**
 * Checks whether there is a following page. The first page is always
 * present. Returns true if there is a pending fetchNextPage operation.
 */
- (BOOL)hasNextPage;


/**
 * Requests the next feed page. If prior hasNextPage() call returned
 * false, behavior is undefined. If there is a fetch operation already
 * pending, the call is ignored.
 */
- (void)fetchNextPageWithDataHandler:(nullable YMKReviewsSessionDataHandler)dataHandler;


/**
 * Cancels the pending operation, if there is one. If the fetch
 * operation has been cancelled before any user notification was sent,
 * the next fetchNextPage() call will request the same page.
 */
- (void)cancel;


@end
/// @endcond

