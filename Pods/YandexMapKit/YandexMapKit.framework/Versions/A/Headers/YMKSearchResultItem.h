#import <YandexMapKit/YMKGeoObject.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKSubtitlePart.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/**
 * Wrapper of the GeoObject in response, which makes it easy to get the
 * certain fields from GeoObject
 */
@interface YMKSearchResultItem : YRTPlatformBinding

/**
 * Id (constructed inside)
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Name (from GeoObject)
 */
@property (nonatomic, readonly, nonnull) NSString *name;

/**
 * CategoryClass (optional, first if there are several)
 *
 * Optional property, can be nil.
 */
@property (nonatomic, readonly, nullable) NSString *categoryClass;

/**
 * Wrapped GeoObject itself
 */
@property (nonatomic, readonly, nonnull) YMKGeoObject *geoObject;

/**
 * Point from the GeoObject
 */
@property (nonatomic, readonly, nonnull) YMKPoint *point;

/**
 * Does the GeoObject contain details for subtitle
 */
- (BOOL)hasDetails;


/**
 * Details for the subtitle
 */
- (nonnull NSArray<YMKSubtitlePart *> *)details;


/**
 * Is it an advertisement GeoObject
 */
- (BOOL)isAdvertisement;


/**
 * Is the organization closed now
 */
- (BOOL)isClosed;


/**
 * Is the GeoObject found in offline search
 */
- (BOOL)isOffline;


/**
 * Tells if this object is valid or no. Any method called on an invalid
 * object will throw an exception. The object becomes invalid only on UI
 * thread, and only when its implementation depends on objects already
 * destroyed by now. Please refer to general docs about the interface for
 * details on its invalidation.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

@end

