#import <YandexMapKit/YMKDrivingRawSpeedLimit.h>

@interface YMKDrivingRawSpeedLimits : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingRawSpeedLimit *> *speedLimits;


+ (nonnull YMKDrivingRawSpeedLimits *)rawSpeedLimitsWithSpeedLimits:(nonnull NSArray<YMKDrivingRawSpeedLimit *> *)speedLimits;


@end

