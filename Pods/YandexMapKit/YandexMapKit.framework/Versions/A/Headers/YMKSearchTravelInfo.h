#import <YandexMapKit/YMKLocalizedValue.h>

/**
 * Describes information about route via driving or walking.
 */
@interface YMKSearchTravelInfo : NSObject

/**
 * Route duration.
 */
@property (nonatomic, readonly, nonnull) YMKLocalizedValue *duration;

/**
 * Route distance.
 */
@property (nonatomic, readonly, nonnull) YMKLocalizedValue *distance;


+ (nonnull YMKSearchTravelInfo *)travelInfoWithDuration:(nonnull YMKLocalizedValue *)duration
                                               distance:(nonnull YMKLocalizedValue *)distance;


@end

