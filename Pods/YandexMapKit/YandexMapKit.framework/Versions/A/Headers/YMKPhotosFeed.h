#import <YandexMapKit/YMKAtomFeed.h>
#import <YandexMapKit/YMKPhotosEntry.h>

/// @cond EXCLUDE
/**
 * Photo feed page.
 */
@interface YMKPhotosFeed : NSObject

/**
 * Atom part of the feed.
 */
@property (nonatomic, readonly, nonnull) YMKAtomFeed *atomFeed;

/**
 * Photos in this part of the feed.
 */
@property (nonatomic, readonly, nonnull) NSArray<YMKPhotosEntry *> *entries;


+ (nonnull YMKPhotosFeed *)feedWithAtomFeed:(nonnull YMKAtomFeed *)atomFeed
                                    entries:(nonnull NSArray<YMKPhotosEntry *> *)entries;


@end
/// @endcond

