#import <YandexRuntime/YRTPlatformBinding.h>

@class YMKDrivingRoute;

typedef void(^YMKDrivingSessionRouteHandler)(
    NSArray<YMKDrivingRoute *> *routes,
    NSError *error);

@interface YMKDrivingSession : YRTPlatformBinding

- (void)cancel;


- (void)retryWithRouteHandler:(nullable YMKDrivingSessionRouteHandler)routeHandler;


@end

