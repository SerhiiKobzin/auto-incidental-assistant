#import <YandexMapKit/YMKGeoObjectSession.h>
#import <YandexMapKit/YMKMasstransitLineSession.h>
#import <YandexMapKit/YMKMasstransitThreadSession.h>
#import <YandexMapKit/YMKMasstransitVehicleSession.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
/**
 * Provides methods to retrieve mass transit data from the server.
 */
@interface YMKMasstransitInfoService : YRTPlatformBinding

/**
 * Submits a request to retrieve detailed information about a mass
 * transit stop object by its ID.
 *
 * @param id Mass transit stop identifier.
 * @param objListener A listener that is used to retrieve a mass transit
 * stop geo object.
 */
- (nullable YMKGeoObjectSession *)stopWithId:(nonnull NSString *)id
                            geoObjectHandler:(nullable YMKGeoObjectSessionGeoObjectHandler)geoObjectHandler;


/**
 * Submits a request to retrieve detailed information about a mass
 * transit stop object by its URI.
 *
 * @param uri URI of the mass transit stop.
 * @param objListener A listener that is used to retrieve a mass transit
 * stop geo object.
 */
- (nullable YMKGeoObjectSession *)resolveUriWithUri:(nonnull NSString *)uri
                                   geoObjectHandler:(nullable YMKGeoObjectSessionGeoObjectHandler)geoObjectHandler;


/**
 * Submits a request to retrieve detailed information about a mass
 * transit vehicle.
 *
 * @param id Mass transit vehicle identifier.
 * @param vehicleListener A listener that is used to retrieve a mass
 * transit vehicle object.
 */
- (nullable YMKMasstransitVehicleSession *)vehicleWithId:(nonnull NSString *)id
                                          vehicleHandler:(nullable YMKMasstransitVehicleSessionVehicleHandler)vehicleHandler;


/**
 * Submits a request to retrieve detailed information about a mass
 * transit thread.
 *
 * @param id Mass transit thread identifier.
 * @param threadListener A listener that is used to retrieve a mass
 * transit thread object.
 */
- (nullable YMKMasstransitThreadSession *)threadWithId:(nonnull NSString *)id
                                         threadHandler:(nullable YMKMasstransitThreadSessionThreadHandler)threadHandler;


/**
 * Submits a request to retrieve detailed information about the mass
 * transit line.
 *
 * @param id Mass transit line identifier.
 * @param lineListener A listener that is used to retrieve a mass
 * transit line object.
 */
- (nullable YMKMasstransitLineSession *)lineWithId:(nonnull NSString *)id
                                       lineHandler:(nullable YMKMasstransitLineSessionLineHandler)lineHandler;


@end
/// @endcond

