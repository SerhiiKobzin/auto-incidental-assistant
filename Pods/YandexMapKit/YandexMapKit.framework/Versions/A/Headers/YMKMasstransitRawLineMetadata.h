#import <YandexMapKit/YMKMasstransitLine.h>

@interface YMKMasstransitRawLineMetadata : NSObject

@property (nonatomic, readonly, nonnull) YMKMasstransitLine *line;


+ (nonnull YMKMasstransitRawLineMetadata *)rawLineMetadataWithLine:(nonnull YMKMasstransitLine *)line;


@end

