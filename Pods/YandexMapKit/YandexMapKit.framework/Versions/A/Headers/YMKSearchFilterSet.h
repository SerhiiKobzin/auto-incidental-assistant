#import <Foundation/Foundation.h>

/**
 * Collection of filters.
 */
@interface YMKSearchFilterSet : NSObject

/**
 * Ids for filters in the collection.
 */
@property (nonatomic, readonly, nonnull) NSArray<NSString *> *ids;


+ (nonnull YMKSearchFilterSet *)filterSetWithIds:(nonnull NSArray<NSString *> *)ids;


@end

