#import <YandexMapKit/YMKVehicle.h>

#import <YandexRuntime/YRTPlatformBinding.h>

/// @cond EXCLUDE
typedef void(^YMKMasstransitVehicleSessionVehicleHandler)(
    YMKVehicle *vehicle,
    NSError *error);

/**
 * Handler for a mass transit YMKVehicle async request.
 */
@interface YMKMasstransitVehicleSession : YRTPlatformBinding

/**
 * Tries to cancel the current mass transit vehicle request.
 */
- (void)cancel;


/**
 * Retries the mass transit vehicle request using the specified
 * callback.
 */
- (void)retryWithVehicleHandler:(nullable YMKMasstransitVehicleSessionVehicleHandler)vehicleHandler;


@end
/// @endcond

