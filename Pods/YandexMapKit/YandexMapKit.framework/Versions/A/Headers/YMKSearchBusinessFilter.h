#import <YandexMapKit/YMKSearchFeature.h>

@class YMKSearchBusinessFilterBooleanValue;
@class YMKSearchBusinessFilterEnumValue;
@class YMKSearchBusinessFilterValues;

/**
 * A filter that could be applied to search results.
 *
 * Filters can be either boolean (i.e. Wi-Fi availability in a cafe) or
 * enumerated (i.e. cuisine type in a restaurant). Enumerated filters
 * support multiple selected values (OR-combined), to search, for
 * example, for restaurants with Armenian or Georgian cuisine at once.
 *
 * This class is used in two separate ways: a) server response contains
 * all filters applicable to current search request and b) client can
 * use some of these filters to get more specific results in the
 * following search requests
 */
@interface YMKSearchBusinessFilter : NSObject

/**
 * Filter id.
 */
@property (nonatomic, readonly, nonnull) NSString *id;

/**
 * Human-readable filter name.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *name;

/**
 * The filter should not be used by the client, because filter is either
 * used already (selected:true, disabled:true) or nothing would be found
              * (selected:false, disabled:true).
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *disabled;

/**
 * Filter values.
 */
@property (nonatomic, readonly, nonnull) YMKSearchBusinessFilterValues *values;


+ (nonnull YMKSearchBusinessFilter *)businessFilterWithId:(nonnull NSString *)id
                                                     name:(nullable NSString *)name
                                                 disabled:(nullable NSNumber *)disabled
                                                   values:(nonnull YMKSearchBusinessFilterValues *)values;


@end


/**
 * Value for boolean filters.
 */
@interface YMKSearchBusinessFilterBooleanValue : NSObject

/**
 * Filter value. Set in server reponse for selected filters.
 */
@property (nonatomic, readonly) BOOL value;

/**
 * Selected marker. Set in server response for selected filters.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *selected;


+ (nonnull YMKSearchBusinessFilterBooleanValue *)booleanValueWithValue:( BOOL)value
                                                              selected:(nullable NSNumber *)selected;


@end


/**
 * Value for enum filters.
 */
@interface YMKSearchBusinessFilterEnumValue : NSObject

/**
 * Filter value. Set in server response for selected filters.
 */
@property (nonatomic, readonly, nonnull) YMKSearchFeatureEnumValue *value;

/**
 * Selected marker. Set in server response for selected filters.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *selected;

/**
 * Same as YMKSearchBusinessFilter::disabled, but for this specific enum
 * value.
 *
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *disabled;


+ (nonnull YMKSearchBusinessFilterEnumValue *)enumValueWithValue:(nonnull YMKSearchFeatureEnumValue *)value
                                                        selected:(nullable NSNumber *)selected
                                                        disabled:(nullable NSNumber *)disabled;


@end


@interface YMKSearchBusinessFilterValues : NSObject

@property (nonatomic, readonly) NSArray<YMKSearchBusinessFilterBooleanValue *> *booleans;

@property (nonatomic, readonly) NSArray<YMKSearchBusinessFilterEnumValue *> *enums;

+ (YMKSearchBusinessFilterValues *)valuesWithBooleans:(NSArray<YMKSearchBusinessFilterBooleanValue *> *)booleans;

+ (YMKSearchBusinessFilterValues *)valuesWithEnums:(NSArray<YMKSearchBusinessFilterEnumValue *> *)enums;

@end

