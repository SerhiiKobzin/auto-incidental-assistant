#import <YandexMapKit/YMKSearchReferenceType.h>

@interface YMKSearchReferencesObjectMetadata : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKSearchReferenceType *> *references;


+ (nonnull YMKSearchReferencesObjectMetadata *)referencesObjectMetadataWithReferences:(nonnull NSArray<YMKSearchReferenceType *> *)references;


@end

