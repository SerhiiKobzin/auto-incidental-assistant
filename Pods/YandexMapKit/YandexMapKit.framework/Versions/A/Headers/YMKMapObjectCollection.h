#import <YandexMapKit/YMKCircle.h>
#import <YandexMapKit/YMKIconStyle.h>
#import <YandexMapKit/YMKMapObject.h>
#import <YandexMapKit/YMKMapObjectCollectionListener.h>
#import <YandexMapKit/YMKMapObjectVisitor.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolygon.h>
#import <YandexMapKit/YMKPolyline.h>

#import <YandexRuntime/YRTAnimatedImageProvider.h>

#import <UIKit/UIKit.h>

@class YMKCircleMapObject;
@class YMKColoredPolylineMapObject;
@class YMKPlacemarkMapObject;
@class YMKPolygonMapObject;
@class YMKPolylineMapObject;

typedef NS_ENUM(NSUInteger, YMKConflictResolvingMode) {

    /**
     * Placemarks in this collection displace intersected placemarks in map
     * layers. Placemark map objects do not displace each other.
     */
    YMKConflictResolvingModeParticipate,

    /**
     * Placemarks in this collection do not participate in conflict
     * resolving.
     */
    YMKConflictResolvingModeIgnore
};


/**
 * A collection of map objects that can hold any set of MapObject items,
 * including nested collections.
 */
@interface YMKMapObjectCollection : YMKMapObject

/**
 * Creates a new empty placemark and adds it to the current collection.
 */
- (nullable YMKPlacemarkMapObject *)addEmptyPlacemarkWithPoint:(nonnull YMKPoint *)point;


/**
 * Creates a new placemark with the default icon and style, and adds it
 * to the current collection.
 */
- (nullable YMKPlacemarkMapObject *)addPlacemarkWithPoint:(nonnull YMKPoint *)point;


/**
 * Creates a new placemark with the default style and adds it to the
 * current collection.
 */
- (nullable YMKPlacemarkMapObject *)addPlacemarkWithPoint:(nonnull YMKPoint *)point
                                                    image:(nullable UIImage *)image;


/**
 * Creates a new placemark and adds it to the current collection.
 */
- (nullable YMKPlacemarkMapObject *)addPlacemarkWithPoint:(nonnull YMKPoint *)point
                                                    image:(nullable UIImage *)image
                                                    style:(nonnull YMKIconStyle *)style;


/**
 * Creates a new animated placemark and adds it to the current
 * collection.
 */
- (nullable YMKPlacemarkMapObject *)addPlacemarkWithPoint:(nonnull YMKPoint *)point
                                            animatedImage:(nullable id<YRTAnimatedImageProvider>)animatedImage
                                                    style:(nonnull YMKIconStyle *)style;


/**
 * Creates a new polyline and adds it to the current collection.
 */
- (nullable YMKPolylineMapObject *)addPolylineWithPolyline:(nonnull YMKPolyline *)polyline;


/**
 * Creates a new colored polyline and adds it to the current collection.
 */
- (nullable YMKColoredPolylineMapObject *)addColoredPolylineWithPolyline:(nonnull YMKPolyline *)polyline;


/**
 * Creates a new colored polyline with an empty geometry and adds it to
 * the current collection.
 */
- (nullable YMKColoredPolylineMapObject *)addColoredPolyline;


/**
 * Creates a new polygon and adds it to the current collection.
 */
- (nullable YMKPolygonMapObject *)addPolygonWithPolygon:(nonnull YMKPolygon *)polygon;


/**
 * Creates a new circle with the specified style and adds it to the
 * current collection.
 */
- (nullable YMKCircleMapObject *)addCircleWithCircle:(nonnull YMKCircle *)circle
                                         strokeColor:(nonnull UIColor *)strokeColor
                                         strokeWidth:(float)strokeWidth
                                           fillColor:(nonnull UIColor *)fillColor;


/**
 * Creates a new nested collection of map objects.
 */
- (nullable YMKMapObjectCollection *)addCollection;


/**
 * Traverses through the collection with a visitor object. Used for
 * iteration over map objects in the collection.
 */
- (void)traverseWithMapObjectVisitor:(nullable id<YMKMapObjectVisitor>)mapObjectVisitor;


/**
 * Removes the given map object from the collection.
 */
- (void)removeWithMapObject:(nullable YMKMapObject *)mapObject;


/**
 * Removes all map objects from the collection.
 */
- (void)clear;


/**
 * Adds a listener to track notifications of changes to the collection.
 */
- (void)addListenerWithCollectionListener:(nullable id<YMKMapObjectCollectionListener>)collectionListener;


/**
 * Sets conflict resolving mode for placemarks in this collection. Child
 * collections inherit value of this property, but can override it.
 * Default value is Ignore.
 */
- (void)setConflictResolvingModeWithMode:(YMKConflictResolvingMode)mode;


@end

