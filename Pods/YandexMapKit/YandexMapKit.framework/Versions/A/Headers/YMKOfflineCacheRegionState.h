#import <Foundation/Foundation.h>

/**
 * @attention This feature is not available in the free MapKit version.
 */
typedef NS_ENUM(NSUInteger, YMKOfflineCacheRegionState) {

    /**
     * Available for download on server.
     */
    YMKOfflineCacheRegionStateAvailable,

    /**
     * Download in progress (or paused).
     */
    YMKOfflineCacheRegionStateDownloading,

    /**
     * Download is paused.
     */
    YMKOfflineCacheRegionStatePaused,

    /**
     * Map is unpacking.
     */
    YMKOfflineCacheRegionStateUnpacking,

    /**
     * Whole map loaded in local store.
     */
    YMKOfflineCacheRegionStateCompleted
};

