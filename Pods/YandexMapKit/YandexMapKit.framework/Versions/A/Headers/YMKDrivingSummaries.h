#import <YandexMapKit/YMKDrivingSummary.h>

@interface YMKDrivingSummaries : NSObject

@property (nonatomic, readonly, nonnull) NSArray<YMKDrivingSummary *> *summaries;


+ (nonnull YMKDrivingSummaries *)summariesWithSummaries:(nonnull NSArray<YMKDrivingSummary *> *)summaries;


@end

