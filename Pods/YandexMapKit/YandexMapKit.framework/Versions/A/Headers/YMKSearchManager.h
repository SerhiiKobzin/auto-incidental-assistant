#import <YandexMapKit/YMKBoundingBox.h>
#import <YandexMapKit/YMKGeometry.h>
#import <YandexMapKit/YMKPoint.h>
#import <YandexMapKit/YMKPolyline.h>
#import <YandexMapKit/YMKSearchOptions.h>
#import <YandexMapKit/YMKSearchSession.h>
#import <YandexMapKit/YMKSuggestItem.h>

#import <YandexRuntime/YRTPlatformBinding.h>

typedef void(^YMKSearchManagerResponseHandler)(
    NSArray<YMKSuggestItem *> *suggest,
    NSError *error);

/**
 * Main interface to start search.
 */
@interface YMKSearchManager : YRTPlatformBinding

/**
 * Search request for searching a user query near given geometry.
 *
 * @param text User query.
 * @param geometry Geometry to search near. Supported types: point,
 * bounding box, polyline and polygon. If the polyline is provided,
 * setSortByDistance(polyline) is assumed on the first request. Polygon
 * is expected to be a search window: 4 points in outer ring (or 5 if
 * the last point is equal to the first) and no inner rings.
 * @param searchOptions Various additional search parameters, see
 * YMKSearchOptions definition for details.
 * @param searchListener Listener to handle search result.
 *
 * @return YMKSearchSession which allows further searches, cancel and
 * retry.
 */
- (nullable YMKSearchSession *)submitWithText:(nonnull NSString *)text
                                     geometry:(nonnull YMKGeometry *)geometry
                                searchOptions:(nonnull YMKSearchOptions *)searchOptions
                              responseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


/**
 * Search request that is used to search for a user query along the
 * given polyline inside the given window.
 *
 * @param text User query.
 * @param polyline Polyline to search near;
 * YMKSearchSession::setSortByDistanceWithOrigin: is assumed on the
 * first request.
 * @param geometry Geometry to search near; supported types: point,
 * bounding box, polyline and polygon. Polygon is expected to be a
 * search window: 4 points in outer ring (or 5 if the last point is
 * equal to first) and no inner rings.
 * @param searchOptions Various additional search parameters, see
 * YMKSearchOptions definition for details.
 * @param searchListener Listener to handle search result.
 *
 * @return YMKSearchSession which allows further searches, cancel and
 * retry. Session should be stored by user or search is automatically
 * cancelled.
 */
- (nullable YMKSearchSession *)submitWithText:(nonnull NSString *)text
                                     polyline:(nonnull YMKPolyline *)polyline
                                     geometry:(nonnull YMKGeometry *)geometry
                                searchOptions:(nonnull YMKSearchOptions *)searchOptions
                              responseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


/**
 * Reverse search request (to search objects at the given coordinates)
 *
 * @param point Coordinates to search at.
 * @param zoom Current zoom level. Skips objects that are too small for
 * a given zoom level.
 * @param searchOptions Additional search parameters, see
 * YMKSearchOptions definition for details. Currently the only supported
 * options are YMKSearchOptions::origin, YMKSearchOptions::searchTypes
 * and YMKSearchOptions::snippets. Only 'geo' and 'biz' types are
 * supported and not at the same time.
 * @param searchListener Listener to handle search result.
 *
 * @return YMKSearchSession which allows further searches, cancel and
 * retry. Session should be stored by user or search is automatically
 * cancelled.
 *
 * Remark:
 * @param zoom has optional type, it may be uninitialized.
 */
- (nullable YMKSearchSession *)submitWithPoint:(nonnull YMKPoint *)point
                                          zoom:(nullable NSNumber *)zoom
                                 searchOptions:(nonnull YMKSearchOptions *)searchOptions
                               responseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


/**
 * Search request for URI resolution.
 *
 * @param uri Object uri.
 * @param searchOptions Additional search parameters, see
 * YMKSearchOptions definition for details. Currently the only supported
 * options are YMKSearchOptions::origin and YMKSearchOptions::snippets.
 * @param searchListener Listener to handle search result.
 *
 * @return YMKSearchSession which allows search cancel and retry. Should
 * be stored by user or search is automatically cancelled.
 */
- (nullable YMKSearchSession *)resolveURIWithUri:(nonnull NSString *)uri
                                   searchOptions:(nonnull YMKSearchOptions *)searchOptions
                                 responseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;


/// @cond EXCLUDE
/**
 * Deprecated. Use
 * YMKSearchManager::resolveURIWithUri:searchOptions:responseHandler:
 * instead.
 *
 * @param oid Id of organization.
 * @param searchOptions Various additional search parameters, see
 * YMKSearchOptions definition for details. Currently the only supported
 * options are YMKSearchOptions::origin and YMKSearchOptions::snippets.
 * @param searchListener Listener to handle search result.
 *
 * @return YMKSearchSession which allows search cancel and retry. Should
 * be stored by user or search is automatically cancelled.
 */
- (nullable YMKSearchSession *)searchByOidWithOid:(nonnull NSString *)oid
                                    searchOptions:(nonnull YMKSearchOptions *)searchOptions
                                  responseHandler:(nullable YMKSearchSessionResponseHandler)responseHandler;
/// @endcond


/**
 * Begin a suggest request. The current request is cancelled, if
 * present.
 *
 * @param text Text to get suggestions for.
 * @param window Current map window position.
 * @param searchOptions Various additional suggest parameters. See the
 * YMKSearchOptions definition for details. Currently supported options
 * are YMKSearchOptions::searchTypes and YMKSearchOptions::userPosition.
 * Only 'geo', 'biz' and 'transit' types are supported by the suggest
 * backend. If there are no supported types in
 * searchOptions.searchTypes, the result is empty.
 * @param suggestListener Function called when the result is ready.
 */
- (void)suggestWithText:(nonnull NSString *)text
                 window:(nonnull YMKBoundingBox *)window
          searchOptions:(nonnull YMKSearchOptions *)searchOptions
        responseHandler:(nullable YMKSearchManagerResponseHandler)responseHandler;


/**
 * Cancel current suggest request.
 */
- (void)cancelSuggest;


@end

