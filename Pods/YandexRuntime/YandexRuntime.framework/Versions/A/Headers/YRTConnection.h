#import <YandexRuntime/YRTClient.h>

/// @cond EXCLUDE
@protocol YRTConnection <NSObject>

- (BOOL)connectWithClient:(nullable YRTClient *)client;


- (void)disconnect;


@end
/// @endcond
