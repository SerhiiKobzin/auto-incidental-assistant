#import <YandexRuntime/YRTTestSupportLiteTestStructure.h>
#import <YandexRuntime/YRTTestSupportOptionsTestStructure.h>
#import <YandexRuntime/YRTTestSupportTestStructure.h>
#import <YandexRuntime/YRTTestSupportTestTypes.h>

#import <UIKit/UIKit.h>

/// @cond EXCLUDE
@interface YRTTestSupportFullTestStructure : NSObject

@property (nonatomic, readonly) BOOL b;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *ob;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vb;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vob;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *db;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dob;

@property (nonatomic, readonly) NSInteger i;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oi;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vi;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *voi;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *di;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *doi;

@property (nonatomic, readonly) NSUInteger ui;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oui;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vui;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *voui;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dui;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *doui;

@property (nonatomic, readonly) long long i64;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oi64;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vi64;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *voi64;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *di64;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *doi64;

@property (nonatomic, readonly) float fl;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *ofl;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vfl;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vofl;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dfl;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dofl;

@property (nonatomic, readonly) double d;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *od;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vd;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vod;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dd;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dod;

@property (nonatomic, readonly, nonnull) NSString *s;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSString *os;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vs;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vos;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *ds;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dos;

@property (nonatomic, readonly) NSTimeInterval ti;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oti;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vti;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *voti;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dti;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *doti;

@property (nonatomic, readonly, nonnull) NSDate *at;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSDate *oat;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vat;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *voat;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dat;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *doat;

@property (nonatomic, readonly, nonnull) NSDate *rt;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSDate *ort;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vrt;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vort;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *drt;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dort;

@property (nonatomic, readonly, nonnull) NSData *by;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSData *oby;

@property (nonatomic, readonly, nonnull) UIColor *c;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) UIColor *oc;

@property (nonatomic, readonly, nonnull) NSArray<UIColor *> *vc;

@property (nonatomic, readonly, nonnull) NSArray<UIColor *> *voc;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, UIColor *> *dc;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, UIColor *> *doc;

@property (nonatomic, readonly) CGPoint p;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSValue *op;

@property (nonatomic, readonly, nonnull) NSArray<NSValue *> *vp;

@property (nonatomic, readonly, nonnull) NSArray<NSValue *> *vop;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSValue *> *dp;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSValue *> *dop;

@property (nonatomic, readonly) YRTTestSupportTestEnum e;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *oe;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *ve;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *voe;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *de;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *doe;

@property (nonatomic, readonly) YRTTestSupportTestBitfieldEnum be;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) NSNumber *obe;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vbe;

@property (nonatomic, readonly, nonnull) NSArray<NSNumber *> *vobe;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dbe;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, NSNumber *> *dobe;

@property (nonatomic, readonly, nonnull) YRTTestSupportTestStructure *ts;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YRTTestSupportTestStructure *ots;

@property (nonatomic, readonly, nonnull) NSArray<YRTTestSupportTestStructure *> *vts;

@property (nonatomic, readonly, nonnull) NSArray<YRTTestSupportTestStructure *> *vots;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, YRTTestSupportTestStructure *> *dts;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, YRTTestSupportTestStructure *> *dots;

@property (nonatomic, readonly, nonnull) YRTTestSupportLiteTestStructure *lts;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YRTTestSupportLiteTestStructure *olts;

@property (nonatomic, readonly, nonnull) NSArray<YRTTestSupportLiteTestStructure *> *vlts;

@property (nonatomic, readonly, nonnull) NSArray<YRTTestSupportLiteTestStructure *> *volts;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, YRTTestSupportLiteTestStructure *> *dlts;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, YRTTestSupportLiteTestStructure *> *dolts;

@property (nonatomic, readonly, nonnull) YRTTestSupportOptionsTestStructure *opts;

/**
 * Optional property, can be null.
 */
@property (nonatomic, readonly, nullable) YRTTestSupportOptionsTestStructure *oopts;

@property (nonatomic, readonly, nonnull) NSArray<YRTTestSupportOptionsTestStructure *> *vopts;

@property (nonatomic, readonly, nonnull) NSArray<YRTTestSupportOptionsTestStructure *> *voopts;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, YRTTestSupportOptionsTestStructure *> *dopts;

@property (nonatomic, readonly, nonnull) NSDictionary<NSString *, YRTTestSupportOptionsTestStructure *> *doopts;


+ (nonnull YRTTestSupportFullTestStructure *)fullTestStructureWithB:( BOOL)b
                                                                 ob:(nullable NSNumber *)ob
                                                                 vb:(nonnull NSArray<NSNumber *> *)vb
                                                                vob:(nonnull NSArray<NSNumber *> *)vob
                                                                 db:(nonnull NSDictionary<NSString *, NSNumber *> *)db
                                                                dob:(nonnull NSDictionary<NSString *, NSNumber *> *)dob
                                                                  i:( NSInteger)i
                                                                 oi:(nullable NSNumber *)oi
                                                                 vi:(nonnull NSArray<NSNumber *> *)vi
                                                                voi:(nonnull NSArray<NSNumber *> *)voi
                                                                 di:(nonnull NSDictionary<NSString *, NSNumber *> *)di
                                                                doi:(nonnull NSDictionary<NSString *, NSNumber *> *)doi
                                                                 ui:( NSUInteger)ui
                                                                oui:(nullable NSNumber *)oui
                                                                vui:(nonnull NSArray<NSNumber *> *)vui
                                                               voui:(nonnull NSArray<NSNumber *> *)voui
                                                                dui:(nonnull NSDictionary<NSString *, NSNumber *> *)dui
                                                               doui:(nonnull NSDictionary<NSString *, NSNumber *> *)doui
                                                                i64:( long long)i64
                                                               oi64:(nullable NSNumber *)oi64
                                                               vi64:(nonnull NSArray<NSNumber *> *)vi64
                                                              voi64:(nonnull NSArray<NSNumber *> *)voi64
                                                               di64:(nonnull NSDictionary<NSString *, NSNumber *> *)di64
                                                              doi64:(nonnull NSDictionary<NSString *, NSNumber *> *)doi64
                                                                 fl:( float)fl
                                                                ofl:(nullable NSNumber *)ofl
                                                                vfl:(nonnull NSArray<NSNumber *> *)vfl
                                                               vofl:(nonnull NSArray<NSNumber *> *)vofl
                                                                dfl:(nonnull NSDictionary<NSString *, NSNumber *> *)dfl
                                                               dofl:(nonnull NSDictionary<NSString *, NSNumber *> *)dofl
                                                                  d:( double)d
                                                                 od:(nullable NSNumber *)od
                                                                 vd:(nonnull NSArray<NSNumber *> *)vd
                                                                vod:(nonnull NSArray<NSNumber *> *)vod
                                                                 dd:(nonnull NSDictionary<NSString *, NSNumber *> *)dd
                                                                dod:(nonnull NSDictionary<NSString *, NSNumber *> *)dod
                                                                  s:(nonnull NSString *)s
                                                                 os:(nullable NSString *)os
                                                                 vs:(nonnull NSArray<NSNumber *> *)vs
                                                                vos:(nonnull NSArray<NSNumber *> *)vos
                                                                 ds:(nonnull NSDictionary<NSString *, NSNumber *> *)ds
                                                                dos:(nonnull NSDictionary<NSString *, NSNumber *> *)dos
                                                                 ti:( NSTimeInterval)ti
                                                                oti:(nullable NSNumber *)oti
                                                                vti:(nonnull NSArray<NSNumber *> *)vti
                                                               voti:(nonnull NSArray<NSNumber *> *)voti
                                                                dti:(nonnull NSDictionary<NSString *, NSNumber *> *)dti
                                                               doti:(nonnull NSDictionary<NSString *, NSNumber *> *)doti
                                                                 at:(nonnull NSDate *)at
                                                                oat:(nullable NSDate *)oat
                                                                vat:(nonnull NSArray<NSNumber *> *)vat
                                                               voat:(nonnull NSArray<NSNumber *> *)voat
                                                                dat:(nonnull NSDictionary<NSString *, NSNumber *> *)dat
                                                               doat:(nonnull NSDictionary<NSString *, NSNumber *> *)doat
                                                                 rt:(nonnull NSDate *)rt
                                                                ort:(nullable NSDate *)ort
                                                                vrt:(nonnull NSArray<NSNumber *> *)vrt
                                                               vort:(nonnull NSArray<NSNumber *> *)vort
                                                                drt:(nonnull NSDictionary<NSString *, NSNumber *> *)drt
                                                               dort:(nonnull NSDictionary<NSString *, NSNumber *> *)dort
                                                                 by:(nonnull NSData *)by
                                                                oby:(nullable NSData *)oby
                                                                  c:(nonnull UIColor *)c
                                                                 oc:(nullable UIColor *)oc
                                                                 vc:(nonnull NSArray<UIColor *> *)vc
                                                                voc:(nonnull NSArray<UIColor *> *)voc
                                                                 dc:(nonnull NSDictionary<NSString *, UIColor *> *)dc
                                                                doc:(nonnull NSDictionary<NSString *, UIColor *> *)doc
                                                                  p:( CGPoint)p
                                                                 op:(nullable NSValue *)op
                                                                 vp:(nonnull NSArray<NSValue *> *)vp
                                                                vop:(nonnull NSArray<NSValue *> *)vop
                                                                 dp:(nonnull NSDictionary<NSString *, NSValue *> *)dp
                                                                dop:(nonnull NSDictionary<NSString *, NSValue *> *)dop
                                                                  e:( YRTTestSupportTestEnum)e
                                                                 oe:(nullable NSNumber *)oe
                                                                 ve:(nonnull NSArray<NSNumber *> *)ve
                                                                voe:(nonnull NSArray<NSNumber *> *)voe
                                                                 de:(nonnull NSDictionary<NSString *, NSNumber *> *)de
                                                                doe:(nonnull NSDictionary<NSString *, NSNumber *> *)doe
                                                                 be:( YRTTestSupportTestBitfieldEnum)be
                                                                obe:(nullable NSNumber *)obe
                                                                vbe:(nonnull NSArray<NSNumber *> *)vbe
                                                               vobe:(nonnull NSArray<NSNumber *> *)vobe
                                                                dbe:(nonnull NSDictionary<NSString *, NSNumber *> *)dbe
                                                               dobe:(nonnull NSDictionary<NSString *, NSNumber *> *)dobe
                                                                 ts:(nonnull YRTTestSupportTestStructure *)ts
                                                                ots:(nullable YRTTestSupportTestStructure *)ots
                                                                vts:(nonnull NSArray<YRTTestSupportTestStructure *> *)vts
                                                               vots:(nonnull NSArray<YRTTestSupportTestStructure *> *)vots
                                                                dts:(nonnull NSDictionary<NSString *, YRTTestSupportTestStructure *> *)dts
                                                               dots:(nonnull NSDictionary<NSString *, YRTTestSupportTestStructure *> *)dots
                                                                lts:(nonnull YRTTestSupportLiteTestStructure *)lts
                                                               olts:(nullable YRTTestSupportLiteTestStructure *)olts
                                                               vlts:(nonnull NSArray<YRTTestSupportLiteTestStructure *> *)vlts
                                                              volts:(nonnull NSArray<YRTTestSupportLiteTestStructure *> *)volts
                                                               dlts:(nonnull NSDictionary<NSString *, YRTTestSupportLiteTestStructure *> *)dlts
                                                              dolts:(nonnull NSDictionary<NSString *, YRTTestSupportLiteTestStructure *> *)dolts
                                                               opts:(nonnull YRTTestSupportOptionsTestStructure *)opts
                                                              oopts:(nullable YRTTestSupportOptionsTestStructure *)oopts
                                                              vopts:(nonnull NSArray<YRTTestSupportOptionsTestStructure *> *)vopts
                                                             voopts:(nonnull NSArray<YRTTestSupportOptionsTestStructure *> *)voopts
                                                              dopts:(nonnull NSDictionary<NSString *, YRTTestSupportOptionsTestStructure *> *)dopts
                                                             doopts:(nonnull NSDictionary<NSString *, YRTTestSupportOptionsTestStructure *> *)doopts;


@end
/// @endcond

