#import <Foundation/Foundation.h>

/// @cond EXCLUDE
typedef NS_ENUM(NSUInteger, YRTTestSupportTestEnum) {

    YRTTestSupportTestEnumFirst,

    YRTTestSupportTestEnumSecond,

    YRTTestSupportTestEnumThird
};
/// @endcond


/// @cond EXCLUDE
typedef NS_OPTIONS(NSUInteger, YRTTestSupportTestBitfieldEnum) {

    YRTTestSupportTestBitfieldEnumOne = 1,

    YRTTestSupportTestBitfieldEnumTwo = YRTTestSupportTestBitfieldEnumOne << 1,

    YRTTestSupportTestBitfieldEnumFour = YRTTestSupportTestBitfieldEnumTwo << 1,

    YRTTestSupportTestBitfieldEnumEight = YRTTestSupportTestBitfieldEnumFour << 1
};
/// @endcond

