#import <YandexRuntime/YRTI18nManager.h>
#import <YandexRuntime/YRTPlatformBinding.h>

@interface YRTI18nManagerFactory : YRTPlatformBinding

/**
 * Returns the locale currently used by the runtime.
 */
+ (void)getLocaleWithLocaleDelegate:(nullable YRTLocaleDelegate)localeDelegate;


/**
 * Sets the runtime locale. The user needs to restart the application
 * for the changes to take effect.
 */
+ (void)setLocaleWithLanguage:(nonnull NSString *)language
                      country:(nonnull NSString *)country
         localeUpdateDelegate:(nullable YRTLocaleUpdateDelegate)localeUpdateDelegate;


/**
 * Sets a custom library language. Country settings are determined by
 * system preferences. The user needs to restart the application for the
 * changes to take effect.
 */
+ (void)setLocaleWithLanguage:(nonnull NSString *)language
         localeUpdateDelegate:(nullable YRTLocaleUpdateDelegate)localeUpdateDelegate;


/**
 * Resets locale settings to system defaults.
 */
+ (void)resetLocaleWithLocaleResetDelegate:(nullable YRTLocaleResetDelegate)localeResetDelegate;


+ (nullable YRTI18nManager *)getI18nManagerInstance;


@end
