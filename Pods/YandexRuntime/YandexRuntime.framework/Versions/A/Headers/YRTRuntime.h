#import <YandexRuntime/YRTPlatformBinding.h>

typedef void(^YRTFailedAssertionListener)(
    NSString *file,
    NSInteger line,
    NSString *condition,
    NSString *message,
    NSArray<NSString *> *stack);

@interface YRTRuntime : YRTPlatformBinding

/// @cond EXCLUDE
+ (nonnull NSString *)version;
/// @endcond


/// @cond EXCLUDE
+ (void)setFailedAssertionListener:(nullable YRTFailedAssertionListener)failedAssertionListener;
/// @endcond


@end
