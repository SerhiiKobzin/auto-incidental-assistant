#import <YandexRuntime/YRTPlatformBinding.h>

@class YRTReportData;

/// @cond EXCLUDE
@interface YRTReportFactory : YRTPlatformBinding

- (nullable YRTReportData *)createReportDataWithData:(nonnull NSData *)data;


@end
/// @endcond

