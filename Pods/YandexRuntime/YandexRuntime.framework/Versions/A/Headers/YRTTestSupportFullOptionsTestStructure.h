#import <YandexRuntime/YRTTestSupportTestTypes.h>

#import <UIKit/UIKit.h>

@interface YRTTestSupportFullOptionsTestStructure : NSObject

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *b;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *i;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *ui;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *i64;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *fl;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *d;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSString *s;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *ti;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSDate *at;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSDate *rt;

/**
 * Optional property, can be null.
 */
@property (nonatomic, strong) NSData *by;

/**
 * Optional property, can be null.
 */
@property (nonatomic, strong) UIColor *c;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSValue *p;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *e;

/**
 * Optional property, can be null.
 */
@property (nonatomic, copy) NSNumber *be;

+ (nonnull YRTTestSupportFullOptionsTestStructure *)fullOptionsTestStructureWithB:(nullable NSNumber *)b
                                                                                i:(nullable NSNumber *)i
                                                                               ui:(nullable NSNumber *)ui
                                                                              i64:(nullable NSNumber *)i64
                                                                               fl:(nullable NSNumber *)fl
                                                                                d:(nullable NSNumber *)d
                                                                                s:(nullable NSString *)s
                                                                               ti:(nullable NSNumber *)ti
                                                                               at:(nullable NSDate *)at
                                                                               rt:(nullable NSDate *)rt
                                                                               by:(nullable NSData *)by
                                                                                c:(nullable UIColor *)c
                                                                                p:(nullable NSValue *)p
                                                                                e:(nullable NSNumber *)e
                                                                               be:(nullable NSNumber *)be;


@end
