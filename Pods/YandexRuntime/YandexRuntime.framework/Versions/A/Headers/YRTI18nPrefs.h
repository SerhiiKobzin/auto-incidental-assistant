#import <YandexRuntime/YRTPrefs.h>

@interface YRTI18nPrefs : NSObject

@property (nonatomic, readonly) YRTSystemOfMeasurement som;

@property (nonatomic, readonly) YRTTimeFormat timeFormat;


+ (nonnull YRTI18nPrefs *)i18nPrefsWithSom:( YRTSystemOfMeasurement)som
                                timeFormat:( YRTTimeFormat)timeFormat;


@end

