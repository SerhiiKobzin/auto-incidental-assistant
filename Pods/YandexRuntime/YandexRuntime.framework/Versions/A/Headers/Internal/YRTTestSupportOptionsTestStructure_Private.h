#import <YandexRuntime/YRTTestSupportOptionsTestStructure.h>

#import <yandex/maps/runtime/bindings/ios/to_native.h>
#import <yandex/maps/runtime/bindings/ios/to_platform.h>
#import <yandex/maps/runtime/internal/test_support/test_types.h>

namespace yandex {
namespace maps {
namespace runtime {
namespace bindings {
namespace ios {
namespace internal {

template <>
struct ToNative<::yandex::maps::runtime::internal::test_support::OptionsTestStructure, YRTTestSupportOptionsTestStructure, void> {
    static ::yandex::maps::runtime::internal::test_support::OptionsTestStructure from(
        YRTTestSupportOptionsTestStructure* platformOptionsTestStructure);
};

template <typename PlatformType>
struct ToNative<::yandex::maps::runtime::internal::test_support::OptionsTestStructure, PlatformType,
        typename std::enable_if<
            std::is_convertible<PlatformType, YRTTestSupportOptionsTestStructure*>::value>::type> {
    static ::yandex::maps::runtime::internal::test_support::OptionsTestStructure from(
        PlatformType platformOptionsTestStructure)
    {
        return ToNative<::yandex::maps::runtime::internal::test_support::OptionsTestStructure, YRTTestSupportOptionsTestStructure>::from(
            platformOptionsTestStructure);
    }
};

template <>
struct ToPlatform<::yandex::maps::runtime::internal::test_support::OptionsTestStructure> {
    static YRTTestSupportOptionsTestStructure* from(
        const ::yandex::maps::runtime::internal::test_support::OptionsTestStructure& optionsTestStructure);
};

} // namespace internal
} // namespace ios
} // namespace bindings
} // namespace runtime
} // namespace maps
} // namespace yandex
