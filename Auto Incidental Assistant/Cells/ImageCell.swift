//
//  ImageCell.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/11/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
