//
//  ExpandedImageCell.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/11/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

protocol ExpandedImageCellDelegate: class {
    
    func deleteButtonTouchUpInside()
    func retakeButtonTouchUpInside()
    
}

class ExpandedImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    weak var delegate: ExpandedImageCellDelegate?
    
    @IBAction func deleteButtonTouchUpInside(_ sender: UIButton) {
        delegate?.deleteButtonTouchUpInside()
    }
    
    @IBAction func retakeButtonTouchUpInside(_ sender: UIButton) {
        delegate?.retakeButtonTouchUpInside()
    }
    
}
