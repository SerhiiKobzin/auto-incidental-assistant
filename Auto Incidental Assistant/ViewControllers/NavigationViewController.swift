//
//  NavigationViewController.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/11/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

class NavigationViewController: UIViewController {
    
    @IBOutlet weak var locationDateTimeCheckboxLabel: UILabel!
    @IBOutlet weak var injuredDriverCheckboxLabel: UILabel!
    @IBOutlet weak var causerDriverCheckboxLabel: UILabel!
    @IBOutlet weak var photosCheckboxLabel: UILabel!
    @IBOutlet weak var schemeCheckboxLabel: UILabel!
    @IBOutlet var arrowLabels: [UILabel]!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationItem()
        setupArrowLabels()
        setupButtons()
        setupCloseButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupCheckboxLabels()
        setupSendButton()
    }
    
    @IBAction func locationDateTimeButtonTouchUpInside(_ sender: UIButton) {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Location Date Time View Controller") as? LocationDateTimeViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func injuredDriverButtonTouchUpInside(_ sender: UIButton) {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Driver View Controller") as? DriverViewController else {
            return
        }
        viewController.driverRole = .injured
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func causerDriverButtonTouchUpInside(_ sender: UIButton) {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Driver View Controller") as? DriverViewController else {
            return
        }
        viewController.driverRole = .causer
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func photosButtonTouchUpInside(_ sender: UIButton) {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Photos View Controller") as? PhotosViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func schemeButtonTouchUpInside(_ sender: UIButton) {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Scheme View Controller") as? SchemeViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func sendButtonTouchUpInside(_ sender: UIButton) {
        AlertManager.shared.showAlert(title: "Отправить данные", message: "Нажав кнопку \"Отправить\" Вы даете свое согласие на обработку ваших персональных данных", positiveTitle: "Отправить", negativeTitle: "Отменить", positiveActionHandler: { [weak self] _ in
            self?.showActivityIndicatorViewController()
            self?.sendData(completionHandler: { regNum in
                self?.hideActivityIndicatorViewController()
                guard let regNum = regNum else {
                    AlertManager.shared.showAlert(title: "Ошибка", message: "Во время отправки данных возникла ошибка", positiveTitle: "Закрыть", negativeTitle: nil, positiveActionHandler: { _ in
                        self?.goToLandingViewController()
                    }, negativeActionHandler: nil, presenter: self)
                    return
                }
                AlertManager.shared.showAlert(title: "Случай ДТП зарегистрирован\n№ " + regNum.description, message: "Для дальнейшего оформления прибудьте в ЦОДТП. Адрес Вы получите по СМС", positiveTitle: "Закрыть", negativeTitle: nil, positiveActionHandler: { _ in
                    UserDefaultsManager.shared.removeAllData()
                    self?.goToLandingViewController()
                }, negativeActionHandler: nil, presenter: self)
            })
        }, negativeActionHandler: nil, presenter: self)
    }
    
    @IBAction func deleteButtonTouchUpInside(_ sender: UIButton) {
        AlertManager.shared.showAlert(title: "Удалить ДТП", message: "После удаления регистрации ДТП пропадет вся внесенная информация.\nВы уверены что хотите удалить регистрацию ДТП?", positiveTitle: "Удалить", negativeTitle: "Отменить", positiveActionHandler: { [weak self] _ in
            UserDefaultsManager.shared.removeAllData()
            self?.goToLandingViewController()
        }, negativeActionHandler: nil, presenter: self)
    }
    
    @IBAction func closeButtonTouchUpInside(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension NavigationViewController {
    
    func setupNavigationItem() {
        navigationItem.setHidesBackButton(true, animated: true)
    }
    
    func setupCheckboxLabels() {
        locationDateTimeCheckboxLabel.text = UserDefaultsManager.shared.restoreValidationResultForLocationDateTimeViewController() ? "\u{f046}" : "\u{f096}"
        injuredDriverCheckboxLabel.text = UserDefaultsManager.shared.restoreValidationResultForDriverViewController(driverRole: .injured) ? "\u{f046}" : "\u{f096}"
        causerDriverCheckboxLabel.text = UserDefaultsManager.shared.restoreValidationResultForDriverViewController(driverRole: .causer) ? "\u{f046}" : "\u{f096}"
        photosCheckboxLabel.text = UserDefaultsManager.shared.restoreValidationResultForPhotosViewController() ? "\u{f046}" : "\u{f096}"
        schemeCheckboxLabel.text = UserDefaultsManager.shared.restoreValidationResultForSchemeViewController() ? "\u{f046}" : "\u{f096}"
    }
    
    func setupArrowLabels() {
        arrowLabels.forEach({ $0.text = "\u{f061}" })
    }
    
    func setupButtons() {
        [sendButton, deleteButton].forEach({
            let layer = $0?.layer
            layer?.shadowColor = UIColor.black.cgColor
            layer?.shadowOpacity = 0.5
            layer?.shadowOffset = CGSize(width: 0, height: 1)
            layer?.shadowRadius = 2
        })
    }
    
    func setupSendButton() {
        sendButton.isUserInteractionEnabled = validateData()
        sendButton.backgroundColor = validateData() ? UIColor(red: 0 / 255, green: 150 / 255, blue: 255 / 255, alpha: 1.0) : .lightGray
    }
    
    func setupCloseButton() {
        closeButton.setTitle("\u{f00d}", for: .normal)
    }
    
    func validateData() -> Bool {
        return UserDefaultsManager.shared.restoreValidationResultForLocationDateTimeViewController() && UserDefaultsManager.shared.restoreValidationResultForDriverViewController(driverRole: .injured) && UserDefaultsManager.shared.restoreValidationResultForDriverViewController(driverRole: .causer) && UserDefaultsManager.shared.restoreValidationResultForPhotosViewController() && UserDefaultsManager.shared.restoreValidationResultForSchemeViewController()
    }
    
    func sendData(completionHandler: @escaping (Int?) -> ()) {
        getAuthToken(completionHandler: { [weak self] authToken in
            guard let authToken = authToken else {
                completionHandler(nil)
                return
            }
            self?.postAccidentData(authToken: authToken, completionHandler: { accidentId, regNum in
                guard let accidentId = accidentId, let regNum = regNum else {
                    completionHandler(nil)
                    return
                }
                self?.postSchemeFileAndUpdateAccidentData(authToken: authToken, accidentId: accidentId, completionHandler: { success in
                    guard success else {
                        completionHandler(nil)
                        return
                    }
                    self?.postPhotoFilesAndCreateAccidentImages(authToken: authToken, accidentId: accidentId, completionHandler: { success in
                        guard success else {
                            completionHandler(nil)
                            return
                        }
                        completionHandler(regNum)
                    })
                })
            })
        })
    }
    
    func getAuthToken(completionHandler: @escaping (String?) -> ()) {
        NetworkManager.shared.getAuthToken(completionHandler: completionHandler)
    }
    
    func postAccidentData(authToken: String, completionHandler: @escaping (String?, Int?) -> ()) {
        guard let address = UserDefaultsManager.shared.restoreAddress(), let date = UserDefaultsManager.shared.restoreDate(), let dateServerString = DateConverter.shared.dateToServerDateString(date: date), let latitude = UserDefaultsManager.shared.restoreCoordinate()?.latitude, let longitude = UserDefaultsManager.shared.restoreCoordinate()?.longitude, let injuredDriverPhone = UserDefaultsManager.shared.restoreDriverPhone(driverRole: .injured)?.replacingOccurrences(of: "+7 (", with: "").replacingOccurrences(of: ") ", with: "").replacingOccurrences(of: " ", with: ""), let injuredDriverName = UserDefaultsManager.shared.restoreDriverName(driverRole: .injured), let injuredDriverLicenseSerie = UserDefaultsManager.shared.restoreDriverLicenseSerie(driverRole: .injured)?.replacingOccurrences(of: " ", with: ""), let injuredDriverLicenseNumber = UserDefaultsManager.shared.restoreDriverLicenseNumber(driverRole: .injured), var injuredVehicleRegistrationNumber = UserDefaultsManager.shared.restoreVehicleRegistrationNumber(driverRole: .injured), let injuredVehicleMaker = UserDefaultsManager.shared.restoreVehicleMaker(driverRole: .injured), let injuredVehicleModel = UserDefaultsManager.shared.restoreVehicleModel(driverRole: .injured), let injuredVehicleRegistrationCertificateSerie = UserDefaultsManager.shared.restoreVehicleRegistrationCertificateSerie(driverRole: .injured)?.replacingOccurrences(of: " ", with: ""), let injuredVehicleRegistrationCertificateNumber = UserDefaultsManager.shared.restoreVehicleRegistrationCertificateNumber(driverRole: .injured), let causerDriverPhone = UserDefaultsManager.shared.restoreDriverPhone(driverRole: .causer)?.replacingOccurrences(of: "+7 (", with: "").replacingOccurrences(of: ") ", with: "").replacingOccurrences(of: " ", with: ""), let causerDriverName = UserDefaultsManager.shared.restoreDriverName(driverRole: .causer), let causerDriverLicenseSerie = UserDefaultsManager.shared.restoreDriverLicenseSerie(driverRole: .causer)?.replacingOccurrences(of: " ", with: ""), let causerDriverLicenseNumber = UserDefaultsManager.shared.restoreDriverLicenseNumber(driverRole: .causer), var causerVehicleRegistrationNumber = UserDefaultsManager.shared.restoreVehicleRegistrationNumber(driverRole: .causer), let causerVehicleMaker = UserDefaultsManager.shared.restoreVehicleMaker(driverRole: .causer), let causerVehicleModel = UserDefaultsManager.shared.restoreVehicleModel(driverRole: .causer), let causerVehicleRegistrationCertificateSerie = UserDefaultsManager.shared.restoreVehicleRegistrationCertificateSerie(driverRole: .causer)?.replacingOccurrences(of: " ", with: ""), let causerVehicleRegistrationCertificateNumber = UserDefaultsManager.shared.restoreVehicleRegistrationCertificateNumber(driverRole: .causer) else {
            completionHandler(nil, nil)
            return
        }
        let latitudeDouble = Double(latitude)
        let longitudeDouble = Double(longitude)
        let injuredOther = UserDefaultsManager.shared.restoreOtherSwitchState(driverRole: .injured)
        if !injuredOther {
            injuredVehicleRegistrationNumber = injuredVehicleRegistrationNumber.replacingOccurrences(of: " ", with: "")
        }
        let injuredAbsent = UserDefaultsManager.shared.restoreAbsentSwitchState(driverRole: .injured)
        let injuredInsuranceCompanyName = UserDefaultsManager.shared.restoreInsuranceCompanyName(driverRole: .injured)
        let injuredInsuranceCompanyId = insuranceCompanies.first(where: { $0.1 == injuredInsuranceCompanyName })?.0
        let injuredInsurancePolicySerie = UserDefaultsManager.shared.restoreInsurancePolicySerie(driverRole: .injured)
        let injuredInsurancePolicyNumber = UserDefaultsManager.shared.restoreInsurancePolicyNumber(driverRole: .injured)
        let causerOther = UserDefaultsManager.shared.restoreOtherSwitchState(driverRole: .causer)
        if !causerOther {
            causerVehicleRegistrationNumber = causerVehicleRegistrationNumber.replacingOccurrences(of: " ", with: "")
        }
        let causerAbsent = UserDefaultsManager.shared.restoreAbsentSwitchState(driverRole: .causer)
        let causerInsuranceCompanyName = UserDefaultsManager.shared.restoreInsuranceCompanyName(driverRole: .causer)
        let causerInsuranceCompanyId = insuranceCompanies.first(where: { $0.1 == causerInsuranceCompanyName })?.0
        let causerInsurancePolicySerie = UserDefaultsManager.shared.restoreInsurancePolicySerie(driverRole: .causer)
        let causerInsurancePolicyNumber = UserDefaultsManager.shared.restoreInsurancePolicyNumber(driverRole: .causer)
        NetworkManager.shared.postAccidentData(authToken: authToken, address: address, date: dateServerString, latitude: latitudeDouble, longitude: longitudeDouble, udid: "", injuredDriverPhone: injuredDriverPhone, injuredDriverName: injuredDriverName, injuredDriverLicenseSerie: injuredDriverLicenseSerie, injuredDriverLicenseNumber: injuredDriverLicenseNumber, injuredVehicleRegistrationNumber: injuredVehicleRegistrationNumber, injuredOther: injuredOther, injuredVehicleMaker: injuredVehicleMaker, injuredVehicleModel: injuredVehicleModel, injuredVehicleRegistrationCertificateSerie: injuredVehicleRegistrationCertificateSerie, injuredVehicleRegistrationCertificateNumber: injuredVehicleRegistrationCertificateNumber, injuredAbsent: injuredAbsent, injuredInsuranceCompanyId: injuredInsuranceCompanyId, injuredInsurancePolicySerie: injuredInsurancePolicySerie, injuredInsurancePolicyNumber: injuredInsurancePolicyNumber, causerDriverPhone: causerDriverPhone, causerDriverName: causerDriverName, causerDriverLicenseSerie: causerDriverLicenseSerie, causerDriverLicenseNumber: causerDriverLicenseNumber, causerVehicleRegistrationNumber: causerVehicleRegistrationNumber, causerOther: causerOther, causerVehicleMaker: causerVehicleMaker, causerVehicleModel: causerVehicleModel, causerVehicleRegistrationCertificateSerie: causerVehicleRegistrationCertificateSerie, causerVehicleRegistrationCertificateNumber: causerVehicleRegistrationCertificateNumber, causerAbsent: causerAbsent, causerInsuranceCompanyId: causerInsuranceCompanyId, causerInsurancePolicySerie: causerInsurancePolicySerie, causerInsurancePolicyNumber: causerInsurancePolicyNumber, completionHandler: completionHandler)
    }
    
    func postSchemeFileAndUpdateAccidentData(authToken: String, accidentId: String, completionHandler: @escaping (Bool) -> ()) {
        guard let imageURL = UserDefaultsManager.shared.restoreImageURL() else {
            completionHandler(false)
            return
        }
        NetworkManager.shared.postPhotoFile(authToken: authToken, fileURL: imageURL, completionHandler: { fileId in
            guard let fileId = fileId else {
                completionHandler(false)
                return
            }
            NetworkManager.shared.updateAccidentData(authToken: authToken, accidentId: accidentId, fileId: fileId, completionHandler: completionHandler)
        })
    }
    
    func postImageFileAndCreateAccidentImage(authToken: String, imageURL: URL, accidentId: String, completionHandler: @escaping (Bool) -> ()) {
        NetworkManager.shared.postPhotoFile(authToken: authToken, fileURL: imageURL, completionHandler: { fileId in
            guard let fileId = fileId else {
                completionHandler(false)
                return
            }
            NetworkManager.shared.postAccidentImage(authToken: authToken, accidentId: accidentId, fileId: fileId, completionHandler: completionHandler)
        })
    }
    
    func postPhotoFilesAndCreateAccidentImages(authToken: String, accidentId: String, completionHandler: @escaping (Bool) -> ()) {
        guard let imageURLs = UserDefaultsManager.shared.restoreImageURLs()?.filter({ $0 != nil }) else {
            completionHandler(false)
            return
        }
        var completedFileTransmitCounter = 0
        for imageURL in imageURLs {
            guard let imageURL = imageURL else {
                continue
            }
            postImageFileAndCreateAccidentImage(authToken: authToken, imageURL: imageURL, accidentId: accidentId, completionHandler: { success in
                guard success else {
                    completionHandler(false)
                    return
                }
                completedFileTransmitCounter += 1
                if completedFileTransmitCounter == imageURLs.count {
                    completionHandler(true)
                }
            })
        }
    }
    
    func goToLandingViewController() {
        guard let viewController = navigationController?.viewControllers.first(where: { $0 as? LandingViewController != nil }) else {
            return
        }
        navigationController?.popToViewController(viewController, animated: true)
    }
    
    func showActivityIndicatorViewController() {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Activity Indicator View Controller")
        present(viewController, animated: true)
    }
    
    func hideActivityIndicatorViewController() {
        dismiss(animated: true)
    }
    
}
