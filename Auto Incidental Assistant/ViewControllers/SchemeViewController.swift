//
//  SchemeViewController.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/11/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import AVKit
import UIKit

class SchemeViewController: UIViewController {
    
    @IBOutlet weak var schemeImageView: UIImageView!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var schemeImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var wrappableViewHeightConstraint: NSLayoutConstraint!
    
    private var imageURL: URL? {
        didSet {
            if let newValue = imageURL, let image = UIImage(contentsOfFile: newValue.path) {
                schemeImageView.image = image
                schemeImageView.contentMode = .scaleAspectFit
                schemeImageViewHeightConstraint.constant = getImageHeightForImage(image: image)
                wrappableViewHeightConstraint.constant = 54.0
            } else {
                schemeImageView.image = #imageLiteral(resourceName: "AddPhotoIcon")
                schemeImageView.contentMode = .center
                schemeImageViewHeightConstraint.constant = 200.0
                wrappableViewHeightConstraint.constant = 0.0
            }
            UserDefaultsManager.shared.saveImageURL(url: imageURL)
        }
    }
    
    var validationResult = false {
        didSet {
            UserDefaultsManager.shared.saveValidationResultForSchemeViewController(validationResult: validationResult)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSchemeImageView()
        setupNavigationButton()
        setupNextButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restoreData()
    }
    
    @IBAction func deleteButtonTouchUpInside(_ sender: UIButton) {
        imageURL = nil
    }
    
    @IBAction func retakeButtonTouchUpInside(_ sender: UIButton) {
        requestForCameraUse()
    }
    
    @IBAction func navigationButtonTouchUpInside(_ sender: UIButton) {
        goToNavigationViewController()
    }
    
    @IBAction func nextButtonTouchUpInside(_ sender: UIButton) {
        validationResult = validateData()
        if !validationResult {
            AlertManager.shared.showAlert(title: "Необходимо добавить фото схемы ДТП", message: "Вы действительно хотите перейти на следующую страницу?", positiveTitle: "Перейти", negativeTitle: "Остаться", positiveActionHandler: { [weak self] _ in
                self?.goToNextViewController()
                }, negativeActionHandler: nil, presenter: self)
            return
        }
        goToNavigationViewController()
    }
    
}

extension SchemeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let metadataDictionary = info[UIImagePickerControllerMediaMetadata] as? [CFString : Any], let exifDictionary = metadataDictionary[kCGImagePropertyExifDictionary] as? [CFString : Any], let date = exifDictionary[kCGImagePropertyExifDateTimeOriginal] as? String, let coordinate = UserDefaultsManager.shared.restoreCoordinate(), let imageWithMetadata = addMetadataToImage(image: image, date: date, latitude: String(coordinate.latitude), longitude: String(coordinate.longitude)) else {
            dismiss(animated: true)
            return
        }
        UIImageWriteToSavedPhotosAlbum(imageWithMetadata, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
}

extension SchemeViewController {
    
    func setupSchemeImageView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHandler(_:)))
        schemeImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func setupNavigationButton() {
        navigationButton.setTitle("\u{f0c9}", for: .normal)
    }
    
    func setupNextButton() {
        nextButton.setTitle("\u{f061}", for: .normal)
        let layer = nextButton.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 2
    }
    
    func restoreData() {
        imageURL = UserDefaultsManager.shared.restoreImageURL()
    }
    
    func validateData() -> Bool {
        return imageURL != nil
    }
    
    func getImageHeightForImage(image: UIImage) -> CGFloat {
        let width = image.size.width
        let height = image.size.height
        let imageViewWidth = schemeImageView.bounds.size.width
        let resizeRatio = imageViewWidth / width
        return height * resizeRatio
    }
    
    @objc func tapGestureHandler(_ sender: UITapGestureRecognizer) {
        if imageURL != nil {
            return
        }
        requestForCameraUse()
    }
    
    func requestForCameraUse() {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Camera is not available on this device")
            return
        }
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            presentCamera()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] granted in
                if !granted {
                    print("Access to camera is not granted")
                    return
                }
                self?.presentCamera()
            })
        default:
            break
        }
    }
    
    func presentCamera() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        present(imagePickerController, animated: true)
    }
    
    func addMetadataToImage(image: UIImage?, date: String, latitude: String, longitude: String) -> UIImage? {
        guard let image = image else {
            return nil
        }
        let metadata = (date + "\nШ: " + latitude + " Д: " + longitude) as NSString
        let imageSize = image.size
        let attributes = [NSAttributedStringKey.foregroundColor : UIColor.yellow,
                          NSAttributedStringKey.font            : UIFont.systemFont(ofSize: 60.0)]
        let metadataSize = metadata.size(withAttributes: attributes)
        UIGraphicsBeginImageContext(imageSize)
        image.draw(in: CGRect(x: 0.0, y: 0.0, width: imageSize.width, height: imageSize.height))
        metadata.draw(at: CGPoint(x: 40.0, y: imageSize.height - metadataSize.height - 40.0), withAttributes: attributes)
        let imageWithDate = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithDate
    }
    
    func saveImageToDocumentsDirectory(image: UIImage?) -> URL? {
        guard let image = image, let data = UIImageJPEGRepresentation(image, 1.0), let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let filename = UUID().uuidString + ".jpg"
        let url = documentDirectory.appendingPathComponent(filename)
        do {
            try data.write(to: url)
            return url
        }
        catch let error {
            print(error)
            return nil
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        guard error == nil, let url = saveImageToDocumentsDirectory(image: image) else {
            dismiss(animated: true, completion: {
                AlertManager.shared.showAlert(title: "Ошибка", message: "Ошибка сохранения фото", positiveTitle: nil, negativeTitle: "Закрыть", positiveActionHandler: nil, negativeActionHandler: nil, presenter: self)
            })
            return
        }
        dismiss(animated: true, completion: { [weak self] in
            self?.imageURL = url
        })
        
    }
    
    func goToNavigationViewController() {
        if var viewControllers = navigationController?.viewControllers, let viewController = viewControllers.first(where: { $0 as? NavigationViewController != nil }) as? NavigationViewController {
            viewControllers = viewControllers.filter({ $0 != viewController })
            navigationController?.viewControllers = viewControllers
            navigationController?.pushViewController(viewController, animated: true)
            return
        }
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation View Controller") as? NavigationViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToNextViewController() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation View Controller") as? NavigationViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
