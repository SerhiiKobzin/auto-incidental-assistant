//
//  PhotosViewController.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/11/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import AVFoundation
import CoreLocation
import Photos
import UIKit

class PhotosViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    var expandedImageIndex: Int?
    
    var imageURLs: [URL?] = [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil] {
        didSet {
            visibleImageURLs = imageURLs
            UserDefaultsManager.shared.saveImageURLs(urls: imageURLs)
        }
    }
    
    var visibleImageURLs: [URL?] = [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil] {
        didSet {
            if let selectedImageIndex = selectedImageIndex, let expandedImageIndex = expandedImageIndex {
                visibleImageURLs.insert(visibleImageURLs[selectedImageIndex], at: expandedImageIndex)
            }
            collectionView.reloadData()
        }
    }
    
    var selectedImageIndex: Int? {
        didSet {
            guard let newValue = selectedImageIndex else {
                expandedImageIndex = nil
                return
            }
            expandedImageIndex = (newValue / 3 + 1) * 3
        }
    }
    
    var validationResult = false {
        didSet {
            UserDefaultsManager.shared.saveValidationResultForPhotosViewController(validationResult: validationResult)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationButton()
        setupNextButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        restoreData()
    }
    
    @IBAction func navigationButtonTouchUpInside(_ sender: UIButton) {
        goToNavigationViewController()
    }
    
    @IBAction func nextButtonTouchUpInside(_ sender: UIButton) {
        validationResult = validateData()
        if !validationResult {
            AlertManager.shared.showAlert(title: "Необходимо добавить хотя бы одно фото ДТП", message: "Вы действительно хотите перейти на следующую страницу?", positiveTitle: "Перейти", negativeTitle: "Остаться", positiveActionHandler: { [weak self] _ in
                self?.goToNextViewController()
                }, negativeActionHandler: nil, presenter: self)
            return
        }
        goToNextViewController()
    }
    
}

extension PhotosViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return visibleImageURLs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = indexPath.item
        let imageURL = visibleImageURLs[item]
        if item == expandedImageIndex {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Expanded Image Cell", for: indexPath) as? ExpandedImageCell else {
                return UICollectionViewCell()
            }
            if let imageURL = imageURL, let image = UIImage(contentsOfFile: imageURL.path) {
                cell.imageView.image = image
                cell.imageView.contentMode = .scaleAspectFill
            }
            cell.delegate = self
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image Cell", for: indexPath) as? ImageCell else {
            return UICollectionViewCell()
        }
        if let imageURL = imageURL, let image = UIImage(contentsOfFile: imageURL.path) {
            cell.imageView.image = image
            cell.imageView.contentMode = .scaleAspectFill
        } else {
            cell.imageView.image = #imageLiteral(resourceName: "AddPhotoIcon")
            cell.imageView.contentMode = .center
        }
        return cell
    }
    
}

extension PhotosViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize.zero
        }
        let item = indexPath.item
        if item == expandedImageIndex {
            return getExpandedImageCellSize(collectionView: collectionView)
        }
        let itemWidth = (collectionView.bounds.size.width - layout.minimumInteritemSpacing * 2) / 3
        collectionViewHeightConstraint.constant = itemWidth * 5 + layout.minimumLineSpacing * (expandedImageIndex == nil ? 4 : 5) + (expandedImageIndex == nil ? 0.0 : getExpandedImageCellSize(collectionView: collectionView).height)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var item = indexPath.item
        if item == expandedImageIndex {
            return
        }
        if item == selectedImageIndex {
            selectedImageIndex = nil
            visibleImageURLs = imageURLs
            return
        }
        if visibleImageURLs[item] == nil {
            requestForCameraUse()
            return
        }
        if let expandedImageIndex = expandedImageIndex, item > expandedImageIndex {
            item -= 1
        }
        selectedImageIndex = item
        visibleImageURLs = imageURLs
    }
    
}

extension PhotosViewController: ExpandedImageCellDelegate {
    
    func deleteButtonTouchUpInside() {
        if let selectedImageIndex = selectedImageIndex {
            self.selectedImageIndex = nil
            imageURLs[selectedImageIndex] = nil
        }
    }
    
    func retakeButtonTouchUpInside() {
        requestForCameraUse()
    }
    
}

extension PhotosViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage, let metadataDictionary = info[UIImagePickerControllerMediaMetadata] as? [CFString : Any], let exifDictionary = metadataDictionary[kCGImagePropertyExifDictionary] as? [CFString : Any], let date = exifDictionary[kCGImagePropertyExifDateTimeOriginal] as? String, let coordinate = UserDefaultsManager.shared.restoreCoordinate(), let imageWithMetadata = addMetadataToImage(image: image, date: date, latitude: String(coordinate.latitude), longitude: String(coordinate.longitude)) else {
            dismiss(animated: true)
            return
        }
        UIImageWriteToSavedPhotosAlbum(imageWithMetadata, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    
}

extension PhotosViewController {
    
    func setupNavigationButton() {
        navigationButton.setTitle("\u{f0c9}", for: .normal)
    }
    
    func setupNextButton() {
        nextButton.setTitle("\u{f061}", for: .normal)
        let layer = nextButton.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 2
    }
    
    func restoreData() {
        imageURLs = UserDefaultsManager.shared.restoreImageURLs() ?? [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
    }
    
    func validateData() -> Bool {
        return imageURLs.filter({ $0 != nil }).count > 0
    }
    
    func getExpandedImageCellSize(collectionView: UICollectionView) -> CGSize {
        guard let expandedImageIndex = expandedImageIndex, let expandedImageURL = visibleImageURLs[expandedImageIndex], let expandedImage = UIImage(contentsOfFile: expandedImageURL.path) else {
            return CGSize.zero
        }
        let width = expandedImage.size.width
        let height = expandedImage.size.height
        let collectionViewWidth = collectionView.bounds.size.width
        let resizeRatio = collectionViewWidth / width
        return CGSize(width: collectionViewWidth, height: height * resizeRatio + 64.0)
    }
    
    func requestForCameraUse() {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Camera is not available on this device")
            return
        }
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            presentCamera()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { [weak self] granted in
                if !granted {
                    print("Access to camera is not granted")
                    return
                }
                self?.presentCamera()
            })
        default:
            break
        }
    }
    
    func presentCamera() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        present(imagePickerController, animated: true)
    }
    
    func addMetadataToImage(image: UIImage?, date: String, latitude: String, longitude: String) -> UIImage? {
        guard let image = image else {
            return nil
        }
        let metadata = (date + "\nШ: " + latitude + " Д: " + longitude) as NSString
        let imageSize = image.size
        let attributes = [NSAttributedStringKey.foregroundColor : UIColor.yellow,
                          NSAttributedStringKey.font            : UIFont.systemFont(ofSize: 60.0)]
        let metadataSize = metadata.size(withAttributes: attributes)
        UIGraphicsBeginImageContext(imageSize)
        image.draw(in: CGRect(x: 0.0, y: 0.0, width: imageSize.width, height: imageSize.height))
        metadata.draw(at: CGPoint(x: 40.0, y: imageSize.height - metadataSize.height - 40.0), withAttributes: attributes)
        let imageWithDate = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithDate
    }
    
    func saveImageToDocumentsDirectory(image: UIImage?) -> URL? {
        guard let image = image, let data = UIImageJPEGRepresentation(image, 0.7), let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let filename = UUID().uuidString + ".jpg"
        let url = documentDirectory.appendingPathComponent(filename)
        do {
            try data.write(to: url)
            return url
        }
        catch let error {
            print(error)
            return nil
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        guard error == nil, let indexToInsertImage = collectionView.indexPathsForSelectedItems?.first?.item ?? selectedImageIndex, let url = saveImageToDocumentsDirectory(image: image) else {
            dismiss(animated: true, completion: {
                AlertManager.shared.showAlert(title: "Ошибка", message: "Ошибка сохранения фото", positiveTitle: nil, negativeTitle: "Закрыть", positiveActionHandler: nil, negativeActionHandler: nil, presenter: self)
            })
            return
        }
        dismiss(animated: true, completion: { [weak self] in
            self?.imageURLs[indexToInsertImage] = url
        })
    }
    
    func goToNavigationViewController() {
        if var viewControllers = navigationController?.viewControllers, let viewController = viewControllers.first(where: { $0 as? NavigationViewController != nil }) as? NavigationViewController {
            viewControllers = viewControllers.filter({ $0 != viewController })
            navigationController?.viewControllers = viewControllers
            navigationController?.pushViewController(viewController, animated: true)
            return
        }
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation View Controller") as? NavigationViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToNextViewController() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Scheme View Controller") as? SchemeViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
