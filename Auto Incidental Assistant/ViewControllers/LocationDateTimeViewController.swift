//
//  LocationDateTimeViewController.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 6/29/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import CoreLocation
import SkyFloatingLabelTextField
import UIKit
import YandexMapKit

class LocationDateTimeViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var yandexMapView: YMKMapView!
    @IBOutlet weak var addressTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var dateTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var timeTextField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var pickersView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    private let locationManager = CLLocationManager()
    
    private var titleColor = UIColor.gray
    
    var coordinate: CLLocationCoordinate2D? {
        didSet {
            guard let newValue = coordinate, let map = yandexMapView.mapWindow.map else {
                return
            }
            UserDefaultsManager.shared.saveCoordinate(coordinate: newValue)
            let latitude = newValue.latitude
            let longitude = newValue.longitude
            map.move(with: YMKCameraPosition(target: YMKPoint(latitude: latitude, longitude: longitude), zoom: 0.0, azimuth: 0.0, tilt: 0.0), animationType: YMKAnimation(type: .smooth, duration: 0.0), cameraCallback: nil)
            map.move(with: YMKCameraPosition(target: YMKPoint(latitude: latitude, longitude: longitude), zoom: 17.0, azimuth: 0.0, tilt: 0.0), animationType: YMKAnimation(type: .smooth, duration: 5.0), cameraCallback: nil)
            getAddressWithCoordinate(latitude: latitude, longitude: longitude)
        }
    }
    
    var address: String? {
        didSet {
            guard let newValue = address else {
                return
            }
            UserDefaultsManager.shared.saveAddress(address: newValue)
            addressTextField.text = newValue
        }
    }
    
    var date: Date? {
        didSet {
            guard let newValue = date else {
                return
            }
            UserDefaultsManager.shared.saveDate(date: newValue)
            dateTextField.text = DateConverter.shared.dateToDateString(date: newValue)
            timeTextField.text = DateConverter.shared.dateToTimeString(date: newValue)
        }
    }
    
    private var activeTextField: UITextField? {
        didSet {
            if let oldValue = oldValue as? SkyFloatingLabelTextField {
                oldValue.titleColor = titleColor
            }
            if let newValue = activeTextField as? SkyFloatingLabelTextField {
                newValue.errorMessage = nil
                titleColor = newValue.titleColor
                newValue.titleColor = newValue.selectedTitleColor
            }
            if oldValue == addressTextField {
                address = addressTextField.text
            }
            if activeTextField == nil || activeTextField == dateTextField || activeTextField == timeTextField {
                oldValue?.resignFirstResponder()
            }
            oldValue?.isUserInteractionEnabled = true
            activeTextField?.isUserInteractionEnabled = false
        }
    }
    
    var validationResult = false {
        didSet {
            UserDefaultsManager.shared.saveValidationResultForLocationDateTimeViewController(validationResult: validationResult)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        setupYandexMapView()
        setupTextFields()
        setupNavigationButton()
        setupNextButton()
        setupPickersView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
        restoreData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activeTextField = nil
        removeKeyboardNotifications()
    }
    
    @IBAction func datePickerOKButtonTouchUpInside(_ sender: UIButton) {
        date = datePicker.date
        pickersView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func datePickerCancelButtonTouchUpInside(_ sender: UIButton) {
        pickersView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func navigationButtonTouchUpInside(_ sender: UIButton) {
        activeTextField = nil
        goToNavigationViewController()
    }
    
    @IBAction func nextButtonTouchUpInside(_ sender: UIButton) {
        activeTextField = nil
        validationResult = validateData()
        if !validationResult {
            AlertManager.shared.showAlert(title: "Не все поля заполнены", message: "Вы действительно хотите перейти на следующую страницу?", positiveTitle: "Перейти", negativeTitle: "Остаться", positiveActionHandler: { [weak self] _ in
                self?.goToNextViewController()
            }, negativeActionHandler: nil, presenter: self)
            return
        }
        goToNextViewController()
    }
    
}

extension LocationDateTimeViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        if textField == dateTextField {
            datePicker.date = date ?? Date()
            datePicker.datePickerMode = .date
            pickersView.isHidden = false
            return false
        }
        if textField == timeTextField {
            datePicker.date = date ?? Date()
            datePicker.datePickerMode = .time
            pickersView.isHidden = false
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeTextField = nil
        return true
    }
    
}

extension LocationDateTimeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let coordinate = locations.last?.coordinate else {
            return
        }
        stopUpdatingLocation()
        self.coordinate = coordinate
    }
    
}

extension LocationDateTimeViewController {
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func setupYandexMapView() {
        guard let userLocationLayer = yandexMapView.mapWindow.map?.userLocationLayer else {
            return
        }
        userLocationLayer.isEnabled = true
    }
    
    func setupTextFields() {
        [addressTextField, dateTextField, timeTextField].forEach({
            $0?.titleFormatter = { $0 }
            $0?.iconFont = UIFont(name: "FontAwesome", size: 15)
        })
        addressTextField.iconText = "\u{f041}"
        dateTextField.iconText = "\u{f073}"
        timeTextField.iconText = "\u{f017}"
    }
    
    func setupNavigationButton() {
        navigationButton.setTitle("\u{f0c9}", for: .normal)
    }
    
    func setupNextButton() {
        nextButton.setTitle("\u{f061}", for: .normal)
        let layer = nextButton.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 2
    }
    
    func setupPickersView() {
        let layer = pickersView.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 2
    }
    
    func restoreData() {
        address = UserDefaultsManager.shared.restoreAddress() ?? ""
        date = UserDefaultsManager.shared.restoreDate() ?? Date()
        if let coordinate = UserDefaultsManager.shared.restoreCoordinate() {
            self.coordinate = coordinate
        } else {
            startUpdatingLocation()
        }
    }
    
    func validateData() -> Bool {
        var result = true
        if let address = self.address, address.isEmpty {
            addressTextField.errorMessage = "Введите адрес ДТП"
            result = false
        }
        return result
    }
    
    func startUpdatingLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func getAddressWithCoordinate(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        if let address = self.address, !(address.isEmpty || address == "Адрес не определен") {
            return
        }
        NetworkManager.shared.getAddressWithCoordinates(latitude: latitude.description, longitude: longitude.description, completionHandler: { [weak self] address in
            self?.address = address ?? "Адрес не определен"
        })
    }
    
    func goToNavigationViewController() {
        if var viewControllers = navigationController?.viewControllers, let viewController = viewControllers.first(where: { $0 as? NavigationViewController != nil }) as? NavigationViewController {
            viewControllers = viewControllers.filter({ $0 != viewController })
            navigationController?.viewControllers = viewControllers
            navigationController?.pushViewController(viewController, animated: true)
            return
        }
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation View Controller") as? NavigationViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToNextViewController() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Driver View Controller") as? DriverViewController else {
            return
        }
        viewController.driverRole = .injured
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            let keyboardFrameSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardFrameSize.height, 0.0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
            var rect = view.frame
            rect.size.height -= keyboardFrameSize.height
            if let activeTextFieldFrame = activeTextField?.frame, !rect.contains(activeTextFieldFrame.origin) || !rect.contains(CGPoint(x: activeTextFieldFrame.origin.x + activeTextFieldFrame.size.width, y: activeTextFieldFrame.origin.y + activeTextFieldFrame.size.height)) {
                    scrollView.scrollRectToVisible(activeTextFieldFrame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide() {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
}
