//
//  DriverViewController.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 6/30/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import SkyFloatingLabelTextField
import UIKit

enum DriverRole: String {
    case injured = "Injured"
    case causer = "Causer"
}

class DriverViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var driverPhoneTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var driverNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var driverLicenseSerieTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var driverLicenseNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var vehicleRegistrationNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var otherSwitch: UISwitch!
    @IBOutlet weak var vehicleMakerTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var vehicleModelTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var vehicleRegistrationCertificateSerieTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var vehicleRegistrationCertificateNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var absentSwitch: UISwitch!
    @IBOutlet weak var insuranceCompanyNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var insurancePolicySerieTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var insurancePolicyNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var navigationButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var pickersView: UIView!
    @IBOutlet weak var makerPickerBackgroundView: UIView!
    @IBOutlet weak var makerPickerView: UIPickerView!
    @IBOutlet weak var modelPickerBackgroundView: UIView!
    @IBOutlet weak var modelPickerView: UIPickerView!
    @IBOutlet weak var insuranceCompanyNamePickerBackgroundView: UIView!
    @IBOutlet weak var insuranceCompanyNamePickerView: UIPickerView!
    @IBOutlet weak var insurancePolicySeriePickerBackgroundView: UIView!
    @IBOutlet weak var insurancePolicySeriePickerView: UIPickerView!
    @IBOutlet weak var wrappableViewHeightConstraint: NSLayoutConstraint!
    
    private let makers = vehiclesMakersAndModels.keys.sorted()
    private let insuranceCompaniesNames = insuranceCompanies.map({ $0.1 }).sorted()
    
    private var titleColor = UIColor.gray
    private var shouldClearVehicleRegistrationNumber = false
    private var models = [String]()
    var driverRole: DriverRole = .injured
    
    private var driverPhone: String? {
        didSet {
            guard let newValue = driverPhone else {
                return
            }
            UserDefaultsManager.shared.saveDriverPhone(dirverPhone: newValue, driverRole: driverRole)
            driverPhoneTextField.text = newValue
        }
    }
    
    private var driverName: String? {
        didSet {
            guard let newValue = driverName else {
                return
            }
            UserDefaultsManager.shared.saveDriverName(driverName: newValue, driverRole: driverRole)
            driverNameTextField.text = newValue
        }
    }
    
    var driverLicenseSerie: String? {
        didSet {
            guard let newValue = driverLicenseSerie else {
                return
            }
            UserDefaultsManager.shared.saveDriverLicenseSerie(driverLicenseSerie: newValue, driverRole: driverRole)
            driverLicenseSerieTextField.text = newValue
        }
    }
    
    var driverLicenseNumber: String? {
        didSet {
            guard let newValue = driverLicenseNumber else {
                return
            }
            UserDefaultsManager.shared.saveDriverLicenseNumber(driverLicenseNumber: newValue, driverRole: driverRole)
            driverLicenseNumberTextField.text = newValue
        }
    }
    
    var vehicleRegistrationNumber: String? {
        didSet {
            guard let newValue = vehicleRegistrationNumber else {
                return
            }
            UserDefaultsManager.shared.saveVehicleRegistrationNumber(vehicleRegistrationNumber: newValue, driverRole: driverRole)
            vehicleRegistrationNumberTextField.text = newValue
        }
    }
    
    var otherSwitchState = false {
        didSet {
            UserDefaultsManager.shared.saveOtherSwitchState(otherSwitchState: otherSwitchState, driverRole: driverRole)
            if shouldClearVehicleRegistrationNumber {
                vehicleRegistrationNumber?.removeAll()
            }
            otherSwitch.isOn = otherSwitchState
        }
    }
    
    var vehicleMaker: String? {
        didSet {
            guard let newValue = vehicleMaker else {
                return
            }
            UserDefaultsManager.shared.saveVehicleMaker(vehicleMaker: newValue, driverRole: driverRole)
            vehicleMakerTextField.text = newValue
        }
    }
    
    var vehicleModel: String? {
        didSet {
            guard let newValue = vehicleModel else {
                return
            }
            UserDefaultsManager.shared.saveVehicleModel(vehicleModel: newValue, driverRole: driverRole)
            vehicleModelTextField.text = newValue
        }
    }
    
    var vehicleRegistrationCertificateSerie: String? {
        didSet {
            guard let newValue = vehicleRegistrationCertificateSerie else {
                return
            }
            UserDefaultsManager.shared.saveVehicleRegistrationCertificateSerie(vehicleRegistrationCertificateSerie: newValue, driverRole: driverRole)
            vehicleRegistrationCertificateSerieTextField.text = newValue
        }
    }
    
    var vehicleRegistrationCertificateNumber: String? {
        didSet {
            guard let newValue = vehicleRegistrationCertificateNumber else {
                return
            }
            UserDefaultsManager.shared.saveVehicleRegistrationCertificateNumber(vehicleRegistrationCertificateNumber: newValue, driverRole: driverRole)
            vehicleRegistrationCertificateNumberTextField.text = newValue
        }
    }
    
    var absentSwitchState = false {
        didSet {
            UserDefaultsManager.shared.saveAbsentSwitchState(absentSwitchState: absentSwitchState, driverRole: driverRole)
            wrappableViewHeightConstraint.constant = absentSwitchState ? 0.0 : 120.0
            absentSwitch.isOn = absentSwitchState
        }
    }
    
    var insuranceCompanyName: String? {
        didSet {
            guard let newValue = insuranceCompanyName else {
                return
            }
            UserDefaultsManager.shared.saveInsuranceCompanyName(insuranceCompanyName: newValue, driverRole: driverRole)
            insuranceCompanyNameTextField.text = newValue
        }
    }
    
    var insurancePolicySerie: String? {
        didSet {
            guard let newValue = insurancePolicySerie else {
                return
            }
            UserDefaultsManager.shared.saveInsurancePolicySerie(insurancePolicySerie: newValue, driverRole: driverRole)
            insurancePolicySerieTextField.text = newValue
        }
    }
    
    var insurancePolicyNumber: String? {
        didSet {
            guard let newValue = insurancePolicyNumber else {
                return
            }
            UserDefaultsManager.shared.saveInsurancePolicyNumber(insurancePolicyNumber: newValue, driverRole: driverRole)
            insurancePolicyNumberTextField.text = newValue
        }
    }
    
    private var filteredMakers = [String]() {
        didSet {
            if filteredMakers.isEmpty {
                pickersView.isHidden = true
                makerPickerBackgroundView.isHidden = true
                return
            }
            makerPickerBackgroundView.isHidden = false
            pickersView.isHidden = false
            makerPickerView.reloadAllComponents()
        }
    }
    
    private var filteredModels = [String]() {
        didSet {
            if filteredModels.isEmpty {
                pickersView.isHidden = true
                modelPickerBackgroundView.isHidden = true
                return
            }
            modelPickerBackgroundView.isHidden = false
            pickersView.isHidden = false
            modelPickerView.reloadAllComponents()
        }
    }
    
    private var filteredInsuranceCompaniesNames = [String]() {
        didSet {
            if filteredInsuranceCompaniesNames.isEmpty {
                pickersView.isHidden = true
                insuranceCompanyNamePickerBackgroundView.isHidden = true
                return
            }
            insuranceCompanyNamePickerBackgroundView.isHidden = false
            pickersView.isHidden = false
            insuranceCompanyNamePickerView.reloadAllComponents()
        }
    }
    
    private var activeTextField: UITextField? {
        didSet {
            if let oldValue = oldValue as? SkyFloatingLabelTextField {
                oldValue.titleColor = titleColor
            }
            if let newValue = activeTextField as? SkyFloatingLabelTextField {
                newValue.errorMessage = nil
                titleColor = newValue.titleColor
                newValue.titleColor = newValue.selectedTitleColor
            }
            if oldValue == driverPhoneTextField {
                driverPhone = driverPhoneTextField.text
            }
            if oldValue == driverNameTextField {
                driverName = driverNameTextField.text
            }
            if oldValue == driverLicenseSerieTextField {
                driverLicenseSerie = driverLicenseSerieTextField.text
            }
            if oldValue == driverLicenseNumberTextField {
                driverLicenseNumber = driverLicenseNumberTextField.text
            }
            if oldValue == vehicleRegistrationNumberTextField {
                vehicleRegistrationNumber = vehicleRegistrationNumberTextField.text
            }
            if oldValue == vehicleMakerTextField {
                vehicleMaker = vehicleMakerTextField.text
            }
            if oldValue == vehicleModelTextField {
                vehicleModel = vehicleModelTextField.text
            }
            if oldValue == vehicleRegistrationCertificateSerieTextField {
                vehicleRegistrationCertificateSerie = vehicleRegistrationCertificateSerieTextField.text
            }
            if oldValue == vehicleRegistrationCertificateNumberTextField {
                vehicleRegistrationCertificateNumber = vehicleRegistrationCertificateNumberTextField.text
            }
            if oldValue == insuranceCompanyNameTextField {
                insuranceCompanyName = insuranceCompanyNameTextField.text
            }
            if oldValue == insurancePolicySerieTextField {
                insurancePolicySerie = insurancePolicySerieTextField.text
            }
            if oldValue == insurancePolicyNumberTextField {
                insurancePolicyNumber = insurancePolicyNumberTextField.text
            }
            if activeTextField == nil || activeTextField == insurancePolicySerieTextField {
                oldValue?.resignFirstResponder()
            }
            oldValue?.isUserInteractionEnabled = true
            activeTextField?.isUserInteractionEnabled = false
        }
    }
    
    var validationResult = false {
        didSet {
            UserDefaultsManager.shared.saveValidationResultForDriverViewController(validationResult: validationResult, driverRole: driverRole)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationItem()
        setupView()
        setupTextFields()
        setupNavigationButton()
        setupNextButton()
        setupPickersView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
        restoreData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        activeTextField = nil
        removeKeyboardNotifications()
    }
    
    @IBAction func otherSwitchChangeValue(_ sender: UISwitch) {
        otherSwitchState = sender.isOn
        activeTextField = nil
    }
    
    @IBAction func absentSwitchChangeValue(_ sender: UISwitch) {
        absentSwitchState = sender.isOn
        activeTextField = nil
    }
    
    @IBAction func makerPickerOKButtonTouchUpInside(_ sender: UIButton) {
        let selectedMaker = filteredMakers[makerPickerView.selectedRow(inComponent: 0)]
        vehicleMakerTextField.text = selectedMaker
        models = vehiclesMakersAndModels[selectedMaker] ?? []
        pickersView.isHidden = true
        makerPickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func makerPickerCancelButtonTouchUpInside(_ sender: UIButton) {
        pickersView.isHidden = true
        makerPickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func modelPickerOKButtonTouchUpInside(_ sender: UIButton) {
        vehicleModelTextField.text = filteredModels[modelPickerView.selectedRow(inComponent: 0)]
        pickersView.isHidden = true
        modelPickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func modelPickerCancelButtonTouchUpInside(_ sender: UIButton) {
        pickersView.isHidden = true
        modelPickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func insuranceCompanyNamePickerOKButtonTouchUpInside(_ sender: UIButton) {
        insuranceCompanyNameTextField.text = filteredInsuranceCompaniesNames[insuranceCompanyNamePickerView.selectedRow(inComponent: 0)]
        pickersView.isHidden = true
        insuranceCompanyNamePickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func insuranceCompanyNamePickerCancelButtonTouchUpInside(_ sender: UIButton) {
        pickersView.isHidden = true
        insuranceCompanyNamePickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func insurancePolicySeriePickerOKButtonTouchUpInside(_ sender: UIButton) {
        insurancePolicySerieTextField.text = insurancePolicySeries[insurancePolicySeriePickerView.selectedRow(inComponent: 0)]
        pickersView.isHidden = true
        insurancePolicySeriePickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func insurancePolicySeriePickerCancelButtonTouchUpInsid(_ sender: UIButton) {
        pickersView.isHidden = true
        insurancePolicySeriePickerBackgroundView.isHidden = true
        activeTextField = nil
    }
    
    @IBAction func navigationButtonTouchUpInside(_ sender: UIButton) {
        activeTextField = nil
        goToNavigationViewController()
    }
    
    @IBAction func nextButtonTouchUpInside(_ sender: UIButton) {
        activeTextField = nil
        validationResult = validateData()
        if !validationResult {
            AlertManager.shared.showAlert(title: "Не все поля заполнены", message: "Вы действительно хотите перейти на следующую страницу?", positiveTitle: "Перейти", negativeTitle: "Остаться", positiveActionHandler: { [weak self] _ in
                self?.goToNextViewController()
                }, negativeActionHandler: nil, presenter: self)
            return
        }
        goToNextViewController()
    }
    
}

extension DriverViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        guard let text = textField.text else {
            return false
        }
        if textField == vehicleMakerTextField {
            filteredMakers = makers.filter({ $0.lowercased().hasPrefix(text.lowercased()) })
        }
        if textField == vehicleModelTextField {
            guard let maker = vehicleMakerTextField.text, !maker.isEmpty else {
                return false
            }
            filteredModels = models.filter({ $0.lowercased().hasPrefix(text.lowercased()) })
        }
        if textField == insuranceCompanyNameTextField {
            filteredInsuranceCompaniesNames = insuranceCompaniesNames.filter({ $0.lowercased().hasPrefix(text.lowercased()) })
        }
        if textField == insurancePolicySerieTextField {
            insurancePolicySeriePickerBackgroundView.isHidden = false
            pickersView.isHidden = false
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else {
            return false
        }
        if textField == vehicleMakerTextField {
            if let maker = makers.first(where: { $0.lowercased() == text.lowercased() }) {
                textField.text = maker
                models = (vehiclesMakersAndModels[maker] ?? []).sorted()
            } else {
                models = []
            }
            
            pickersView.isHidden = true
            makerPickerBackgroundView.isHidden = true
        }
        if textField == vehicleModelTextField {
            if let model = models.first(where: { $0.lowercased() == text.lowercased() }) {
                textField.text = model
            }
            pickersView.isHidden = true
            modelPickerBackgroundView.isHidden = true
        }
        if textField == insuranceCompanyNameTextField {
            if let insuranceCompanyName = insuranceCompaniesNames.first(where: { $0.lowercased() == text.lowercased() }) {
                textField.text = insuranceCompanyName
            }
            pickersView.isHidden = true
            insuranceCompanyNamePickerBackgroundView.isHidden = true
        }
        activeTextField = nil
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return false
        }
        let textLength = text.count
        if textField == driverPhoneTextField {
            // Delete only in permitted length ranges
            if string.isEmpty && textLength > 4 {
                if text.hasSuffix(" ") {
                    textField.text = String(text.dropLast())
                }
                if text.hasSuffix(") ") {
                    textField.text = String(text.dropLast(2))
                }
                return true
            }
            // Digits only in permitted length ranges
            guard "0"..."9" ~= string && (4...6 ~= textLength || 9...11 ~= textLength || 13...14 ~= textLength || 16...17 ~= textLength) else {
                return false
            }
            // Insert white spaces
            if textLength == 6 {
                textField.text = text + string + ") "
                return false
            }
            if textLength == 11 || textLength == 14 {
                textField.text = text + string + " "
                return false
            }
        }
        if textField == driverLicenseSerieTextField {
            // Delete only in permitted length ranges
            if string.isEmpty {
                if text.last == " " {
                    textField.text = String(text.dropLast())
                }
                return true
            }
            // Digits or letters only in permitted length ranges
            guard "0"..."9" ~= string && (0...1 ~= textLength || 3...4 ~= textLength) || ("а"..."я" ~= string || "А"..."Я" ~= string) && 3...4 ~= textLength else {
                return false
            }
            if ("а"..."я" ~= string || "А"..."Я" ~= string) && textLength == 3 && text.hasPrefix("00") {
                return false
            }
            if let lastCharacter = text.last, "0"..."9" ~= string && textLength == 4 && ("а"..."я" ~= String(lastCharacter) || "А"..."Я" ~= String(lastCharacter)) || ("а"..."я" ~= string || "А"..."Я" ~= string) && textLength == 4 && "0"..."9" ~= String(lastCharacter) {
                return false
            }
            if string == "0" && text == "00 0" {
                return false
            }
            // Insert white spaces
            if textLength == 1 {
                textField.text = text + string + " "
                return false
            }
        }
        if textField == driverLicenseNumberTextField || textField == vehicleRegistrationCertificateNumberTextField {
            // Delete only in permitted length ranges
            if string.isEmpty {
                return true
            }
            // Digits only in permitted length ranges
            guard "0"..."9" ~= string && textLength < 6 else {
                return false
            }
            if string == "0" && text == "00000" {
                return false
            }
        }
        if textField == vehicleRegistrationNumberTextField {
            if otherSwitch.isOn {
                return true
            }
            // Delete only in permitted length ranges
            if string.isEmpty {
                if text.last == " " {
                    textField.text = String(text.dropLast())
                }
                return true
            }
            // Digits or letters only in permitted length ranges
            guard "0"..."9" ~= string && (2...4 ~= textLength || 9...11 ~= textLength) || ("а"..."я" ~= string || "А"..."Я" ~= string) && (textLength == 0 || 6...7 ~= textLength) else {
                return false
            }
            // Insert white spaces
            if textLength == 0 || textLength == 4 || textLength == 7 {
                textField.text = text + string + " "
                return false
            }
        }
        if textField == vehicleRegistrationCertificateSerieTextField {
            // Delete only in permitted length ranges
            if string.isEmpty {
                if text.last == " " {
                    textField.text = String(text.dropLast())
                }
                return true
            }
            // Digits or letters only in permitted length ranges
            guard "0"..."9" ~= string && (0...1 ~= textLength || 3...4 ~= textLength) else {
                return false
            }
            if string == "0" && text == "00 0" {
                return false
            }
            // Insert white spaces
            if textLength == 1 {
                textField.text = text + string + " "
                return false
            }
        }
        if textField == vehicleMakerTextField {
            let predicate = (string.isEmpty ? String(text.dropLast()) : (text + string)).lowercased()
            filteredMakers = makers.filter({ $0.lowercased().hasPrefix(predicate) })
        }
        if textField == vehicleModelTextField {
            let predicate = (string.isEmpty ? String(text.dropLast()) : (text + string)).lowercased()
            filteredModels = models.filter({ $0.lowercased().hasPrefix(predicate) })
        }
        if textField == insuranceCompanyNameTextField {
            let predicate = (string.isEmpty ? String(text.dropLast()) : (text + string)).lowercased()
            let tempFilteredInsuranceCompaniesNames = insuranceCompaniesNames.filter({ $0.lowercased().hasPrefix(predicate) })
            if tempFilteredInsuranceCompaniesNames.count == 0 {
                return false
            }
            filteredInsuranceCompaniesNames = tempFilteredInsuranceCompaniesNames
        }
        if textField == insurancePolicyNumberTextField {
            // Delete only in permitted length ranges
            if string.isEmpty {
                return true
            }
            // Digits only in permitted length ranges
            guard "0"..."9" ~= string && textLength < 10 else {
                return false
            }
            if string == "0" && text == "000000000" {
                return false
            }
        }
        return true
    }
    
}

extension DriverViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == makerPickerView {
            return filteredMakers.count
        }
        if pickerView == modelPickerView {
            return filteredModels.count
        }
        if pickerView == insuranceCompanyNamePickerView {
            return filteredInsuranceCompaniesNames.count
        }
        if pickerView == insurancePolicySeriePickerView {
            return insurancePolicySeries.count
        }
        return 0
    }
    
}
    
extension DriverViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == makerPickerView {
            return filteredMakers[row]
        }
        if pickerView == modelPickerView {
            return filteredModels[row]
        }
        if pickerView == insuranceCompanyNamePickerView {
            return filteredInsuranceCompaniesNames[row]
        }
        if pickerView == insurancePolicySeriePickerView {
            return insurancePolicySeries[row]
        }
        return nil
    }
    
}

extension DriverViewController {
    
    func setupNavigationItem() {
        switch driverRole {
        case .injured:
            navigationItem.title = "Участник \"А\" / Пострадавший"
        case .causer:
            navigationItem.title = "Участник \"Б\" / Виновник"
        }
    }
    
    func setupView() {
        switch driverRole {
        case .injured:
            view.backgroundColor = UIColor(red: 192/255, green: 220/255, blue: 187/255, alpha: 1.0)
        case .causer:
            view.backgroundColor = UIColor(red: 231/255, green: 198/255, blue: 199/255, alpha: 1.0)
        }
    }
    
    func setupTextFields() {
        [driverPhoneTextField, driverNameTextField, driverLicenseSerieTextField, driverLicenseNumberTextField, vehicleRegistrationNumberTextField, vehicleMakerTextField, vehicleModelTextField, vehicleRegistrationCertificateSerieTextField, vehicleRegistrationCertificateNumberTextField, insuranceCompanyNameTextField, insurancePolicySerieTextField, insurancePolicyNumberTextField].forEach({
            $0?.titleFormatter = { $0 }
        })
    }
    
    func setupNavigationButton() {
        navigationButton.setTitle("\u{f0c9}", for: .normal)
    }
    
    func setupNextButton() {
        nextButton.setTitle("\u{f061}", for: .normal)
        let layer = nextButton.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 2
    }
    
    func setupPickersView() {
        let layer = pickersView.layer
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 2
    }
    
    func restoreData() {
        driverPhone = UserDefaultsManager.shared.restoreDriverPhone(driverRole: driverRole) ?? "+7 ("
        driverName = UserDefaultsManager.shared.restoreDriverName(driverRole: driverRole) ?? ""
        driverLicenseSerie = UserDefaultsManager.shared.restoreDriverLicenseSerie(driverRole: driverRole) ?? ""
        driverLicenseNumber = UserDefaultsManager.shared.restoreDriverLicenseNumber(driverRole: driverRole) ?? ""
        vehicleRegistrationNumber = UserDefaultsManager.shared.restoreVehicleRegistrationNumber(driverRole: driverRole) ?? ""
        shouldClearVehicleRegistrationNumber = false
        otherSwitchState = UserDefaultsManager.shared.restoreOtherSwitchState(driverRole: driverRole)
        shouldClearVehicleRegistrationNumber = true
        vehicleMaker = UserDefaultsManager.shared.restoreVehicleMaker(driverRole: driverRole) ?? ""
        vehicleModel = UserDefaultsManager.shared.restoreVehicleModel(driverRole: driverRole) ?? ""
        vehicleRegistrationCertificateSerie = UserDefaultsManager.shared.restoreVehicleRegistrationCertificateSerie(driverRole: driverRole) ?? ""
        vehicleRegistrationCertificateNumber = UserDefaultsManager.shared.restoreVehicleRegistrationCertificateNumber(driverRole: driverRole) ?? ""
        absentSwitchState = UserDefaultsManager.shared.restoreAbsentSwitchState(driverRole: driverRole)
        insuranceCompanyName = UserDefaultsManager.shared.restoreInsuranceCompanyName(driverRole: driverRole) ?? ""
        insurancePolicySerie = UserDefaultsManager.shared.restoreInsurancePolicySerie(driverRole: driverRole) ?? insurancePolicySeries.first
        insurancePolicyNumber = UserDefaultsManager.shared.restoreInsurancePolicyNumber(driverRole: driverRole) ?? ""
    }
    
    func validateData() -> Bool {
        var result = true
        if let driverPhone = driverPhone, driverPhone.count < 18 {
            driverPhoneTextField.errorMessage = "Формат поля должен быть +7 (000) 000 00 00"
            result = false
        }
        if let driverPhone = driverPhone, driverPhone.count == 4 {
            driverPhoneTextField.errorMessage = "Введите телефон водителя ТС"
            result = false
        }
        if let driverName = driverName, driverName.isEmpty {
            driverNameTextField.errorMessage = "Введите ФИО водителя"
            result = false
        }
        if let driverLicenseSerie = driverLicenseSerie, driverLicenseSerie.count < 5 {
            driverLicenseSerieTextField.errorMessage = "Серия ВУ должна содержать 4 символа"
            result = false
        }
        if let driverLicenseSerie = driverLicenseSerie, driverLicenseSerie.isEmpty {
            driverLicenseSerieTextField.errorMessage = "Введите серию ВУ"
            result = false
        }
        if let driverLicenseNumber = driverLicenseNumber, driverLicenseNumber.count < 6 {
            driverLicenseNumberTextField.errorMessage = "Номер ВУ должен содержать 6 цифр"
            result = false
        }
        if let driverLicenseNumber = driverLicenseNumber, driverLicenseNumber.isEmpty {
            driverLicenseNumberTextField.errorMessage = "Введите номер ВУ"
            result = false
        }
        if !otherSwitchState, let vehicleRegistrationNumber = vehicleRegistrationNumber, vehicleRegistrationNumber.count < 11 {
            vehicleRegistrationNumberTextField.errorMessage = "Формат поля должен быть a 000 aa 00 или а 000 аа 000"
            result = false
        }
        if let vehicleRegistrationNumber = vehicleRegistrationNumber, vehicleRegistrationNumber.isEmpty {
            vehicleRegistrationNumberTextField.errorMessage = "Введите гос. номер ТС"
            result = false
        }
        if let vehicleMaker = vehicleMaker, vehicleMaker.isEmpty {
            vehicleMakerTextField.errorMessage = "Введите марку ТС"
            result = false
        }
        if let vehicleModel = vehicleModel, vehicleModel.isEmpty {
            vehicleModelTextField.errorMessage = "Введите модель ТС"
            result = false
        }
        if let vehicleRegistrationCertificateSerie = vehicleRegistrationCertificateSerie, vehicleRegistrationCertificateSerie.count < 5 {
            vehicleRegistrationCertificateSerieTextField.errorMessage = "Серия СТС должна содержать 4 цифры"
            result = false
        }
        if let vehicleRegistrationCertificateSerie = vehicleRegistrationCertificateSerie, vehicleRegistrationCertificateSerie.isEmpty {
            vehicleRegistrationCertificateSerieTextField.errorMessage = "Введите серию СТС"
            result = false
        }
        if let vehicleRegistrationCertificateNumber = vehicleRegistrationCertificateNumber, vehicleRegistrationCertificateNumber.count < 6 {
            vehicleRegistrationCertificateNumberTextField.errorMessage = "Номер СТС должен содержать 6 цифр"
            result = false
        }
        if let vehicleRegistrationCertificateNumber = vehicleRegistrationCertificateNumber, vehicleRegistrationCertificateNumber.isEmpty {
            vehicleRegistrationCertificateNumberTextField.errorMessage = "Введите номер СТС"
            result = false
        }
        if !absentSwitchState, let insuranceCompanyName = insuranceCompanyName, insuranceCompanyName.isEmpty {
            insuranceCompanyNameTextField.errorMessage = "Введите название страховой компании"
            result = false
        }
        if !absentSwitchState, let insurancePolicyNumber = insurancePolicyNumber, insurancePolicyNumber.count < 10 {
            insurancePolicyNumberTextField.errorMessage = "Номер полиса должен содержать 10 цифр"
            result = false
        }
        if !absentSwitchState, let insurancePolicyNumber = insurancePolicyNumber, insurancePolicyNumber.isEmpty {
            insurancePolicyNumberTextField.errorMessage = "Введите номер полиса"
            result = false
        }
        return result
    }
    
    func goToNavigationViewController() {
        if var viewControllers = navigationController?.viewControllers, let viewController = viewControllers.first(where: { $0 as? NavigationViewController != nil }) as? NavigationViewController {
            viewControllers = viewControllers.filter({ $0 != viewController })
            navigationController?.viewControllers = viewControllers
            navigationController?.pushViewController(viewController, animated: true)
            return
        }
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Navigation View Controller") as? NavigationViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToNextViewController() {
        switch driverRole {
        case .injured:
            guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Driver View Controller") as? DriverViewController else {
                return
            }
            viewController.driverRole = .causer
            navigationController?.pushViewController(viewController, animated: true)
        case .causer:
            guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Photos View Controller") as? PhotosViewController else {
                return
            }
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            let keyboardFrameSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardFrameSize.height, 0.0)
            scrollView.contentInset = contentInsets
            scrollView.scrollIndicatorInsets = contentInsets
            var rect = view.frame
            rect.size.height -= keyboardFrameSize.height
            if let activeTextFieldFrame = activeTextField?.frame, !rect.contains(activeTextFieldFrame.origin) || !rect.contains(CGPoint(x: activeTextFieldFrame.origin.x + activeTextFieldFrame.size.width, y: activeTextFieldFrame.origin.y + activeTextFieldFrame.size.height)) {
                scrollView.scrollRectToVisible(activeTextFieldFrame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide() {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
}
