//
//  LandingViewController.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 6/29/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

class LandingViewController: UIViewController {
    
    @IBOutlet var buttons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(hidden: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigationBar(hidden: false)
    }
    
    @IBAction func registerButtonTouchUpInside(_ sender: UIButton) {
        goToLocationDateTimeViewController()
    }
    
    @IBAction func helpButtonTouchUpInside(_ sender: UIButton) {
        goToHelpViewController()
    }
    
}

extension LandingViewController {
    
    func setupButtons() {
        buttons.forEach({
            let layer = $0.layer
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.5
            layer.shadowOffset = CGSize(width: 0, height: 1)
            layer.shadowRadius = 2
        })
    }
    
    func setupNavigationBar(hidden: Bool) {
        navigationController?.setNavigationBarHidden(hidden, animated: true)
    }
    
    func goToLocationDateTimeViewController() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Location Date Time View Controller") as? LocationDateTimeViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToHelpViewController() {
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Help View Controller") as? HelpViewController else {
            return
        }
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
