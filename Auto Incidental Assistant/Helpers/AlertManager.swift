//
//  AlertManager.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/11/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import UIKit

class AlertManager {
    
    static let shared = AlertManager()
    
    func showAlert(title: String?, message: String?, positiveTitle: String?, negativeTitle: String?, positiveActionHandler: ((UIAlertAction) -> ())?, negativeActionHandler: ((UIAlertAction) -> ())?, presenter: UIViewController?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if negativeTitle != nil {
            let negativeAction = UIAlertAction(title: negativeTitle, style: .default, handler: negativeActionHandler)
            alertController.addAction(negativeAction)
        }
        if positiveTitle != nil {
            let positiveAction = UIAlertAction(title: positiveTitle, style: .default, handler: positiveActionHandler)
            alertController.addAction(positiveAction)
        }
        presenter?.present(alertController, animated: true)
    }
    
}
