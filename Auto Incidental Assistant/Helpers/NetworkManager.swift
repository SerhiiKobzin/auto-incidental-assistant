//
//  NetworkManager.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/18/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import Alamofire

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private let baseURL = "http://185.104.106.98:8080/dtp/rest/"
    private let getAuthTokenEndpoint = "auth"
    private let postAccidentDataEndpoint = "v2/entities/dtp$Accident"
    private let postImageFileEndpoint = "v2/files"
    private let postAccidentImageEndpoint = "v2/entities/dtp$AccidentImage"
    
    func getAddressWithCoordinates(latitude: String, longitude: String, completionHandler: @escaping (String?) -> ()) {
        let urlString = "https://geocode-maps.yandex.ru/1.x/"
        let parameters: Parameters = ["geocode" : longitude + "," + latitude,
                                      "format"  : "json",
                                      "lang"    : "ru_RU"]
        Alamofire.request(urlString, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            guard let valueDictionary = response.result.value as? [String : Any], let responseDictionary = valueDictionary["response"] as? [String : Any], let geoObjectCollectionDictionary = responseDictionary["GeoObjectCollection"] as? [String : Any], let featureMemberArray = geoObjectCollectionDictionary["featureMember"] as? [[String : Any]], let firstFeatureMember = featureMemberArray.first, let geoObjectDictionary = firstFeatureMember["GeoObject"] as? [String : Any], let metaDataPropertyDictionary = geoObjectDictionary["metaDataProperty"] as? [String : Any], let geocoderMetaDataDictionary = metaDataPropertyDictionary["GeocoderMetaData"] as? [String : Any], let addressDictionary = geocoderMetaDataDictionary["Address"] as? [String : Any], let componentsArray = addressDictionary["Components"] as? [[String : String]] else {
                completionHandler(nil)
                return
            }
            let streetDictionary = componentsArray.first(where: { $0["kind"] == "street" })
            let street = streetDictionary?["name"] ?? "улица не определена"
            let houseDictionary = componentsArray.first(where: { $0["kind"] == "house" })
            let house = houseDictionary?["name"] ?? "номер дома не определен"
            let localityDictionary = componentsArray.first(where: { $0["kind"] == "locality" })
            let locality = localityDictionary?["name"] ?? "населенный пункт не определен"
            let provinceDictionary = componentsArray.first(where: { $0["kind"] == "province" })
            let province = provinceDictionary?["name"] ?? "область не определена"
            let countryDictionary = componentsArray.first(where: { $0["kind"] == "country" })
            let country = countryDictionary?["name"] ?? "страна не определена"
            completionHandler(street + ", " + house + ", " + locality + ", " + province + ", " + country)
        })
    }
    
    func getAuthToken(completionHandler: @escaping (String?) -> ()) {
        let urlString = baseURL + getAuthTokenEndpoint
        let parameters: Parameters = ["clientId" : "idtp"]
        Alamofire.request(urlString, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            guard let valueDictionary = response.result.value as? [String : Any], let accessToken = valueDictionary["access_token"] as? String else {
                completionHandler(nil)
                return
            }
            completionHandler(accessToken)
        })
    }
    
    func postAccidentData(authToken: String, address: String, date: String, latitude: Double, longitude: Double, udid: String, injuredDriverPhone: String, injuredDriverName: String, injuredDriverLicenseSerie: String, injuredDriverLicenseNumber: String, injuredVehicleRegistrationNumber: String, injuredOther: Bool, injuredVehicleMaker: String, injuredVehicleModel: String, injuredVehicleRegistrationCertificateSerie: String, injuredVehicleRegistrationCertificateNumber: String, injuredAbsent: Bool, injuredInsuranceCompanyId: Int?, injuredInsurancePolicySerie: String?, injuredInsurancePolicyNumber: String?, causerDriverPhone: String, causerDriverName: String, causerDriverLicenseSerie: String, causerDriverLicenseNumber: String, causerVehicleRegistrationNumber: String, causerOther: Bool, causerVehicleMaker: String, causerVehicleModel: String, causerVehicleRegistrationCertificateSerie: String, causerVehicleRegistrationCertificateNumber: String, causerAbsent: Bool, causerInsuranceCompanyId: Int?, causerInsurancePolicySerie: String?, causerInsurancePolicyNumber: String?, completionHandler: @escaping (String?, Int?) -> ()) {
        let urlString = baseURL + postAccidentDataEndpoint
        let headers = ["Authorization" : "Bearer " + authToken]
        var injuredDriver: Parameters = ["_entityName"      : "dtp$Participant",
                                         "type"             : "notGuilty",
                                         "name"             : injuredDriverName,
                                         "phone"            : injuredDriverPhone,
                                         "numberPlate"      : injuredVehicleRegistrationNumber,
                                         "otherNumberPlate" : injuredOther,
                                         "dlNumber"         : injuredDriverLicenseNumber,
                                         "dlSeries"         : injuredDriverLicenseSerie,
                                         "vBrand"           : injuredVehicleMaker,
                                         "vModel"           : injuredVehicleModel,
                                         "vDocNumber"       : injuredVehicleRegistrationCertificateNumber,
                                         "vDocSeries"       : injuredVehicleRegistrationCertificateSerie,
                                         "noInsurance"      : injuredAbsent]
        if let injuredInsuranceCompanyId = injuredInsuranceCompanyId {
            injuredDriver["insuranceCompany"] = ["id" : injuredInsuranceCompanyId]
        }
        if let injuredInsurancePolicySerie = injuredInsurancePolicySerie {
            injuredDriver["insuranceSeries"] = injuredInsurancePolicySerie
        }
        if let injuredInsurancePolicyNumber = injuredInsurancePolicyNumber {
            injuredDriver["insuranceNumber"] = injuredInsurancePolicyNumber
        }
        var causerDriver: Parameters = ["_entityName"      : "dtp$Participant",
                                        "type"             : "guilty",
                                        "name"             : causerDriverName,
                                        "phone"            : causerDriverPhone,
                                        "numberPlate"      : causerVehicleRegistrationNumber,
                                        "otherNumberPlate" : causerOther,
                                        "dlNumber"         : causerDriverLicenseNumber,
                                        "dlSeries"         : causerDriverLicenseSerie,
                                        "vBrand"           : causerVehicleMaker,
                                        "vModel"           : causerVehicleModel,
                                        "vDocNumber"       : causerVehicleRegistrationCertificateNumber,
                                        "vDocSeries"       : causerVehicleRegistrationCertificateSerie,
                                        "noInsurance"      : causerAbsent]
        if let causerInsuranceCompanyId = causerInsuranceCompanyId {
            causerDriver["insuranceCompany"] = ["id" : causerInsuranceCompanyId]
        }
        if let causerInsurancePolicySerie = causerInsurancePolicySerie {
            causerDriver["insuranceSeries"] = causerInsurancePolicySerie
        }
        if let causerInsurancePolicyNumber = causerInsurancePolicyNumber {
            causerDriver["insuranceNumber"] = causerInsurancePolicyNumber
        }
        let parameters: Parameters = ["date"         : date,
                                      "place"        : address,
                                      "latitude"     : latitude,
                                      "longitude"    : longitude,
                                      "imei"         : udid,
                                      "participants" : [injuredDriver, causerDriver]]
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            guard let valueDictionary = response.result.value as? [String : Any], let id = valueDictionary["id"] as? String, let regNum = valueDictionary["regNum"] as? Int else {
                completionHandler(nil, nil)
                return
            }
            completionHandler(id, regNum)
        }
    }
    
    func postPhotoFile(authToken: String, fileURL: URL, completionHandler: @escaping (String?) -> ()) {
        let fileName = fileURL.lastPathComponent
        let urlString = baseURL + postImageFileEndpoint + "?name=" + fileName
        let headers = ["Authorization" : "Bearer " + authToken]
        Alamofire.upload(fileURL, to: urlString, method: .post, headers: headers).responseJSON(completionHandler: { response in
            guard let valueDictionary = response.result.value as? [String : Any], let id = valueDictionary["id"] as? String else {
                completionHandler(nil)
                return
            }
            completionHandler(id)
        })
    }
    
    func updateAccidentData(authToken: String, accidentId: String, fileId: String, completionHandler: @escaping (Bool) -> ()) {
        let urlString = baseURL + postAccidentDataEndpoint + "/" + accidentId
        let headers = ["Authorization" : "Bearer " + authToken]
        let parameters: Parameters = ["schemeFile" : ["id" : fileId]]
        Alamofire.request(urlString, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            guard let valueDictionary = response.result.value as? [String : Any], valueDictionary["id"] as? String != nil else {
                completionHandler(false)
                return
            }
            completionHandler(true)
        })
    }
    
    func postAccidentImage(authToken: String, accidentId: String, fileId: String, completionHandler: @escaping (Bool) -> ()) {
        let urlString = baseURL + postAccidentImageEndpoint
        let headers = ["Authorization" : "Bearer " + authToken]
        let parameters: Parameters = ["accident"  : ["id" : accidentId],
                                      "imageFile" : ["id" : fileId]]
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            guard let valueDictionary = response.result.value as? [String : Any], valueDictionary["id"] as? String != nil else {
                completionHandler(false)
                return
            }
            completionHandler(true)
        })
    }
    
}
