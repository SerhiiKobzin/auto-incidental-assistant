//
//  UserDefaultsManager.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/15/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import CoreLocation
import Foundation
import UIKit

class UserDefaultsManager {
    
    static let shared = UserDefaultsManager()
    
    func saveCoordinate(coordinate: CLLocationCoordinate2D) {
        let latitude = coordinate.latitude
        let longitude = coordinate.longitude
        UserDefaults.standard.set(latitude, forKey: "Latitude")
        UserDefaults.standard.set(longitude, forKey: "Longitude")
        UserDefaults.standard.synchronize()
    }
    
    func restoreCoordinate() -> CLLocationCoordinate2D? {
        guard let latitude = UserDefaults.standard.object(forKey: "Latitude") as? CLLocationDegrees, let longitude = UserDefaults.standard.object(forKey: "Longitude") as? CLLocationDegrees else {
            return nil
        }
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func saveAddress(address: String) {
        UserDefaults.standard.set(address, forKey: "Address")
        UserDefaults.standard.synchronize()
    }
    
    func restoreAddress() -> String? {
        return UserDefaults.standard.string(forKey: "Address")
    }
    
    func saveDate(date: Date) {
        UserDefaults.standard.set(date, forKey: "Date")
        UserDefaults.standard.synchronize()
    }
    
    func restoreDate() -> Date? {
        return UserDefaults.standard.object(forKey: "Date") as? Date
    }
    
    func saveValidationResultForLocationDateTimeViewController(validationResult: Bool) {
        UserDefaults.standard.set(validationResult, forKey: "Validation Result For Location Date Time View Controller")
        UserDefaults.standard.synchronize()
    }
    
    func restoreValidationResultForLocationDateTimeViewController() -> Bool {
        return UserDefaults.standard.bool(forKey: "Validation Result For Location Date Time View Controller")
    }
    
    func saveDriverPhone(dirverPhone: String, driverRole: DriverRole) {
        UserDefaults.standard.set(dirverPhone, forKey: driverRole.rawValue + " Driver Phone")
        UserDefaults.standard.synchronize()
    }

    func restoreDriverPhone(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Driver Phone")
    }

    func saveDriverName(driverName: String, driverRole: DriverRole) {
        UserDefaults.standard.set(driverName, forKey: driverRole.rawValue + " Driver Name")
        UserDefaults.standard.synchronize()
    }

    func restoreDriverName(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Driver Name")
    }
    
    func saveDriverLicenseSerie(driverLicenseSerie: String, driverRole: DriverRole) {
        UserDefaults.standard.set(driverLicenseSerie, forKey: driverRole.rawValue + " Driver License Serie")
        UserDefaults.standard.synchronize()
    }
    
    func restoreDriverLicenseSerie(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Driver License Serie")
    }
    
    func saveDriverLicenseNumber(driverLicenseNumber: String, driverRole: DriverRole) {
        UserDefaults.standard.set(driverLicenseNumber, forKey: driverRole.rawValue + " Driver License Number")
        UserDefaults.standard.synchronize()
    }
    
    func restoreDriverLicenseNumber(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Driver License Number")
    }
    
    func saveVehicleRegistrationNumber(vehicleRegistrationNumber: String, driverRole: DriverRole) {
        UserDefaults.standard.set(vehicleRegistrationNumber, forKey: driverRole.rawValue + " Vehicle Registration Number")
        UserDefaults.standard.synchronize()
    }
    
    func restoreVehicleRegistrationNumber(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Vehicle Registration Number")
    }
    
    func saveOtherSwitchState(otherSwitchState: Bool, driverRole: DriverRole) {
        UserDefaults.standard.set(otherSwitchState, forKey: driverRole.rawValue + " Other Switch State")
        UserDefaults.standard.synchronize()
    }
    
    func restoreOtherSwitchState(driverRole: DriverRole) -> Bool {
        return UserDefaults.standard.bool(forKey: driverRole.rawValue + " Other Switch State")
    }
    
    func saveVehicleMaker(vehicleMaker: String, driverRole: DriverRole) {
        UserDefaults.standard.set(vehicleMaker, forKey: driverRole.rawValue + " Vehicle Maker")
        UserDefaults.standard.synchronize()
    }
    
    func restoreVehicleMaker(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Vehicle Maker")
    }
    
    func saveVehicleModel(vehicleModel: String, driverRole: DriverRole) {
        UserDefaults.standard.set(vehicleModel, forKey: driverRole.rawValue + " Vehicle Model")
        UserDefaults.standard.synchronize()
    }
    
    func restoreVehicleModel(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Vehicle Model")
    }
    
    func saveVehicleRegistrationCertificateSerie(vehicleRegistrationCertificateSerie: String, driverRole: DriverRole) {
        UserDefaults.standard.set(vehicleRegistrationCertificateSerie, forKey: driverRole.rawValue + " Vehicle Registration Certificate Serie")
        UserDefaults.standard.synchronize()
    }
    
    func restoreVehicleRegistrationCertificateSerie(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Vehicle Registration Certificate Serie")
    }
    
    func saveVehicleRegistrationCertificateNumber(vehicleRegistrationCertificateNumber: String, driverRole: DriverRole) {
        UserDefaults.standard.set(vehicleRegistrationCertificateNumber, forKey: driverRole.rawValue + " Vehicle Registration Certificate Number")
        UserDefaults.standard.synchronize()
    }
    
    func restoreVehicleRegistrationCertificateNumber(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Vehicle Registration Certificate Number")
    }
    
    func saveAbsentSwitchState(absentSwitchState: Bool, driverRole: DriverRole) {
        UserDefaults.standard.set(absentSwitchState, forKey: driverRole.rawValue + " Absent Switch State")
        UserDefaults.standard.synchronize()
    }
    
    func restoreAbsentSwitchState(driverRole: DriverRole) -> Bool {
        return UserDefaults.standard.bool(forKey: driverRole.rawValue + " Absent Switch State")
    }
    
    func saveInsuranceCompanyName(insuranceCompanyName: String, driverRole: DriverRole) {
        UserDefaults.standard.set(insuranceCompanyName, forKey: driverRole.rawValue + " Insurance Company Name")
        UserDefaults.standard.synchronize()
    }
    
    func restoreInsuranceCompanyName(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Insurance Company Name")
    }
    
    func saveInsurancePolicySerie(insurancePolicySerie: String, driverRole: DriverRole) {
        UserDefaults.standard.set(insurancePolicySerie, forKey: driverRole.rawValue + " Insurance Policy Serie")
        UserDefaults.standard.synchronize()
    }
    
    func restoreInsurancePolicySerie(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Insurance Policy Serie")
    }
    
    func saveInsurancePolicyNumber(insurancePolicyNumber: String, driverRole: DriverRole) {
        UserDefaults.standard.set(insurancePolicyNumber, forKey: driverRole.rawValue + " Insurance Policy Number")
        UserDefaults.standard.synchronize()
    }
    
    func restoreInsurancePolicyNumber(driverRole: DriverRole) -> String? {
        return UserDefaults.standard.string(forKey: driverRole.rawValue + " Insurance Policy Number")
    }
    
    func saveValidationResultForDriverViewController(validationResult: Bool, driverRole: DriverRole) {
        UserDefaults.standard.set(validationResult, forKey: "Validation Result For " + driverRole.rawValue + " Driver View Controller")
        UserDefaults.standard.synchronize()
    }
    
    func restoreValidationResultForDriverViewController(driverRole: DriverRole) -> Bool {
        return UserDefaults.standard.bool(forKey: "Validation Result For " + driverRole.rawValue + " Driver View Controller")
    }
    
    func saveImageURLs(urls: [URL?]?) {
        guard let urls = urls else {
            return
        }
        for index in 0...14 {
            UserDefaults.standard.set(urls[index], forKey: "Image URL " + index.description)
        }
        UserDefaults.standard.synchronize()
    }
    
    func restoreImageURLs() -> [URL?]? {
        var urls = [URL?]()
        for index in 0...14 {
            urls.append(UserDefaults.standard.url(forKey: "Image URL " + index.description))
        }
        return urls
    }
    
    func saveValidationResultForPhotosViewController(validationResult: Bool) {
        UserDefaults.standard.set(validationResult, forKey: "Validation Result For Photos View Controller")
        UserDefaults.standard.synchronize()
    }
    
    func restoreValidationResultForPhotosViewController() -> Bool {
        return UserDefaults.standard.bool(forKey: "Validation Result For Photos View Controller")
    }
    
    func saveImageURL(url: URL?) {
        UserDefaults.standard.set(url, forKey: "Image URL")
        UserDefaults.standard.synchronize()
    }
    
    func restoreImageURL() -> URL? {
        return UserDefaults.standard.url(forKey: "Image URL")
    }
    
    func saveValidationResultForSchemeViewController(validationResult: Bool) {
        UserDefaults.standard.set(validationResult, forKey: "Validation Result For Scheme View Controller")
        UserDefaults.standard.synchronize()
    }
    
    func restoreValidationResultForSchemeViewController() -> Bool {
        return UserDefaults.standard.bool(forKey: "Validation Result For Scheme View Controller")
    }
    
    func removeAllData() {
        UserDefaults.standard.removeObject(forKey: "Latitude")
        UserDefaults.standard.removeObject(forKey: "Longitude")
        UserDefaults.standard.removeObject(forKey: "Address")
        UserDefaults.standard.removeObject(forKey: "Date")
        UserDefaults.standard.removeObject(forKey: "Validation Result For Location Date Time View Controller")
        UserDefaults.standard.removeObject(forKey: "Injured Driver Phone")
        UserDefaults.standard.removeObject(forKey: "Injured Driver Name")
        UserDefaults.standard.removeObject(forKey: "Injured Driver License Serie")
        UserDefaults.standard.removeObject(forKey: "Injured Driver License Number")
        UserDefaults.standard.removeObject(forKey: "Injured Vehicle Registration Number")
        UserDefaults.standard.removeObject(forKey: "Injured Other Switch State")
        UserDefaults.standard.removeObject(forKey: "Injured Vehicle Maker")
        UserDefaults.standard.removeObject(forKey: "Injured Vehicle Model")
        UserDefaults.standard.removeObject(forKey: "Injured Vehicle Registration Certificate Serie")
        UserDefaults.standard.removeObject(forKey: "Injured Vehicle Registration Certificate Number")
        UserDefaults.standard.removeObject(forKey: "Injured Absent Switch State")
        UserDefaults.standard.removeObject(forKey: "Injured Insurance Company Name")
        UserDefaults.standard.removeObject(forKey: "Injured Insurance Policy Serie")
        UserDefaults.standard.removeObject(forKey: "Injured Insurance Policy Number")
        UserDefaults.standard.removeObject(forKey: "Validation Result For Injured Driver View Controller")
        UserDefaults.standard.removeObject(forKey: "Causer Driver Phone")
        UserDefaults.standard.removeObject(forKey: "Causer Driver Name")
        UserDefaults.standard.removeObject(forKey: "Causer Driver License Serie")
        UserDefaults.standard.removeObject(forKey: "Causer Driver License Number")
        UserDefaults.standard.removeObject(forKey: "Causer Vehicle Registration Number")
        UserDefaults.standard.removeObject(forKey: "Causer Other Switch State")
        UserDefaults.standard.removeObject(forKey: "Causer Vehicle Maker")
        UserDefaults.standard.removeObject(forKey: "Causer Vehicle Model")
        UserDefaults.standard.removeObject(forKey: "Causer Vehicle Registration Certificate Serie")
        UserDefaults.standard.removeObject(forKey: "Causer Vehicle Registration Certificate Number")
        UserDefaults.standard.removeObject(forKey: "Causer Absent Switch State")
        UserDefaults.standard.removeObject(forKey: "Causer Insurance Company Name")
        UserDefaults.standard.removeObject(forKey: "Causer Insurance Policy Serie")
        UserDefaults.standard.removeObject(forKey: "Causer Insurance Policy Number")
        UserDefaults.standard.removeObject(forKey: "Validation Result For Causer Driver View Controller")
        UserDefaults.standard.removeObject(forKey: "Image URL 0")
        UserDefaults.standard.removeObject(forKey: "Image URL 1")
        UserDefaults.standard.removeObject(forKey: "Image URL 2")
        UserDefaults.standard.removeObject(forKey: "Image URL 3")
        UserDefaults.standard.removeObject(forKey: "Image URL 4")
        UserDefaults.standard.removeObject(forKey: "Image URL 5")
        UserDefaults.standard.removeObject(forKey: "Image URL 6")
        UserDefaults.standard.removeObject(forKey: "Image URL 7")
        UserDefaults.standard.removeObject(forKey: "Image URL 8")
        UserDefaults.standard.removeObject(forKey: "Image URL 9")
        UserDefaults.standard.removeObject(forKey: "Image URL 10")
        UserDefaults.standard.removeObject(forKey: "Image URL 11")
        UserDefaults.standard.removeObject(forKey: "Image URL 12")
        UserDefaults.standard.removeObject(forKey: "Image URL 13")
        UserDefaults.standard.removeObject(forKey: "Image URL 14")
        UserDefaults.standard.removeObject(forKey: "Validation Result For Photos View Controller")
        UserDefaults.standard.removeObject(forKey: "Image URL")
        UserDefaults.standard.removeObject(forKey: "Validation Result For Scheme View Controller")
    }
    
}
