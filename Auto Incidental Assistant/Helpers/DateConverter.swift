//
//  DateConverter.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/3/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

import Foundation

class DateConverter {
    
    static let shared = DateConverter()
    private let dateFormatter = DateFormatter()
    
    var dateFormat = "dd.MM.yyyy"
    var timeFormat = "HH:mm"
    var serverDateFormat = "yyyy-MM-dd HH:mm:ss.000"
    
    func dateToDateString(date: Date) -> String? {
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }
    
    func dateToTimeString(date: Date) -> String? {
        dateFormatter.dateFormat = timeFormat
        return dateFormatter.string(from: date)
    }
    
    func dateStringToDate(string: String) -> Date? {
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: string)
    }
    
    func timeStringToDate(string: String) -> Date? {
        dateFormatter.dateFormat = timeFormat
        return dateFormatter.date(from: string)
    }
    
    func dateToServerDateString(date: Date) -> String? {
        dateFormatter.dateFormat = serverDateFormat
        return dateFormatter.string(from: date)
    }

}
