//
//  InsurancePolicySeries.swift
//  Auto Incidental Assistant
//
//  Created by Serhii Kobzin on 7/4/18.
//  Copyright © 2018 Serhii Kobzin. All rights reserved.
//

let insurancePolicySeries: [String] = ["XXX",
                                       "CCC",
                                       "MMM",
                                       "KKK",
                                       "EEE",
                                       "BBB"]
